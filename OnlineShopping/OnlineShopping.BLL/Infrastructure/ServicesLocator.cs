﻿using System;
using System.Data.Entity;
using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using OnlineShopping.BLL.Services;
using OnlineShopping.DAL.AutoMapper;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.Identity;
using OnlineShopping.DAL.Interfaces;
using OnlineShopping.DAL.Repositories;
using Unity;
using AutoMapper;

namespace OnlineShopping.BLL.Infrastructure
{
    public class ServicesLocator
    {
        private static IUnityContainer _container;
        private static IResolver _resolver;

        public static IResolver GlobalResolver => _resolver ?? (_resolver = BuildResolver());

        private static IResolver BuildResolver()
        {
            return Register(container => new UnityResolveAdapter(container));
        }

        public static T Register<T>(Func<IUnityContainer,T>callback)
        {
            if (_container == null)
            {
                Configure(RegisterDefaultServices());
            }
            return callback(_container);
        }

        private static IUnityContainer RegisterDefaultServices()
        {
            var container = new UnityContainer();
            //
            container.RegisterInstance(DataMapper.GetInstance());
            //

            container.RegisterType<DbContext, DatabaseContext>();
            container.RegisterType<AppUserStore>();
            container.RegisterType<AppUserManager>();

            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IOrderRepository, OrderRepository>();
            container.RegisterType<IProfileRepository, ProfileRepository>();
            container.RegisterType<ICategoryRepository, CategoryRepository>();
            container.RegisterType<IProductCharacteristicsRepository, ProductCharacteristicsRepository>();
            container.RegisterType<IProductImageRepository, ProductImageRepository>();
            container.RegisterType<IOrderItemRepository, OrderItemRepository>();
            container.RegisterType<IAdminOrderRepository, AdminOrderRepository>();

            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IProductService, ProductService>();
            container.RegisterType<IOrderService, OrderService>();
            container.RegisterType<IProfileService, ProfileService>();
            container.RegisterType<ICategoryService,CategoryService>();
            container.RegisterType<IProductCharacteristicsService, ProductCharacteristicsService>();
            container.RegisterType<IProductImageService, ProductImageService>();
            container.RegisterType<IOrderItemService, OrderItemService>();
            container.RegisterType<IAdminOrderService, AdminOrderService>();

            return container;
        }

        private static void Configure(IUnityContainer container)
        {
            _container = container;
        }

    }
}
