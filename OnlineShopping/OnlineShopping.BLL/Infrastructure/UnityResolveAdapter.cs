﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace OnlineShopping.BLL.Infrastructure
{
    public class UnityResolveAdapter:IResolver
    {
        private readonly IUnityContainer _container;

        public UnityResolveAdapter(IUnityContainer container)
        {
            _container = container;
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public IEnumerable<T> ResolveAll<T>()
        {
            return _container.ResolveAll<T>();
        }
    }
}
