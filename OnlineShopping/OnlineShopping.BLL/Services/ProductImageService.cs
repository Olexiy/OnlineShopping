﻿using OnlineShopping.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.BLL.Services
{
    public class ProductImageService : IProductImageService
    {
        private readonly IProductImageRepository _productImageRepository;

        public ProductImageService(IProductImageRepository productImageRepository)
        {
            _productImageRepository = productImageRepository;
        }

        public bool Create(int productId, string productTitle)
        {
            return _productImageRepository.Create(productId, productTitle);
        }

        public string Get(int productId)
        {
            return _productImageRepository.Get(productId);
        }

        public IEnumerable<string> GetProductImages(int productId)
        {
            return _productImageRepository.GetProductImages(productId);
        }
    }
}
