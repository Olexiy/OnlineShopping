﻿using OnlineShopping.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.BLL.Services
{
    public class ProductCharacteristicsService : IProductCharacteristicsService
    {
        private readonly IProductCharacteristicsRepository _productCharRepository;

        public ProductCharacteristicsService(IProductCharacteristicsRepository productCharRepository)
        {
            _productCharRepository = productCharRepository;
        }

        public bool Create(ProductCharacteristics productCharacteristics)
        {
            return _productCharRepository.Create(productCharacteristics);
        }

        public bool Delete(int characteristicsId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductCharacteristics> Get(int productId)
        {
            return _productCharRepository.Get(productId);
        }

        public bool Update(ProductCharacteristics productCharacteristics)
        {
            throw new NotImplementedException();
        }
    }
}
