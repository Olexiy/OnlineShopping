﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.BLL.Services
{
    public class UserService:IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool CreateUser(Registration registration)
        {
            var result = _userRepository.CreateUser(registration);
            if (result.Succeeded || result.Errors == null || !result.Errors.Any()) return true;
            var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent($"\"{result.Errors.First()}\"")
            };
            throw new HttpResponseException(message);
        }

        public bool UpdateUser(User user)
        {
            return _userRepository.UpdateUser(user);
        }

        public bool DeleteUser(int id)
        {
            return _userRepository.DeleteUser(id);
        }

        public User GetUser(int id)
        {
            return _userRepository.GetUser(id);
        }

        public List<User> GetUsers(string nameQuery)
        {
            return _userRepository.GetUsers(nameQuery);
        }
    }
}
