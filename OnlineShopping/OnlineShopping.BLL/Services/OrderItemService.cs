﻿using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using OnlineShopping.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.BLL.Services
{
    public class OrderItemService : IOrderItemService
    {
        private readonly IOrderItemRepository _orderItemRepository;

        public OrderItemService(IOrderItemRepository orderItemRepository)
        {
            _orderItemRepository = orderItemRepository;
        }

        public bool Create(OrderItem orderItem)
        {
            return _orderItemRepository.Create(orderItem);
        }

        public bool Delete(int orderItemId)
        {
            return _orderItemRepository.Delete(orderItemId);
        }

        public OrderItem Get(int orderId, int productId)
        {
            return _orderItemRepository.Get(orderId, productId);
        }

        public IEnumerable<OrderItem> Get(int orderId)
        {
            return _orderItemRepository.Get(orderId);
        }

        public bool Update(OrderItem orderItem)
        {
            return _orderItemRepository.Update(orderItem);
        }
    }
}
