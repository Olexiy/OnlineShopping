﻿using OnlineShopping.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.BLL.Services
{
    public class AdminOrderService : IAdminOrderService
    {
        private readonly IAdminOrderRepository _adminOrderRepository;

        public AdminOrderService(IAdminOrderRepository adminOrderRepository)
        {
            _adminOrderRepository = adminOrderRepository;
        }

        public IEnumerable<Order> GetActiveOrders()
        {
            return _adminOrderRepository.GetActiveOrders();
        }

        public bool UpdateOrder(Order order)
        {
            return _adminOrderRepository.UpdateOrder(order);
        }
    }
}
