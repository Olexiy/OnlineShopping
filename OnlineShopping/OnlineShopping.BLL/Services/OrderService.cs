﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Entities.Models;
using OnlineShopping.BLL.Infrastructure;
using OnlineShopping.BLL.Interfaces;
using OnlineShopping.DAL.Models;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.BLL.Services
{
    public class OrderService:IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public bool CreateOrder(int customerId,Order order)
        {
            return _orderRepository.CreateOrder(customerId,order);
        }

        public bool UpdateOrder(int customerId, Order order)
        {
            return  _orderRepository.UpdateOrder(customerId,order);
        }

        public bool DeleteOrder(int orderId)
        {
            return _orderRepository.DeleteOrder(orderId);
        }

        public Order GetOrder(int customerId, int orderId)
        {
            return _orderRepository.GetOrder(customerId,orderId);
        }

        public IEnumerable<Order> GetOrders(int userId)
        {
            return _orderRepository.GetOrders(userId);
        }
    }
}
