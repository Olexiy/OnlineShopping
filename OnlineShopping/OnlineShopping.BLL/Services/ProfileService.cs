﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.BLL.Services
{
    public class ProfileService:IProfileService
    {
        private readonly IProfileRepository _profileRepository;

        public ProfileService(IProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }

        public ClientProfile Get(int userId)
        {
            return _profileRepository.Get(userId);
        }

        public bool Delete(int userId)
        {
            throw new NotImplementedException();
        }

        public bool Update(ClientProfile clientProfile)
        {
            return _profileRepository.Update(clientProfile);
        }
    }
}
