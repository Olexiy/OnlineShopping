﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.BLL.Services
{
    public class CategoryService:ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public bool Create(Category category)
        {
            return _categoryRepository.Create(category);
        }

        public bool Delete(int id)
        {
            return _categoryRepository.Delete(id);
        }

        public bool Update(Category category)
        {
            return _categoryRepository.Update(category);
        }

        public Category Get(int id)
        {
            return _categoryRepository.Get(id);
        }

        public IEnumerable<Category> Get()
        {
            return _categoryRepository.Get();
        }

        public bool CreateTitle(int categoryId, string categoryTitle)
        {
            return _categoryRepository.CreateTitle(categoryId, categoryTitle);
        }
    }
}
