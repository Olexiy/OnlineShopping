﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace OnlineShopping.BLL.Interfaces
{
    public interface IOrderService
    {
        bool CreateOrder(int customerId,Order order);
        bool UpdateOrder(int customerId, Order order);
        bool DeleteOrder(int orderId);

        Order GetOrder(int customerId, int orderId);
        IEnumerable<Order> GetOrders(int userId);
    }
}
