﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.BLL.Interfaces
{
    public interface IProductCharacteristicsService
    {
        bool Create(ProductCharacteristics productCharacteristics);
        bool Update(ProductCharacteristics productCharacteristics);
        bool Delete(int characteristicsId);
        IEnumerable<ProductCharacteristics> Get(int productId);
    }
}
