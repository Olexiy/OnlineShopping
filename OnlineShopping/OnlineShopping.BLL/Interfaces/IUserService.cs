﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using OnlineShopping.BLL.Infrastructure;

namespace OnlineShopping.BLL.Interfaces
{
    public interface IUserService
    {
        bool CreateUser(Registration registration);
        bool UpdateUser(User user);
        bool DeleteUser(int id);
        User GetUser(int id);
        List<User> GetUsers(string nameQuery);
    }
}
