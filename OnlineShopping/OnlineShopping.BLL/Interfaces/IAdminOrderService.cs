﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.BLL.Interfaces
{
    public interface IAdminOrderService
    {
        bool UpdateOrder(Order order);
        IEnumerable<Order> GetActiveOrders();
    }
}
