﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace OnlineShopping.BLL.Interfaces
{
    public interface IProfileService
    {
        ClientProfile Get(int userId);
        bool Delete(int userId);
        bool Update(ClientProfile clientProfile);
    }
}
