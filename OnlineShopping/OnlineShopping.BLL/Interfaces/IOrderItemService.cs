﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.BLL.Interfaces
{
    public interface IOrderItemService
    {
        bool Create(OrderItem orderItem);
        bool Update(OrderItem orderItem);
        bool Delete(int orderItemId);
        OrderItem Get(int orderId, int productId);
        IEnumerable<OrderItem> Get(int orderId);
    }
}
