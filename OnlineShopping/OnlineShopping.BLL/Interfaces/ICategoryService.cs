﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace OnlineShopping.BLL.Interfaces
{
    public interface ICategoryService
    {
        bool Create(Category category);
        bool Delete(int id);
        bool Update(Category category);
        Category Get(int id);
        IEnumerable<Category> Get();
        bool CreateTitle(int categoryId, string categoryTitle);
    }
}
