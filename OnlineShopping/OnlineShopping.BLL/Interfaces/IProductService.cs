﻿
using System.Collections.Generic;
using Entities.Models;

namespace OnlineShopping.BLL.Interfaces
{
    public interface IProductService
    {
        bool CreateProduct(Product product);
        bool UpdateProduct(Product product);
        bool DeleteProduct(int id);
        Product GetProduct(int productId);
        IEnumerable<Product> GetCategoryProducts(string categoryName);      
    }
}
