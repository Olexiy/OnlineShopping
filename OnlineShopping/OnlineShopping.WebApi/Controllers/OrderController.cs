﻿
using System.Collections.Generic;
using System.Web.Http;
using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using Microsoft.AspNet.Identity;


namespace OnlineShopping.WebApi.Controllers
{
    //[Authorize(Roles = "User")]
    [Authorize]
    public class OrderController : ApiController
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET: api/Order
        public IEnumerable<Order> Get()
        {
            return _orderService.GetOrders(User.Identity.GetUserId<int>());
        }

        // GET: api/Order/5
        public Order Get(int id)
        {
            return _orderService.GetOrder(User.Identity.GetUserId<int>(),id);
        }

        // POST: api/Order
        public bool Post(Order order)
        {
            return _orderService.CreateOrder(User.Identity.GetUserId<int>(),order);
        }

        // PUT: api/Order
        public bool Put(Order order)
        {
            return _orderService.UpdateOrder(User.Identity.GetUserId<int>(), order);
        }

        // DELETE: api/Order/5
        public bool Delete(int id)
        {
            return _orderService.DeleteOrder(id);
        }
    }
}
