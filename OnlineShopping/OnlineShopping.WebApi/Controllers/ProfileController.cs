﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Entities.Models;
using Microsoft.AspNet.Identity;
using OnlineShopping.BLL.Interfaces;

namespace OnlineShopping.WebApi.Controllers
{
    [Authorize]
    public class ProfileController : ApiController
    {
        private readonly IProfileService _profileService;

        public ProfileController(IProfileService profileService)
        {
            _profileService = profileService;
        }

        // GET: api/Profile
        public ClientProfile Get()
        {
            return _profileService.Get(User.Identity.GetUserId<int>());
        }

        // PUT: api/Profile/5
        public bool Put(ClientProfile profile)
        {
            return _profileService.Update(profile);
        }

        // DELETE: api/Profile/5
        public bool Delete(int id)
        {
            return _profileService.Delete(id);
        }
    }
}
