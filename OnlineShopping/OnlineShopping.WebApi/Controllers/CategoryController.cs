﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Entities.Models;
using OnlineShopping.BLL.Interfaces;

namespace OnlineShopping.WebApi.Controllers
{
    public class CategoryController : ApiController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        // GET: api/Category
        public IEnumerable<Category> Get()
        {
            return _categoryService.Get();
        }

        // GET: api/Category/5
        public Category Get(int id)
        {
            return _categoryService.Get(id);
        }

        // POST: api/Category
        public bool Post(Category category)
        {
            return _categoryService.Create(category);
        }

        // PUT: api/Category/5
        public bool Put(Category category)
        {
            return _categoryService.Update(category);
        }

        // DELETE: api/Category/5
        public bool Delete(int id)
        {
            return _categoryService.Delete(id);
        }
    }
}
