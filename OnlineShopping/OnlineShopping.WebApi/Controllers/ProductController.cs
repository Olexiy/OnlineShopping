﻿using System.Collections.Generic;
using System.Web.Http;
using Entities.Models;
using OnlineShopping.BLL.Interfaces;

namespace OnlineShopping.WebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductController : ApiController
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        // GET: api/Product?categoryName={categoryName}
        [AllowAnonymous]
        public IEnumerable<Product> Get(string categoryName)
        {
            return _productService.GetCategoryProducts(categoryName);
        }

        // GET: api/Product/5
        [AllowAnonymous]
        public Product Get(int id)
        {
            return _productService.GetProduct(id);
        }

        // POST: api/Product
        public bool Post(Product product)
        {
            return _productService.CreateProduct(product);
        }

        // PUT: api/Product/5
        public bool Put(Product product)
        {
            return _productService.UpdateProduct(product);
        }

        // DELETE: api/Product/5
        public bool Delete(int id)
        {
            return _productService.DeleteProduct(id);
        }
    }
}
