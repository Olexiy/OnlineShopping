﻿using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnlineShopping.WebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminOrderController : ApiController
    {
        private readonly IAdminOrderService _adminOrderService;

        public AdminOrderController(IAdminOrderService adminOrderService)
        {
            _adminOrderService = adminOrderService;
        }

        // GET: api/AdminOrder
        public IEnumerable<Order> Get()
        {
            return _adminOrderService.GetActiveOrders();
        }

        // PUT: api/AdminOrder/5
        public bool Put(Order order)
        {
            return _adminOrderService.UpdateOrder(order);
        }

    }
}
