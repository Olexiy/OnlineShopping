﻿using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace OnlineShopping.WebApi.Controllers
{
    public class ProductImageController : ApiController
    {
         private readonly IProductImageService _productImageService;
      
         public ProductImageController(IProductImageService productImageService)
         {
            _productImageService = productImageService;
         }
    
      // GET: api/ProductImage?productId={productId}
      public IEnumerable<string> Get(int productId)
      {
            return _productImageService.GetProductImages(productId);
      }

        // GET: api/ProductImage?imageName={imageName}
        public HttpResponseMessage Get(string imageName)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StreamContent(
                new FileStream("D:/Projects/OnlineShopping Resources/images/products/" + imageName, FileMode.Open)); 
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            return response;
        }

        // POST: api/ProductImage?productId={productId}&productTitle={productTitle}
        public bool Post(int productId, string productTitle)
        {
            return _productImageService.Create(productId, productTitle);
        }
    }
}
