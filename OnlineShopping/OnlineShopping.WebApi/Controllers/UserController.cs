﻿using System.Collections.Generic;
using System.Web.Http;
using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;


namespace OnlineShopping.WebApi.Controllers
{
    //[Authorize]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        //search by name
        // GET: api/User?nameQuery={nameQuery}
        public IEnumerable<User> Get(string nameQuery)
        {
            return _userService.GetUsers(nameQuery);
        }

        // GET: api/User/{id}
        [AllowAnonymous]
        public User Get(int id)
        {
            return _userService.GetUser(id);
        }

        [Authorize]
        public User Get()
        {
            return _userService.GetUser(User.Identity.GetUserId<int>());
        }

        // POST: api/User
        [AllowAnonymous]
        public bool Post(Registration registration)
        {
            return _userService.CreateUser(registration);
        }

        // PUT: api/User/
        public bool Put(User user)
        {
            return _userService.UpdateUser(user);
        }

        // DELETE: api/User/{id}
        public bool Delete(int id)
        {
            return _userService.DeleteUser(id);
        }
    }
}
