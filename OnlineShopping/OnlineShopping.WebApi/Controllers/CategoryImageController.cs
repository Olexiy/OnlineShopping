﻿using OnlineShopping.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace OnlineShopping.WebApi.Controllers
{
    public class CategoryImageController : ApiController
    {
        private readonly ICategoryService _categoryService;

        public CategoryImageController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        // GET: api/CategoryImage?categoryTitle={categoryTitle}
        public HttpResponseMessage Get(string categoryTitle)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StreamContent(
                new FileStream("D:/Projects/OnlineShopping Resources/images/categories/" + categoryTitle, FileMode.Open));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            return response;
        }

        // POST: api/CategoryImage?categoryId={categoryId}&categoryTitle={categoryTitle}
        public bool Post(int categoryId, string categoryTitle)
        {
            return _categoryService.CreateTitle(categoryId,categoryTitle);
        }
    }
}
