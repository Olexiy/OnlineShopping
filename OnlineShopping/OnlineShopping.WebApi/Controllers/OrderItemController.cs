﻿using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnlineShopping.WebApi.Controllers
{
    [Authorize]
    public class OrderItemController : ApiController
    {
        private readonly IOrderItemService _orderItemService;

        public OrderItemController(IOrderItemService orderItemService)
        {
            _orderItemService = orderItemService;
        }

        // GET: api/OrderItem?orderId={orderId}
        public IEnumerable<OrderItem> Get(int orderId)
        {
            return _orderItemService.Get(orderId);
        }

        // GET: api/OrderItem?orderId={orderId}&productId={productId}
        public OrderItem Get(int orderId, int productId)
        {
            return _orderItemService.Get(orderId,productId);
        }

        // POST: api/OrderItem
        public bool Post(OrderItem orderItem)
        {
            return _orderItemService.Create(orderItem);
        }

        // PUT: api/OrderItem/5
        public bool Put(OrderItem orderItem)
        {
            return _orderItemService.Update(orderItem);
        }

        // DELETE: api/OrderItem/5
        public bool Delete(int id)
        {
            return _orderItemService.Delete(id);
        }
    }
}
