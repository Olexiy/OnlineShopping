﻿using Entities.Models;
using OnlineShopping.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnlineShopping.WebApi.Controllers
{
    public class ProductCharacteristicsController : ApiController
    {
        private readonly IProductCharacteristicsService _productCharService;

        public ProductCharacteristicsController(IProductCharacteristicsService productCharService)
        {
            _productCharService = productCharService;
        }

        // GET: api/ProductCharacteristics?productId={productId}
        public IEnumerable<ProductCharacteristics> Get(int productId)
        {
            return _productCharService.Get(productId);
        }

        // POST: api/ProductCharacteristics
        public bool Post(ProductCharacteristics productChar)
        {
            return _productCharService.Create(productChar);
        }

        // PUT: api/ProductCharacteristics/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ProductCharacteristics/5
        public void Delete(int id)
        {
        }
    }
}
