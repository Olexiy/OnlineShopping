﻿using System.Web.Http;
using OnlineShopping.BLL.Infrastructure;
using OnlineShopping.WebApi.Tools;

namespace OnlineShopping.WebApi
{
    public class DependencyConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.DependencyResolver = ServicesLocator.Register(container => new UnityResolver(container));
        }
    }
}