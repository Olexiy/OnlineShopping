﻿using System;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using OnlineShopping.BLL.Infrastructure;
using OnlineShopping.DAL.Provides;
using OnlineShopping.WebApi;
using Owin;


[assembly: OwinStartup(typeof(Startup))]

namespace OnlineShopping.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(DependencyConfig.Register);

            app.UseOAuthAuthorizationServer(BuildOAuthServerOption());
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(GlobalConfiguration.Configuration);

            //app.UseCookieAuthentication(new CookieAuthenticationOptions
            //{
            //    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
            //    LoginPath = new PathString("/Account/Login"),
            //});
        }

        private static OAuthAuthorizationServerOptions BuildOAuthServerOption()
        {
            return new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = ServicesLocator.GlobalResolver.Resolve<AuthorizationServerProvider>()
            };
        }
    }
}