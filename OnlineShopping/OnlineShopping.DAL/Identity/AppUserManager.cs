﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.Models;

namespace OnlineShopping.DAL.Identity
{
    public class AppUserManager:UserManager<AppUser,int>
    {
        public AppUserManager(AppUserStore store) : base(store)
        {
        }

        public Task<int> MyDeleteUserAsync(AppUser user, DatabaseContext db)
        {
            db.Users.Remove(db.Users.Include(u => u.ClientProfile).Single(u => u.Id == user.Id));
            return db.SaveChangesAsync();
        }
    }
}
