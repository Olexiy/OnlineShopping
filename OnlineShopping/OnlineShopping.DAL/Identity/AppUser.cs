﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.States;
using Microsoft.AspNet.Identity.EntityFramework;
using OnlineShopping.DAL.Models;

namespace OnlineShopping.DAL.Identity
{
    public class AppUser:IdentityUser<int, AppUserLogin, AppUserRole, AppUserClaim>
    {

        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        public CustomRole UserRole { get; set; }

        public virtual DbClientProfile ClientProfile { get; set; }
        public virtual ICollection<DbOrder> Orders { get; set; } = new List<DbOrder>();
    }
}
