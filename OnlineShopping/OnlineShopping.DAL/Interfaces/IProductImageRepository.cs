﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.DAL.Interfaces
{
    public interface IProductImageRepository
    {
        bool Create(int productId, string productTitle);
        string Get(int productId);
        IEnumerable<string> GetProductImages(int productId);
    }
}
