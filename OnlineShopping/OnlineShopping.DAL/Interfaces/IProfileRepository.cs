﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace OnlineShopping.DAL.Interfaces
{
    public interface IProfileRepository
    {
        bool Update(ClientProfile clientProfile);
        bool Delete(int userId);
        ClientProfile Get(int userId);
    }
}
