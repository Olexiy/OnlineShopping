﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.DAL.Interfaces
{
    public interface IAdminOrderRepository
    {
        bool UpdateOrder(Order order);
        IEnumerable<Order> GetActiveOrders();
    }
}
