﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace OnlineShopping.DAL.Interfaces
{
    public interface ICategoryRepository
    {
        bool Create(Category category);
        bool Update(Category category);
        bool Delete(int id);
        Category Get(int id);
        IEnumerable<Category> Get();
        bool CreateTitle(int categoryId, string categoryTitle);
    }
}
