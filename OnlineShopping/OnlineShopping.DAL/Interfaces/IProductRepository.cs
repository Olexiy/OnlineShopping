﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace OnlineShopping.DAL.Interfaces
{
    public interface IProductRepository
    {
        bool CreateProduct(Product product);
        bool DeleteProduct(int productId);
        bool UpdateProduct(Product product);
        Product GetProduct(int productId);
        IEnumerable<Product> GetCategoryProducts(string categoryName);
    }
}
