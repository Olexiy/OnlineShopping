﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using OnlineShopping.DAL.Models;

namespace OnlineShopping.DAL.Interfaces
{
    public interface IOrderRepository
    {
        bool CreateOrder(int customerId,Order order);
        bool UpdateOrder(int customerId, Order order);
        bool DeleteOrder(int orderId);

        Order GetOrder(int customerId, int orderId);
        IEnumerable<Order> GetOrders(int userId);

        //void AddProduct(int orderId,int productId);
        //void DeleteProduct(int orderId, int productId);
    }
}
