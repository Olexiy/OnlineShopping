﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Microsoft.AspNet.Identity;

namespace OnlineShopping.DAL.Interfaces
{
    public interface IUserRepository
    {
        IdentityResult CreateUser(Registration registration);
        bool UpdateUser(User user);
        bool DeleteUser(int id);
        User GetUser(string login,string password);
        User GetUser(int id);
        List<User> GetUsers(string nameQuery);
    }
}
