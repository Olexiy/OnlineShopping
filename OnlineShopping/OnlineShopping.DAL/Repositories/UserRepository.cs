﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Entities.Models;
using Entities.States;
using Microsoft.AspNet.Identity;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.Identity;
using OnlineShopping.DAL.Interfaces;
using OnlineShopping.DAL.Models;

namespace OnlineShopping.DAL.Repositories
{
    public class UserRepository:IUserRepository
    {
        private readonly DatabaseContext _db;
        private readonly AppUserManager _appUserManager;
        private readonly IMapper _mapper;

        public UserRepository(DatabaseContext db, AppUserManager appUserManager, IMapper mapper)
        {
            _db = db;
            _appUserManager = appUserManager;
            _mapper = mapper;
        }

        public IdentityResult CreateUser(Registration registration)
        {
            if (registration == null) return IdentityResult.Failed();

            var profile = _mapper.Map<DbClientProfile>(registration.ClientProfile);
            var user = _mapper.Map<AppUser>(registration.Credential);
            user.ClientProfile = profile;
            user.UserRole = CustomRole.User;

            return _appUserManager.Create(user, registration.Credential.Password);
        }

        public bool UpdateUser(User user)
        {
            if (user == null) return false;
            var storedUser = _db.Users.Find(user.Id);
            var incomingUser = _mapper.Map<AppUser>(user);
            storedUser.FirstName = incomingUser.FirstName;
            storedUser.LastName = incomingUser.LastName;
            storedUser.UserName = incomingUser.UserName ?? storedUser.UserName;
            _db.SaveChanges();
            return true;
        }

        public bool DeleteUser(int id)
        {
            var appUser = _appUserManager.FindById(id);
            if (appUser == null) return false;
            _db.Users.Remove(_db.Users.Include(u => u.ClientProfile).Single(u => u.Id == appUser.Id));
            _db.SaveChanges();
            return true;
        }

        public User GetUser(string login, string password)
        {
            var appUser = _appUserManager.Find(login,password);
            return _mapper.Map<User>(appUser);
        }

        public User GetUser(int id)
        {
            var appUser = _appUserManager.FindById(id);
            return _mapper.Map<User>(appUser);
        }

        public List<User> GetUsers(string nameQuery)
        {
            var query = _db.Users.Where(u => u.FirstName.ToLower().Contains(nameQuery.ToLower())
                                           || u.LastName.ToLower().Contains(nameQuery.ToLower()));
            return _mapper.Map<List<User>>(query);
        }
    }
}
