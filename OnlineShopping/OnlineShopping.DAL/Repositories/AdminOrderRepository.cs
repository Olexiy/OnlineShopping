﻿using AutoMapper;
using Entities.Models;
using Entities.States;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.Interfaces;
using OnlineShopping.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.DAL.Repositories
{
    public class AdminOrderRepository:IAdminOrderRepository
    {
        private readonly DatabaseContext _db;
        private readonly IMapper _mapper;

        public AdminOrderRepository(DatabaseContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public IEnumerable<Order> GetActiveOrders()
        {
            var query = _db.Orders.Where(order => order.Status == OrderStatus.InProcess);
            var mappedOrder = _mapper.Map<List<Order>>(query.ToList());
            return mappedOrder.OrderBy(o => o.CreatedDate);
        }

        public bool UpdateOrder(Order order)
        {
            if (order == null) return false;
            var dbOrder = _db.Orders.First(o => o.Id == order.Id);
            dbOrder.Status = order.Status;
            _db.SaveChanges();
            return true;
        }
    }
}
