﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Entities.Models;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.Interfaces;
using System.Data.Entity;
using OnlineShopping.DAL.Models;

namespace OnlineShopping.DAL.Repositories
{
    public class ProfileRepository:IProfileRepository
    {
        private readonly DatabaseContext _db;
        private readonly IMapper _mapper;

        public ProfileRepository(DatabaseContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public bool Update(ClientProfile clientProfile)
        {
            if (clientProfile == null) return false;
            var appProfile = _mapper.Map<DbClientProfile>(clientProfile);
            _db.ClientProfiles.Attach(appProfile);
            _db.Entry(appProfile).State = EntityState.Modified;
            _db.SaveChanges();
            return true;
        }

        public bool Delete(int userId)
        {
            throw new NotImplementedException();
        }

        public ClientProfile Get(int userId)
        {
            var profile = _db.ClientProfiles.Single(p => p.AppUser.Id == userId);
            return _mapper.Map<ClientProfile>(profile);
        }
    }
}
