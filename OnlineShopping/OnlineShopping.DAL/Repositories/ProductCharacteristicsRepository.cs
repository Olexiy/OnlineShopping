﻿using AutoMapper;
using Entities.Models;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.EntityFramework.Models;
using OnlineShopping.DAL.Interfaces;
using OnlineShopping.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace OnlineShopping.DAL.Repositories
{
    public class ProductCharacteristicsRepository : IProductCharacteristicsRepository
    {
        private readonly DatabaseContext _db;
        private readonly IMapper _mapper;

        public ProductCharacteristicsRepository(DatabaseContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public bool Create(ProductCharacteristics productCharacteristics)
        {
            if (productCharacteristics == null) return false;
            var appProductChar = _mapper.Map<DbProductCharacteristics>(productCharacteristics);
            _db.ProductCharacteristics.Add(appProductChar);
            _db.SaveChanges();
            return true;
        }

        public bool Delete(int characteristicsId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductCharacteristics> Get(int productId)
        {
            var query = _db.ProductCharacteristics.Where(
                productChar => productChar.ProductId == productId);
        
            return _mapper.Map<List<ProductCharacteristics>>(query.ToList());
        }

        public bool Update(ProductCharacteristics productCharacteristics)
        {
            throw new NotImplementedException();
        }
    
    }
}
