﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Entities.Models;
using OnlineShopping.DAL.Models;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.Interfaces;
using OnlineShopping.DAL.EntityFramework.Models;
using System.Net.Http;
using System.Web.Http;
using System.Net;

namespace OnlineShopping.DAL.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly DatabaseContext _db;
        private readonly IMapper _mapper;

        public OrderRepository(DatabaseContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public bool CreateOrder(int customerId, Order order)
        {
            if (order == null) return false;
            var appOrder = _mapper.Map<DbOrder>(order);
            var orderItems = new List<DbOrderItem>();

            foreach (var orderItem in appOrder.OrderItems)
            {
                if (_db.Products.Any(p => p.Id == orderItem.ProductId))
                {
                    orderItem.Product = _db.Products.First(p => p.Id == orderItem.ProductId);
                    // orderItem.Price = orderItem.Product.Price * orderItem.Quantity;
                    orderItems.Add(orderItem);
                    // appOrder.Sum += orderItem.Price;
                }
                else
                {
                    var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("Error in init productId in orderItem")
                    };
                    throw new HttpResponseException(message);
                }
            }

            appOrder.CustomerId = customerId;
            appOrder.CreatedDate = DateTime.Now;
            appOrder.OrderItems = orderItems;
            _db.Orders.Add(appOrder);
            _db.SaveChanges();
            return true;
        }

        public bool UpdateOrder(int customerId, Order order)
        {
            if (order == null) return false;

            var incomingOrder = _mapper.Map<DbOrder>(order);
            var storedOrder = _db.Orders.Include(op => op.OrderItems).Single(se => se.Id == order.Id);

            if (incomingOrder.OrderItems.Count != 0)
            {
                foreach (var storedOrderItem in storedOrder.OrderItems.ToList())
                {
                    if (incomingOrder.OrderItems.All(i => i.Id != storedOrderItem.Id))
                    {
                        storedOrder.OrderItems.Remove(storedOrderItem);
                        _db.OrderItems.Remove(storedOrderItem);
                    }
                }

                foreach (var incomingOrderItem in incomingOrder.OrderItems.ToList())
                {
                    if (storedOrder.OrderItems.All(storedItem => storedItem.Id != incomingOrderItem.Id))
                    {
                        incomingOrderItem.Product = _db.Products.First(p => p.Id == incomingOrderItem.ProductId);
                        _db.OrderItems.Add(incomingOrderItem);
                        _db.Entry(incomingOrderItem).State = EntityState.Added;
                        storedOrder.OrderItems.Add(incomingOrderItem);
                    }
                }
            }
            storedOrder.ShipPhoneNumber = incomingOrder.ShipAddress ?? storedOrder.ShipPhoneNumber;
            storedOrder.ShipAddress = incomingOrder.ShipAddress ?? storedOrder.ShipAddress; 
            storedOrder.ShipEmail = incomingOrder.ShipEmail ?? storedOrder.ShipEmail;
            storedOrder.Status = incomingOrder.Status;
            storedOrder.CreatedDate = DateTime.Now;
            _db.SaveChanges();
            return true;
        }

        public bool DeleteOrder(int orderId)
        {
            var appOrder = _db.Orders.Find(orderId);
            var orderItems = _db.OrderItems.Where(i => i.OrderId == orderId);
            if (appOrder == null) return false;
            foreach (var orderItem in orderItems)
            {
                _db.OrderItems.Remove(orderItem);
            }
            _db.Orders.Remove(appOrder);
            _db.SaveChanges();
            return true;
        }

        public Order GetOrder(int customerId, int orderId)
        {
            var appOrder = _db.Orders.Find(orderId);
            var user = _db.Users.Find(customerId);
            if (appOrder.CustomerId == customerId || user.UserRole == Entities.States.CustomRole.Admin)
            {
                return _mapper.Map<Order>(appOrder);
            }
            return null;
        }

        public IEnumerable<Order> GetOrders(int userId)
        {
            var query = _db.Orders.Where(order => order.CustomerId == userId);
            var mappedOrder = _mapper.Map<List<Order>>(query.ToList());
            return mappedOrder.OrderByDescending(o => o.CreatedDate);
        }
    }
}
