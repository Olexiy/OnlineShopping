﻿using OnlineShopping.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using AutoMapper;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.EntityFramework.Models;
using System.Data.Entity;
using System.Net.Http;
using System.Web.Http;
using System.Net;

namespace OnlineShopping.DAL.Repositories
{
    public class OrderItemRepository : IOrderItemRepository
    {
        private readonly DatabaseContext _db;
        private readonly IMapper _mapper;

        public OrderItemRepository(DatabaseContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public bool Create(OrderItem orderItem)
        {
            if (orderItem == null) return false;
            var appOrderItem = _mapper.Map<DbOrderItem>(orderItem);
            appOrderItem.Product = _db.Products.First(p => p.Id == appOrderItem.ProductId);
           // appOrderItem.Price = appOrderItem.Quantity * appOrderItem.Product.Price;
            _db.OrderItems.Add(appOrderItem);
            _db.SaveChanges();
            return true;
        }

        public bool Delete(int orderItemId)
        {
            var appOrderItem = _db.OrderItems.Find(orderItemId);
            if (appOrderItem == null) return false;
            _db.OrderItems.Remove(appOrderItem);
            _db.SaveChanges();
            return true;
        }

        public OrderItem Get(int orderId, int productId)
        {
            var appOrderItem = _db.OrderItems.FirstOrDefault(i => i.OrderId == orderId && i.ProductId == productId);
            if(appOrderItem == null)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Error. Doesn't exist Order Item")
                };
                throw new HttpResponseException(message);
            } 

            return _mapper.Map<OrderItem>(appOrderItem);
        }

        public IEnumerable<OrderItem> Get(int orderId)
        {
            var query = _db.OrderItems.Where(i => i.OrderId == orderId);
            return _mapper.Map<List<OrderItem>>(query).ToList();
        }

        public bool Update(OrderItem orderItem)
        {
            if (orderItem == null) return false;
            var storedOrdeItem = _db.OrderItems.Find(orderItem.Id);
            var appOrderItem = _mapper.Map<DbOrderItem>(orderItem);
            if(appOrderItem.Quantity != storedOrdeItem.Quantity && appOrderItem.Quantity != 1)
            {
                storedOrdeItem.Quantity = appOrderItem.Quantity;
               // storedOrdeItem.Price = appOrderItem.Quantity * storedOrdeItem.Product.Price;
            }
            _db.SaveChanges();
            return true;
        }
    }
}
