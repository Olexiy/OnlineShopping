﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Entities.Models;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.EntityFramework.Models;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.DAL.Repositories
{
    public class CategoryRepository:ICategoryRepository
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _db;

        public CategoryRepository(IMapper mapper, DatabaseContext db)
        {
            _mapper = mapper;
            _db = db;
        }

        public bool Create(Category category)
        {
            if (category == null) return false;
            var appCategory = _mapper.Map<DbCategoty>(category);
            _db.Categoties.Add(appCategory);
            _db.SaveChanges();
            return true;
        }

        public bool Update(Category category)
        {
            if (category == null) return false;
            var appCategory = _mapper.Map<DbCategoty>(category);
            _db.Categoties.Attach(appCategory);
            _db.Entry(appCategory).State = EntityState.Modified;
            _db.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var appCategory = _db.Categoties.Find(id);
            if (appCategory == null) return false;
            _db.Categoties.Remove(appCategory);
            _db.SaveChanges();
            return true;
        }

        public Category Get(int id)
        {
            var appCategory = _db.Categoties.Find(id);
            return _mapper.Map<Category>(appCategory);
        }

        public IEnumerable<Category> Get()
        {
            var appCategories = _db.Categoties.ToList();
            return _mapper.Map<List<Category>>(appCategories);
        }

        public bool CreateTitle(int categoryId, string categoryTitle)
        {
            var category = _db.Products.First(p => p.Id == categoryId);
            category.ImageTitle = categoryTitle;
            return true;
        }
    }
}
