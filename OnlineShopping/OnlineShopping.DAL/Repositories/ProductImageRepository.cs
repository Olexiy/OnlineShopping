﻿using OnlineShopping.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using OnlineShopping.DAL.EntityFramework;
using AutoMapper;

namespace OnlineShopping.DAL.Repositories
{
    public class ProductImageRepository : IProductImageRepository
    {
        private readonly DatabaseContext _db;
        private readonly IMapper _mapper;

        public ProductImageRepository(DatabaseContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public bool Create(int productId, string productTitle)
        {
            var product = _db.Products.First(p => p.Id == productId);
            product.ImageTitle = productTitle;
            return true;
        }

        public string Get(int productId)
        {
            var product = _db.Products.First(p => p.Id == productId);
            return product.ImageTitle;
        }

        public IEnumerable<string> GetProductImages(int productId)
        {
            var product = _db.Products.First(p => p.Id == productId);
            return product.ImagesList;
        }
    }
}
