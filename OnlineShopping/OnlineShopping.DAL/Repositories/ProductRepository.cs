﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Entities.Models;
using OnlineShopping.DAL.Models;
using OnlineShopping.DAL.EntityFramework;
using OnlineShopping.DAL.EntityFramework.Models;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.DAL.Repositories
{
    public class ProductRepository:IProductRepository
    {
        private readonly DatabaseContext _db;
        private readonly IMapper _mapper;

        public ProductRepository(DatabaseContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public bool CreateProduct(Product product)
        {
            if (product == null) return false;
            var appProduct = _mapper.Map<DbProduct>(product);
            _db.Products.Add(appProduct);
            _db.SaveChanges();
            return true;
        }

        public bool DeleteProduct(int productId)
        {
            var appProduct = _db.Products.Find(productId);
            if (appProduct == null) return false;
            _db.Products.Remove(appProduct);
            _db.SaveChanges();
            return true;
        }

        public bool UpdateProduct(Product product)
        {
            if (product == null) return false;
            var appProduct = _mapper.Map<DbProduct>(product);
            _db.Products.Attach(appProduct);
            _db.Entry(appProduct).State = EntityState.Modified;
            _db.SaveChanges();
            return true;
        }

        public Product GetProduct(int productId)
        {
            var appProduct = _db.Products.Find(productId);
            return _mapper.Map<Product>(appProduct);
        }

        public IEnumerable<Product> GetCategoryProducts(string categoryName)
        {
            var appCategory = _db.Categoties.FirstOrDefault(category => category.Name == categoryName);

            var dbCategory = _mapper.Map<DbCategoty>(appCategory);

            if (dbCategory == null)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Not valid categoryName")
                };
                throw new HttpResponseException(message);
            }

            var query = _db.Products.Where(
                product => product.CategotyId == dbCategory.Id);

            var products = _mapper.Map<List<Product>>(query.ToList());

            return products;
        }

    }
}
