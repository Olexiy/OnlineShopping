﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using OnlineShopping.DAL.Models;
using Entities.Models;
using OnlineShopping.DAL.EntityFramework.Models;
using OnlineShopping.DAL.Identity;

namespace OnlineShopping.DAL.AutoMapper
{
    public class DataMapper
    {
        private static IMapper _mapper;

        public static IMapper GetInstance()
        {
            return _mapper ?? (_mapper = Configure().CreateMapper());
        }

        private static MapperConfiguration Configure()
        {
            return new MapperConfiguration(ex =>
            {
                ex.CreateMap<DbClientProfile, ClientProfile>();
                ex.CreateMap<ClientProfile, DbClientProfile>();

                ex.CreateMap<Credential, AppUser>()
                    .ForMember(user => user.UserName, option => option.MapFrom(credential => credential.UserName));

                ex.CreateMap<AppUser, User>();
                ex.CreateMap<User, AppUser>();

                ex.CreateMap<DbProduct, Product>();
                ex.CreateMap<Product, DbProduct>()
                      .ForMember(category => category.Category, option => option.Ignore())
                      .ForMember(order => order.OrderItems, option => option.Ignore());

                ex.CreateMap<DbOrderItem, OrderItem>();
                ex.CreateMap<OrderItem, DbOrderItem>()
                      .ForMember(item => item.Order, option => option.Ignore());

                ex.CreateMap<DbProductCharacteristics, ProductCharacteristics>();
                ex.CreateMap<ProductCharacteristics, DbProductCharacteristics>()
                      .ForMember(product => product.Product, option => option.Ignore());

                ex.CreateMap<DbOrder, Order>();
                ex.CreateMap<Order, DbOrder>()
                    .ForMember(order => order.Customer, option => option.Ignore());

                ex.CreateMap<Category, DbCategoty>();
                ex.CreateMap<DbCategoty, Category>();
            });
        }
    }
}
