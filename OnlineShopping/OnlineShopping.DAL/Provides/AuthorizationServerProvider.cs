﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Entities.States;
using Microsoft.Owin.Security.OAuth;
using OnlineShopping.DAL.Interfaces;

namespace OnlineShopping.DAL.Provides
{
    public class AuthorizationServerProvider: OAuthAuthorizationServerProvider
    {
        private readonly IUserRepository _userRepository;

        public AuthorizationServerProvider(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin",new [] {"*"});

            var user = await Task.Run(() => _userRepository.GetUser(context.UserName, context.Password));

            if (user == null)
            {               
                context.SetError("invalid_grand","The user name or password is incorect");
                return;
            }


            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()));
            //identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
            //identity.AddClaim(identity.HasClaim(x => x.Type == ClaimTypes.Role && x.Value == "Admin")
            //    ? new Claim(ClaimTypes.Role, "Admin")
            //    : new Claim(ClaimTypes.Role, "user"));

            identity.AddClaim(user.UserRole == CustomRole.Admin
               ? new Claim(ClaimTypes.Role, "Admin")
               : new Claim(ClaimTypes.Role, "User"));


            context.Validated(identity);
        }
    }
}
