﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using OnlineShopping.DAL.EntityFramework.Models;
using OnlineShopping.DAL.Identity;
using OnlineShopping.DAL.Models;

namespace OnlineShopping.DAL.EntityFramework
{
    using System;
    using System.Data.Entity;

    public class DatabaseContext: IdentityDbContext<AppUser,AppRole,int,AppUserLogin,AppUserRole,AppUserClaim>
    {
        public DbSet<DbOrder> Orders { get; set; }
        public DbSet<DbOrderItem> OrderItems { get; set; }
        public DbSet<DbProduct> Products { get; set; }
        public DbSet<DbClientProfile> ClientProfiles { get; set; }
        public DbSet<DbCategoty> Categoties { get; set; }
        public DbSet<DbProductCharacteristics> ProductCharacteristics { get; set; }

        static DatabaseContext()
        {
            Database.SetInitializer<DatabaseContext>(new DatabaseInitializer());
        }

        public DatabaseContext() : base("OnlineShopping")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("dbo");

            var appUserConfig = modelBuilder.Entity<AppUser>();
            appUserConfig.ToTable("Users").HasKey(u => u.Id);

            var appOrderItem = modelBuilder.Entity<DbOrderItem>();
            appOrderItem.ToTable("OrderItem").HasKey(u => u.Id);

            //One or zero to one or zero;
            appUserConfig.HasOptional(user => user.ClientProfile).WithRequired(profile => profile.AppUser);

            modelBuilder.Entity<DbClientProfile>().ToTable("ClientProfiles")
                .HasKey(profile => profile.Id);

            modelBuilder.Entity<DbOrder>().ToTable("Orders").HasKey(d => d.Id)
                .HasRequired(m => m.Customer).WithMany(u => u.Orders).HasForeignKey(a => a.CustomerId)
                .WillCascadeOnDelete();

           // appOrderItem.HasRequired(m => m.Product).WithMany(u => u.OrderItems)
           //    .HasForeignKey(a => a.ProductId).WillCascadeOnDelete();

           // appOrderItem.HasRequired(m => m.Order).WithMany(u => u.OrderItems)
           //    .HasForeignKey(a => a.OrderId).WillCascadeOnDelete();


            //modelBuilder.Entity<DbOrder>()
            //    .HasMany(m => m.Products).WithMany(u => u.Orders)
            //    .Map(m =>
            //    {
            //        m.MapLeftKey("OrderId");
            //        m.MapRightKey("ProductId");
            //        m.ToTable("OrdersProducts");
            //    });

            modelBuilder.Entity<DbCategoty>().ToTable("Category").HasKey(d => d.Id);

            modelBuilder.Entity<DbProduct>().ToTable("Products").HasKey(d => d.Id)
                .HasRequired(m => m.Category).WithMany(u => u.Products).HasForeignKey(a => a.CategotyId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<DbProductCharacteristics>().ToTable("ProductCharacteristics").HasKey(d => d.Id)
                .HasRequired(m => m.Product).WithMany(u => u.ProductCharacteristics).HasForeignKey(a => a.ProductId)
                .WillCascadeOnDelete();

            //modelBuilder.Entity<DbProduct>().ToTable("Products").HasKey(d => d.Id)
            //    .HasOptional(m => m.Order).WithMany(d => d.Products).HasForeignKey(m => m.OrderId)
            //    .WillCascadeOnDelete();
        }
    }
}
