﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Entities.States;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using OnlineShopping.DAL.EntityFramework.Models;
using OnlineShopping.DAL.Identity;
using OnlineShopping.DAL.Models;
using System.Collections.ObjectModel;
using static OnlineShopping.DAL.Models.DbOrder;

namespace OnlineShopping.DAL.EntityFramework
{
    public class DatabaseInitializer: DropCreateDatabaseIfModelChanges<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            var userManager = new AppUserManager(new AppUserStore(context));

            var user1 = new AppUser()
            {            
                UserName = "admin",
                FirstName = "Olexiy",
                LastName = "Fedorchak",
                UserRole = CustomRole.Admin,
                ClientProfile = new DbClientProfile()
                {
                    Address = "Bogo",
                    Gender = Gender.Male,
                    Info = "some info1",
                    DateOfBirth = new DateTime(1998, 1, 1)
                },
            };

            var user2 = new AppUser()
            {
                UserName = "user2",
                FirstName = "Ivan",
                LastName = "Ivanovich",
                UserRole = CustomRole.User,
                ClientProfile = new DbClientProfile()
                {
                    Address = "Lviv",
                    Gender = Gender.Male,
                    Info = "some info2",
                    DateOfBirth = new DateTime(1991, 3, 24)
                },
            };

            userManager.Create(user1,"123456");
            userManager.Create(user2, "qwerty");

            var category1 = new DbCategoty()
            {
                Name = "Phone",
                Image = "Phone.jpg"
            };

            var category2 = new DbCategoty()
            {
                Name = "Laptop",
                Image = "Laptop.jpg"
            };

            context.Categoties.Add(category1);
            context.Categoties.Add(category2);

            var product1 = new DbProduct()
            {
                Company = "Samsung",
                Model = "J7",
                Price = 5000,
                Category = category1,
                ImageTitle = "Samsung_J7/photo.jpg",
                ImagesList = new Collection<string>() { "Samsung_J7/photo.jpg","Samsung_J7/photo2.jpg", "Samsung_J7/photo3.jpg" }
            };

            var product2 = new DbProduct()
            {
                Company = "Apple",
                Model = "Iphone s5",
                Price = 10000,
                Category = category1,
                ImageTitle = "Apple_Iphone_s5/photo.jpg",
                ImagesList = new Collection<string>() {"Apple_Iphone_s5/photo.jpg"}
                };

            context.Products.Add(product1);
            context.Products.Add(product2);

            var pr1 = new DbProductCharacteristics()
            {
                Name = "Processor",
                Description = "MediaTek",
                Product = product1
            };

            var pr2 = new DbProductCharacteristics()
            {
                Name = "Camera",
                Description = "13Mpx",
                Product = product1
            };

            context.ProductCharacteristics.Add(pr1);
            context.ProductCharacteristics.Add(pr2);

            var orderItem1 = new DbOrderItem()
            {
                Product = product2,
                Quantity = 1
            };

            var orderItem2 = new DbOrderItem()
            {
               Product = product1,
               Quantity = 2
            };

            context.OrderItems.Add(orderItem1);
            context.OrderItems.Add(orderItem2);

            var order = new DbOrder()
            {
                Customer = user2,
                Status = OrderStatus.Finished,
                OrderItems = new List<DbOrderItem> { orderItem1, orderItem2 },
                ShipAddress = "Bogo",
                ShipEmail = "olexiyfed@gmail.com",
                CreatedDate = DateTime.Now
            };

            var order2 = new DbOrder()
            {
                Customer = user2,
                Status = OrderStatus.Active,
                OrderItems = new List<DbOrderItem> { orderItem1 },
                ShipAddress = "Bogo",
                ShipEmail = "olexiyfed@gmail.com",
                CreatedDate = DateTime.Now
            };

            context.Orders.Add(order);
            context.Orders.Add(order2);

            context.SaveChanges();
        }
    }
}
