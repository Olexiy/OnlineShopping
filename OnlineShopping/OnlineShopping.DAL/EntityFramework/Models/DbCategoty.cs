﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Newtonsoft.Json;
using OnlineShopping.DAL.Models;

namespace OnlineShopping.DAL.EntityFramework.Models
{
    public class DbCategoty
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public virtual ICollection<DbProduct> Products { get; set; } = new List<DbProduct>();
    }
}
