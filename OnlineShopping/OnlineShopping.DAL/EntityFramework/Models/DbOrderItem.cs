﻿using OnlineShopping.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.DAL.EntityFramework.Models
{
    public class DbOrderItem
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }

        public decimal Price {
            get {
                return Product.Price * Quantity;
            }
        }

        private int _quantity;
        public int Quantity
        {
            get { return _quantity == 0 ? 1 : _quantity; }
            set { _quantity = value; }
        }

        public virtual DbOrder Order { get; set; }
        public virtual DbProduct Product { get; set; }
    }
}
