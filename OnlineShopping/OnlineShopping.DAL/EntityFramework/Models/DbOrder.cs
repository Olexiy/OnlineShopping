﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineShopping.DAL.Identity;
using OnlineShopping.DAL.Models;
using OnlineShopping.DAL.EntityFramework.Models;
using Entities.States;

namespace OnlineShopping.DAL.Models
{
    public class DbOrder
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }


        private decimal _sum;
        public decimal Sum {
            get
            {
                foreach (var orderItem in OrderItems)
                {
                    _sum += orderItem.Price;
                }
                return _sum;
            }  
        }

        public DateTime? CreatedDate { get; set; }

        public OrderStatus Status { get; set; }

        public string ShipAddress { get; set; }

        public string ShipEmail { get; set; }

        public string ShipPhoneNumber { get; set; }

        public virtual AppUser Customer { get; set; }

        public virtual ICollection<DbOrderItem> OrderItems { get; set; } = new List<DbOrderItem>();
    }
}
