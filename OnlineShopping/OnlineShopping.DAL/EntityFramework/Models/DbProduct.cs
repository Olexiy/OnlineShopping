﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Newtonsoft.Json;
using OnlineShopping.DAL.EntityFramework.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.ObjectModel;

namespace OnlineShopping.DAL.Models
{
    public class DbProduct
    {
        public int Id { get; set; }

        public int CategotyId { get; set; }

        public string Model { get; set; }

        public string Company { get; set; }

        public string Description { get; set; }

        public string ImageTitle { get; set; }

        public decimal Price { get; set; }

        public bool? Status { get; set; }

        public virtual DbCategoty Category { get; set; }

        public virtual ICollection<DbOrderItem> OrderItems { get; set; } = new List<DbOrderItem>();

        public virtual ICollection<DbProductCharacteristics> ProductCharacteristics { get; set; } = new List<DbProductCharacteristics>();

        private string _stringArrayCore = string.Empty;

        public string StringArrayCore
        {
            get{ return _stringArrayCore;}
            set{_stringArrayCore = value;}
        }
        [NotMapped]
        public ICollection<string> ImagesList
        {
            get
            {
                var splitString = _stringArrayCore.Split(';');
                var stringArray = new Collection<string>();
                foreach (var s in splitString)
                {
                    stringArray.Add(s);
                }
                return stringArray;
            }
            set { _stringArrayCore = string.Join(";", value);}
        }

    }
}
