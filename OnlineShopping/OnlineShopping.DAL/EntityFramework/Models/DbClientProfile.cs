﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.States;
using OnlineShopping.DAL.Identity;

namespace OnlineShopping.DAL.Models
{
    public class DbClientProfile
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(250)]
        public string Info { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public Gender? Gender { get; set; }

        public virtual AppUser AppUser { get; set; }
        public virtual ICollection<DbOrder>Orders { get; set; } = new List<DbOrder>();
    }
}
