﻿using OnlineShopping.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.DAL.EntityFramework.Models
{
    public class DbProductCharacteristics
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual DbProduct Product { get; set; }
    }
}
