﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.States
{
    public enum Gender
    {
        Unknown,
        Male,
        Female
    }
}
