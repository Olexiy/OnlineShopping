﻿using Entities.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class Order
    {
        public int Id { get; set; }

        public decimal Sum { get; set; }

        public DateTime? CreatedDate { get; set; }

        //true - finished, false - active
        public OrderStatus Status { get; set; }

        public string ShipAddress { get; set; }

        public string ShipEmail { get; set; }

        public string ShipPhoneNumber { get; set; }

        public User Customer { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; } = new List<OrderItem>();
    }
}
