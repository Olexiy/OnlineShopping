﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Build.Framework;

namespace Entities.Models
{
    public class Product
    {
        public int Id { get; set; }

        public int CategotyId { get; set; }

        public string Model { get; set; }

        public string Company { get; set; }

        public string Description { get; set; }

        public string ImageTitle { get; set; }

        public decimal Price { get; set; }

        public bool Status { get; set; }
    }
}
