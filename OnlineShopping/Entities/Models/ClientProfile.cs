﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.States;

namespace Entities.Models
{
    public class ClientProfile
    {
        public int Id { get; set; }
        public string Info { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public Gender? Gender { get; set; }
    }
}
