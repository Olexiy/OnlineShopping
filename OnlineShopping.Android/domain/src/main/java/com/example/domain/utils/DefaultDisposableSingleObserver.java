package com.example.domain.utils;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by nz2dev on 19.08.2017
 */

public class DefaultDisposableSingleObserver<T> extends DisposableSingleObserver<T> {

    @Override
    public void onSuccess(T t) {
        // TODO default implementation, maybe log all event
    }

    @Override
    public void onError(Throwable e) {
        // TODO default implementation
    }
}
