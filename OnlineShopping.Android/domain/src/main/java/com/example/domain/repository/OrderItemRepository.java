package com.example.domain.repository;

import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by olexi on 1/31/2018
 */

public interface OrderItemRepository {
    Single<OrderItem> getOrderItem(int orderId, int productId);
    Single<List<OrderItem>> getOrderItems(int orderId);
    Single<Boolean> createOrderItem(OrderItem orderItem);
    Single<Boolean> updateOrderItem(OrderItem orderItem);
    Single<Boolean> deleteOrderItem(int id);
}
