package com.example.domain.interactor;


import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.repository.UserRepository;
import com.example.entity.models.User;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 12/20/2017
 */
@Singleton
public class UserInteractor extends BaseInteractor{

    private UserRepository userRepository;

    @Inject
    public UserInteractor(UserRepository userRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread){
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    public void getUser(int id, DisposableSingleObserver<User> observer){
        execute(userRepository.getUser(id),observer);
    }

    public void getUsers(String nameQuery, DisposableSingleObserver<List<User>> observer){
        execute(userRepository.getUsers(nameQuery),observer);
    }

    public void updateUser(User user, DisposableSingleObserver<Boolean> observer){
        execute(userRepository.updateUser(user),observer);
    }
}
