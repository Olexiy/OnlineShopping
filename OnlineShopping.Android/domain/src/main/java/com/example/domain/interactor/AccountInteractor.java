package com.example.domain.interactor;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.repository.AccountRepository;
import com.example.entity.models.Authorization;
import com.example.entity.models.Registration;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 12/20/2017
 */
@Singleton
public class AccountInteractor extends BaseInteractor {
    private AccountRepository accountRepository;

    @Inject
    public AccountInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, AccountRepository accountRepository) {
        super(threadExecutor, postExecutionThread);
        this.accountRepository = accountRepository;
    }

    public void signUp(Registration registration, DisposableSingleObserver<Boolean> observer){
        execute(accountRepository.signUp(registration),observer);
    }

    public void signIn(String login, String password, DisposableSingleObserver<Authorization> observer){
        execute(accountRepository.signIn(login,password),observer);
    }
}
