package com.example.domain.repository;

import com.example.entity.models.Category;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by olexi on 1/5/2018
 */

public interface CategoriesRepository {
    Single<Category> getCategory(int id);
    Single<List<Category>> getCategories();
}
