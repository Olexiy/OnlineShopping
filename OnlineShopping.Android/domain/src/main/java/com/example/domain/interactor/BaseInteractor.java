package com.example.domain.interactor;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 12/20/2017
 */

public class BaseInteractor {

    private ThreadExecutor threadExecutor;
    private PostExecutionThread postExecutionThread;

    //unsubscribe from all threads
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public BaseInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
    }

    protected <T> void execute(Single<T> observable, DisposableSingleObserver<T> observer){
        if(compositeDisposable == null || compositeDisposable.isDisposed()){
            compositeDisposable = new CompositeDisposable();
        }

        Single<T> signedObserable = observable
                .subscribeOn(threadExecutor.getSubscribeScheduler())
                .observeOn(postExecutionThread.getObserveScheduler());

        compositeDisposable.add(signedObserable.subscribeWith(observer));
    }

    public void dispose(){
        if(!compositeDisposable.isDisposed()){
            compositeDisposable.dispose();
        }
    }
}
