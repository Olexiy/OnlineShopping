package com.example.domain.executor;

import io.reactivex.Scheduler;

/**
 * Created by olexi on 12/20/2017
 */

public interface PostExecutionThread {
    Scheduler getObserveScheduler();
}
