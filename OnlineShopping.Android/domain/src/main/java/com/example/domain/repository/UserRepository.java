package com.example.domain.repository;

import com.example.entity.models.User;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by olexi on 12/20/2017
 */

public interface UserRepository {
    Single<User> getUser(int id);
    Single<List<User>> getUsers(String nameQuery);
    Single<Boolean> updateUser(User user);
}
