package com.example.domain.executor;

import java.util.concurrent.Executor;

import io.reactivex.Scheduler;

/**
 * Created by olexi on 12/20/2017
 */

public interface ThreadExecutor extends Executor {
    Scheduler getSubscribeScheduler();
}
