package com.example.domain.interactor;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.repository.OrderItemRepository;
import com.example.domain.repository.OrderRepository;
import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/31/2018
 */
@Singleton
public class OrderItemInteractor extends BaseInteractor{

    private OrderItemRepository repository;

    @Inject
    public OrderItemInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, OrderItemRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    public void getOrderItem(int orderId, int productId , DisposableSingleObserver<OrderItem> observer){
        execute(repository.getOrderItem(orderId,productId),observer);
    }

    public void getOrderItems(int orderId,DisposableSingleObserver<List<OrderItem>> observer){
        execute(repository.getOrderItems(orderId),observer);
    }

    public void createOrderItem(OrderItem orderItem, DisposableSingleObserver<Boolean> observer){
        execute(repository.createOrderItem(orderItem),observer);
    }

    public void updateOrderItem(OrderItem orderItem, DisposableSingleObserver<Boolean> observer){
        execute(repository.updateOrderItem(orderItem),observer);
    }

    public void deleteOrderItem(int id, DisposableSingleObserver<Boolean> observer){
        execute(repository.deleteOrderItem(id),observer);
    }
}
