package com.example.domain.repository;

import com.example.entity.models.Order;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by olexi on 2/12/2018
 */

public interface AdminRepository {
    Single<List<Order>> getAdminOrders();
    Single<Boolean> updateAdminOrder(Order order);
}
