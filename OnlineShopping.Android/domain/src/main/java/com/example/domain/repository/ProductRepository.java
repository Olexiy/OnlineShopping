package com.example.domain.repository;

import com.example.entity.models.Product;
import com.example.entity.models.ProductCharacteristics;

import java.util.List;

import javax.xml.ws.Response;

import io.reactivex.Single;

/**
 * Created by olexi on 12/20/2017
 */

public interface ProductRepository {
    Single<Product> getProduct(int id);
    Single<List<Product>> getProductsList(String categoryName);
    Single<Boolean> createProduct(Product product);
    Single<Boolean> updateProduct(Product product);
    Single<Boolean> deleteProduct(int id);
    Single<List<String>> getProductImages(int productId);
    Single<List<ProductCharacteristics>> getProductCharacteristics(int productId);
}
