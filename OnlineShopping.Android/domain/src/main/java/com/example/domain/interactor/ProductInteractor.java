package com.example.domain.interactor;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.repository.ProductRepository;
import com.example.entity.models.Category;
import com.example.entity.models.Product;
import com.example.entity.models.ProductCharacteristics;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/10/2018
 */
@Singleton
public class ProductInteractor extends BaseInteractor{

    private ProductRepository repository;

    @Inject
    public ProductInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, ProductRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    public void getProduct(int id, DisposableSingleObserver<Product> observer){
        execute(repository.getProduct(id),observer);
    }

    public void getProductsList(String categoryName, DisposableSingleObserver<List<Product>> observer){
        execute(repository.getProductsList(categoryName),observer);
    }

    public void createProduct(Product product, DisposableSingleObserver<Boolean> observer){
        execute(repository.createProduct(product),observer);
    }

    public void updateProduct(Product product, DisposableSingleObserver<Boolean> observer){
        execute(repository.updateProduct(product),observer);
    }

    public void deleteProduct(int id, DisposableSingleObserver<Boolean> observer){
        execute(repository.deleteProduct(id),observer);
    }

    public void getProductImages(int productId, DisposableSingleObserver<List<String>> observer){
        execute(repository.getProductImages(productId),observer);
    }

    public void getProductCharacteristics(int productId, DisposableSingleObserver<List<ProductCharacteristics>> observer){
        execute(repository.getProductCharacteristics(productId),observer);
    }
}
