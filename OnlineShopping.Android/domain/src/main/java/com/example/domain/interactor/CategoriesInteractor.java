package com.example.domain.interactor;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.repository.CategoriesRepository;
import com.example.entity.models.Category;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/5/2018
 */
@Singleton
public class CategoriesInteractor extends BaseInteractor{
    private CategoriesRepository repository;

    @Inject
    public CategoriesInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, CategoriesRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    public void getCategory(int id, DisposableSingleObserver<Category> observer){
        execute(repository.getCategory(id),observer);
    }

    public void getCategories(DisposableSingleObserver<List<Category>> observer){
        execute(repository.getCategories(),observer);
    }
}
