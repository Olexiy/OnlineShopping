package com.example.domain.interactor;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.repository.AdminRepository;
import com.example.entity.models.Category;
import com.example.entity.models.Order;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 2/12/2018
 */
@Singleton
public class AdminInteractor extends BaseInteractor{

    private AdminRepository repository;

    @Inject
    public AdminInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, AdminRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    public void getAdminOrders(DisposableSingleObserver<List<Order>> observer){
        execute(repository.getAdminOrders(),observer);
    }

    public void updateAdminOrder(Order order, DisposableSingleObserver<Boolean> observer){
        execute(repository.updateAdminOrder(order),observer);
    }

}
