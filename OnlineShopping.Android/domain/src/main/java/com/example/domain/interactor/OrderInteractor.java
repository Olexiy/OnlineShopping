package com.example.domain.interactor;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.repository.OrderRepository;
import com.example.entity.models.Order;
import com.example.entity.models.Product;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/21/2018
 */
@Singleton
public class OrderInteractor extends BaseInteractor{
    private OrderRepository repository;

    @Inject
    public OrderInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, OrderRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    public void getOrder(int id, DisposableSingleObserver<Order> observer){
        execute(repository.getOrder(id),observer);
    }

    public void getOrders(DisposableSingleObserver<List<Order>> observer){
        execute(repository.getOrders(),observer);
    }

    public void createOrder(Order order, DisposableSingleObserver<Boolean> observer){
        execute(repository.createOrder(order),observer);
    }

    public void updateOrder(Order order, DisposableSingleObserver<Boolean> observer){
        execute(repository.updateOrder(order),observer);
    }

    public void deleteOrder(int id, DisposableSingleObserver<Boolean> observer){
        execute(repository.deleteOrder(id),observer);
    }
}
