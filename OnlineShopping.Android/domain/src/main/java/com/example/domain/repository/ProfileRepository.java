package com.example.domain.repository;

import com.example.entity.models.ClientProfile;
import com.example.entity.models.Product;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by olexi on 2/13/2018
 */

public interface ProfileRepository {
    Single<ClientProfile> getProfile();
    Single<Boolean> updateProfile(ClientProfile clientProfile);
    Single<Boolean> deleteProfile(int id);
}
