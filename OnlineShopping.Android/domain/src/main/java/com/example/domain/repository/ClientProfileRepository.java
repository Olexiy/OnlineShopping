package com.example.domain.repository;

import com.example.entity.models.ClientProfile;

import io.reactivex.Single;

/**
 * Created by olexi on 12/20/2017
 */

public interface ClientProfileRepository {
    Single<ClientProfile> getProfile(int userId);
}
