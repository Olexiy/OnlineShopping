package com.example.domain.repository;

import com.example.entity.models.Authorization;
import com.example.entity.models.Registration;

import io.reactivex.Single;

/**
 * Created by olexi on 12/20/2017
 */

public interface AccountRepository {
    Single<Authorization> signIn(String login, String password);
    Single<Boolean> signUp(Registration registration);
}
