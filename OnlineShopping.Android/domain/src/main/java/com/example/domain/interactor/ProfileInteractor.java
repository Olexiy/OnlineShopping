package com.example.domain.interactor;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.repository.ProfileRepository;
import com.example.entity.models.ClientProfile;
import com.example.entity.models.Product;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 2/13/2018
 */
@Singleton
public class ProfileInteractor extends BaseInteractor{

    private ProfileRepository repository;

    @Inject
    public ProfileInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, ProfileRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    public void getProfile(DisposableSingleObserver<ClientProfile> observer){
        execute(repository.getProfile(),observer);
    }

    public void updateProfile(ClientProfile clientProfile, DisposableSingleObserver<Boolean> observer){
        execute(repository.updateProfile(clientProfile),observer);
    }

    public void deleteProfile(int id, DisposableSingleObserver<Boolean> observer){
        execute(repository.deleteProfile(id),observer);
    }}

