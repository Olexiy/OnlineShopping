package com.example.domain.repository;

import com.example.entity.models.Order;
import com.example.entity.models.Product;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by olexi on 1/21/2018
 */

public interface OrderRepository {
    Single<Order> getOrder(int id);
    Single<List<Order>> getOrders();
    Single<Boolean> createOrder(Order order);
    Single<Boolean> updateOrder(Order order);
    Single<Boolean> deleteOrder(int id);
}
