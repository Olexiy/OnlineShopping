package com.example.domain.utils;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by nz2dev on 29.08.2017
 */

public class DefaultDisposableObserver<T> extends DisposableObserver<T> {
    @Override
    public void onNext(T value) {
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }
}
