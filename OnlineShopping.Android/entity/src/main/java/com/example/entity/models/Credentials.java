package com.example.entity.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by olexi on 12/20/2017
 */

public class Credentials {

    @SerializedName("FirstName")
    private String firstName;

    @SerializedName("LastName")
    private String lastName;

    @SerializedName("UserName")
    private String userName;

    @SerializedName("Password")
    private String password;

    public Credentials(String firstName, String lastName, String userName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
