package com.example.entity.state;

/**
 * Created by olexi on 12/20/2017
 */

public enum CustomRole {
    User,
    Admin
}
