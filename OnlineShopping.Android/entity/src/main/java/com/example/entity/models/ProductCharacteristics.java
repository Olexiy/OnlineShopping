package com.example.entity.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by olexi on 1/21/2018
 */

public class ProductCharacteristics {
    @SerializedName("Id")
    private int id;

    @SerializedName("ProductId")
    private int productId;

    @SerializedName("Name")
    private String name;

    @SerializedName("Description")
    private String description;

    public ProductCharacteristics(int id, int productId, String name, String description) {
        this.id = id;
        this.productId = productId;
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public int getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
