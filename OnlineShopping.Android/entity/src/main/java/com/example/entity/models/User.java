package com.example.entity.models;

import com.example.entity.state.CustomRole;
import com.google.gson.annotations.SerializedName;

/**
 * Created by olexi on 12/20/2017
 */

public class User {

    @SerializedName("Id")
    private int id;

    @SerializedName("UserName")
    private String userName;

    @SerializedName("FirstName")
    private String firstName;

    @SerializedName("LastName")
    private String lastName;

    @SerializedName("UserRole")
    private int userRole;

    public User(int id, String userName, String firstName, String lastName, int userRole) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userRole = userRole;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return getFirstName() + " " + getLastName();
    }

    public int getUserRole() {
        return userRole;
    }
}
