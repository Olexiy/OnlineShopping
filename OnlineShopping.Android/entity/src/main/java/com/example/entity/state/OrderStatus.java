package com.example.entity.state;

/**
 * Created by olexi on 1/28/2018
 */

public enum OrderStatus {
    Active,
    Finished,
    InProcess
}
