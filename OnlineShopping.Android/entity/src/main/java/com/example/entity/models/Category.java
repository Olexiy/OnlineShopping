package com.example.entity.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by olexi on 12/20/2017
 */

public class Category {
    @SerializedName("Id")
    private int id;

    @SerializedName("Name")
    private String name;

    @SerializedName("Image")
    private String image;

    public Category(String name, String image) {
        this.name = name;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }
}

