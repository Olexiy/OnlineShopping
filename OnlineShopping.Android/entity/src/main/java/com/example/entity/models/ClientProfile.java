package com.example.entity.models;

import com.example.entity.state.Gender;
import com.google.gson.annotations.SerializedName;

/**
 * Created by olexi on 12/20/2017
 */

public class ClientProfile {

    @SerializedName("Info")
    private String info;

    @SerializedName("DateOfBirth")
    private String dateOfBirth;

    @SerializedName("Address")
    private String address;

    @SerializedName("Gender")
    private int gender;

    public ClientProfile() {

    }

    public String getInfo() {
        return info;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public int getGender() {
        return gender;
    }

    public String getGenderString() {
        if(getGender() == Gender.Female.ordinal()){
            return "Female";
        }
        if(getGender() == Gender.Male.ordinal()){
            return "Male";
        }

        return "Unknown";
    }
}
