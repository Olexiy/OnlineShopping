package com.example.entity.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by olexi on 12/20/2017
 */

public class Product {
    @SerializedName("Id")
    private int id;

    @SerializedName("Model")
    private String model;

    @SerializedName("Company")
    private String company;

    @SerializedName("Description")
    private String description;

    @SerializedName("ImageTitle")
    private String ImageTitle;

    @SerializedName("Price")
    private int price;

    @SerializedName("Status")
    private boolean status;

    public Product(String model, String company, String description, String ImageTitle, int price, boolean status) {
        this.model = model;
        this.company = company;
        this.description = description;
        this.ImageTitle = ImageTitle;
        this.price = price;
        this.status = status;
    }


    public String getModel() {
        return model;
    }

    public String getCompany() {
        return company;
    }

    public String getDescription() {
        return description;
    }

    public String getImageTitle() {
        return ImageTitle;
    }

    public int getPrice() {
        return price;
    }

    public boolean isStatus() {
        return status;
    }

    @Override
    public String toString() {
        return getCompany() + " " + getModel();
    }

    public int getId() {
        return id;
    }
}
