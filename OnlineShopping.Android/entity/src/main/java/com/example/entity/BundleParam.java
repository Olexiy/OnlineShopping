package com.example.entity;

/**
 * Created by olexi on 12/20/2017
 */

public class BundleParam {
    public static final String USER_ID = "user_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String PRODUCT_ID = "product_id";
    public static final String ORDER_ID = "order_id";
    public static final String IS_ADMIN = "is_admin";
}
