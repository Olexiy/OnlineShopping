package com.example.entity.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by olexi on 12/20/2017
 */

public class Authorization {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("token_type")
    private String tokenType;


    public Authorization(String accessToken, String tokenType) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
    }


    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }
}
