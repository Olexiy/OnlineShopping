package com.example.entity.state;

/**
 * Created by olexi on 12/20/2017.
 */

public enum Gender {
    Unknown,
    Male,
    Female
}
