package com.example.entity.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by olexi on 1/28/2018
 */

public class OrderItem {

    @SerializedName("Id")
    private int id;

    @SerializedName("OrderId")
    private int orderId;

    @SerializedName("ProductId")
    private int productId;

    @SerializedName("Price")
    private int price;

    @SerializedName("Quantity")
    private int quantity;

    @SerializedName("Product")
    private Product product;

    public OrderItem(int orderId, int productId) {
        this.orderId = orderId;
        this.productId = productId;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getId() {
        return id;
    }

    public int getProductId() {
        return productId;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
