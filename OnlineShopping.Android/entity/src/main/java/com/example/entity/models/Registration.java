package com.example.entity.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by olexi on 12/20/2017
 */

public class Registration {

    @SerializedName("Credential")
    Credentials credentials;

    @SerializedName("ClientProfile")
    ClientProfile profile;

    public Registration(Credentials credentials, ClientProfile profile) {
        this.credentials = credentials;
        this.profile = profile;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public ClientProfile getProfile() {
        return profile;
    }
}
