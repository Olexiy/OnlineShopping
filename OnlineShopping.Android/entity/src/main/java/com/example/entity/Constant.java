package com.example.entity;

/**
 * Created by olexi on 12/20/2017
 */

public class Constant {
    public static final String USERID = "userId";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";

    public static String PRODUCT_GENERAL = "General";
    public static String PRODUCT_CHARACTERISTIC = "Characteristic";
    public static String PRODUCT_REVIEW = "Review";

    public static int ID_NOT_SET = -1;
}
