package com.example.entity;

/**
 * Created by olexi on 12/20/2017
 */

public class IntentExtraParam {
    public static final String USER_ID = "INTENT_PARAM_USER_ID";
    public static final String CATEGORY_NAME = "INTENT_PARAM_CATEGORY_NAME";
    public static final String  PRODUCT_ID = "INTENT_PARAM_PRODUCT_ID";
    public static final String  ORDER_ID = "INTENT_PARAM_ORDER_ID";
    public static final String  IS_ADMIN = "INTENT_PARAM_IS_ADMIN";
}
