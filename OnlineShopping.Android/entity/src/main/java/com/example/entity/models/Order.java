package com.example.entity.models;

import com.example.entity.state.OrderStatus;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by olexi on 12/20/2017
 */

public class Order {

    @SerializedName("Id")
    private int id;

    @SerializedName("Sum")
    private int sum;

    @SerializedName("CreatedDate")
    private String createdDate;

    @SerializedName("Status")
    private int status;

    @SerializedName("ShipAddress")
    private String shipAddress;

    @SerializedName("ShipEmail")
    private String shipEmail;

    @SerializedName("ShipPhoneNumber")
    private String shipPhoneNumber;

    @SerializedName("OrderItems")
    private List<OrderItem> orderItems ;

    public Order() {}

    public int getId() {
        return id;
    }

    public int getSum() {
        return sum;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public String getShipEmail() {
        return shipEmail;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public void setShipEmail(String shipEmail) {
        this.shipEmail = shipEmail;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems == null ? orderItems = new ArrayList<>() : orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public int getStatus() {
        return status;
    }


    public String getStatusString(int status) {
        if(status == OrderStatus.Active.ordinal()){
            return "Active";
        }
        if(status == OrderStatus.Finished.ordinal()){
            return "Finished";
        }
        if(status == OrderStatus.InProcess.ordinal()){
            return "InProcess";
        }
        return null;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getShipPhoneNumber() {
        return shipPhoneNumber;
    }

    public void setShipPhoneNumber(String shipPhoneNumber) {
        this.shipPhoneNumber = shipPhoneNumber;
    }

    public void setId(int id) {
        this.id = id;
    }
}
