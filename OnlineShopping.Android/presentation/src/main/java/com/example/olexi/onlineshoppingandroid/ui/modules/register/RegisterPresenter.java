package com.example.olexi.onlineshoppingandroid.ui.modules.register;

import com.example.domain.interactor.AccountInteractor;
import com.example.entity.models.Registration;
import com.example.olexi.onlineshoppingandroid.common.account.AccountInterceptor;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 2/9/2018
 */

public class RegisterPresenter extends BasePresenter<RegisterView>{

    private AccountInteractor accountInteractor;

    @Inject
    public RegisterPresenter(AccountInteractor accountInteractor){
        this.accountInteractor = accountInteractor;
    }

    public void signUp(Registration registration){
        view.showProgressIndicator(true);
        accountInteractor.signUp(registration, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {
                view.showProgressIndicator(false);
                view.succeedRegister();
            }

            @Override
            public void onError(Throwable e) {
                view.showProgressIndicator(false);
                view.showError(e.toString());
            }
        });
    }
}
