package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.basket.BasketFragment;

import dagger.Component;

/**
 * Created by olexi on 1/26/2018
 */
@ActivityScope
@Component(dependencies = AppComponent.class)
public interface BasketComponent {
    void inject(BasketFragment basketFragment);
}
