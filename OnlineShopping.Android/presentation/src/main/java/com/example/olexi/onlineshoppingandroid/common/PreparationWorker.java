package com.example.olexi.onlineshoppingandroid.common;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.example.data.api.RestApiService;
import com.example.domain.interactor.AccountInteractor;
import com.example.domain.interactor.UserInteractor;
import com.example.entity.models.Authorization;
import com.example.entity.models.User;
import com.example.entity.state.CustomRole;
import com.example.olexi.onlineshoppingandroid.common.account.AccountInterceptor;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.ui.navigation.Navigator;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 12/20/2017
 */
@Singleton
public class PreparationWorker {
    private final Navigator navigator;
    private final AccountPreferences accountPreferences;
    private final AccountInterceptor accountInterceptor;

    private final UserInteractor userInteractor;
    private AccountInteractor accountInteractor;

    private Activity currentSource;
    private IndicativeView view;

    @Inject
    public PreparationWorker(Navigator navigator, AccountPreferences accountPreferences, AccountInterceptor accountInterceptor, UserInteractor userInteractor, AccountInteractor accountInteractor){
        this.navigator = navigator;
        this.accountPreferences = accountPreferences;
        this.accountInterceptor = accountInterceptor;
        this.userInteractor = userInteractor;
        this.accountInteractor = accountInteractor;
    }

    public void startPreparation(Activity source, IndicativeView view){
        this.currentSource = source;
        this.view = view;

        if(accountPreferences.isSigned()){
            view.showProgressIndicator(true);
            signIn();
        }else {
           navigator.navigateUserLogin(source);
        }
    }

    public void loginPreparation(Activity source, IndicativeView view, String token){
        this.currentSource = source;
        this.view = view;
        accountInterceptor.setToken(token);
        if(accountPreferences.isSigned()){
            loadUser();
        }else {
            view.showProgressIndicator(false);
        }
    }


    private void loadUser(){
        userInteractor.getUser(RestApiService.MAIN_USER_ID, new DisposableSingleObserver<User>() {
            @Override
            public void onSuccess(User user) {
                Log.d("Tag","Preparation succeed");
                accountPreferences.setUserId(user.getId());
                view.showProgressIndicator(false);
                startUser(user);
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Tag","Error on load user: " + e.getMessage());
                loadUser();
            }
        });
    }

    private void signIn(){
        accountInteractor.signIn(accountPreferences.getLogin(), accountPreferences.getPassword(), new DisposableSingleObserver<Authorization>() {
            @Override
            public void onSuccess(Authorization value) {
                accountInterceptor.setToken(value.getAccessToken());
                loadUser();
            }

            @Override
            public void onError(Throwable e) {
                signIn();
                Log.d("Tag","Error on signIn user: " + e.getMessage());
            }
        });
    }


    private void startUser(User user){
        if(user.getUserRole() == CustomRole.User.ordinal()){
            navigator.navigateCategories(currentSource);
        }
        if(user.getUserRole() == CustomRole.Admin.ordinal()){
            navigator.navigateAdmin(currentSource);
        }
    }


    public void logout(Activity context){
        accountPreferences.clearData();
        navigator.navigateUserLogin(context);
    }

}
