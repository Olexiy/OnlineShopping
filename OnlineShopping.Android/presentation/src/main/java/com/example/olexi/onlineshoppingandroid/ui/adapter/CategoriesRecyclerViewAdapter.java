package com.example.olexi.onlineshoppingandroid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.entity.models.Category;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.utils.AsyncTaskLoadImage;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/5/2018
 */

public class CategoriesRecyclerViewAdapter extends RecyclerView.Adapter<CategoriesRecyclerViewAdapter.CategoriesViewHolder>{

    private Context context;
    private ArrayList<Category> categories;
    private OnItemClickListener<Category> listener;

    public CategoriesRecyclerViewAdapter(Context context, OnItemClickListener<Category> listener) {
        this.context = context;
        this.listener = listener;
    }

    public void loadData(List<Category> categoriesList){
        if(categories == null){
            categories = new ArrayList<>();
        }
        categories.addAll(categoriesList);
        notifyDataSetChanged();
    }

    public void cleanData() {
        categories = new ArrayList<>();
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categories_list, parent, false);
        return new CategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder viewHolder, int position) {
        viewHolder.initData(context,categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories == null ? 0 : categories.size();
    }


    public class CategoriesViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ll_category_container)
        LinearLayout categoryContainer;

        @BindView(R.id.tv_category_name)
        TextView categoryName;

        @BindView(R.id.iv_category)
        ImageView categoryTitle;

        public CategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void initData(Context context, final Category category){
            categoryName.setText(category.getName());
            if(category.getImage() != null){
                showImageTitle(category.getImage());
            }else {
                categoryTitle.setImageResource(R.drawable.star);
            }

            categoryContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(category);
                }
            });
        }

        public void showImageTitle(String imageName) {
            String url = " http://192.168.0.104:8080/api/CategoryImage?categoryTitle=" + imageName;
            new AsyncTaskLoadImage(categoryTitle).execute(url);
        }
    }
}
