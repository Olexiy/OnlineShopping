package com.example.olexi.onlineshoppingandroid.ui.modules.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.entity.models.ClientProfile;
import com.example.entity.models.User;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.UserComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.utils.BoxLoaderView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 12/21/2017
 */

public class ClientProfileFragment extends BaseFragment implements ClientProfileView{
    @Inject
    ClientProfilePresenter presenter;
    @Inject
    AccountPreferences accountPreferences;

    @BindView(R.id.toolbar_not_scroll)
    Toolbar toolbar;

    @BindView(R.id.box_loader)
    BoxLoaderView boxLoaderView;

    @BindView(R.id.tv_first_name)
    TextView tvFirstName;

    @BindView(R.id.tv_user_last_name)
    TextView tvLastName;

    @BindView(R.id.tv_user_gender)
    TextView tvUserGender;

    @BindView(R.id.tv_user_email)
    TextView tvUserEmail;

    @BindView(R.id.tv_user_address)
    TextView tvUserAddress;

    @BindView(R.id.tv_user_birth)
    TextView tvUserBirth;

    @BindView(R.id.et_user_first_name)
    EditText etFirstName;

    @BindView(R.id.et_user_last_name)
    EditText etLastName;

    @BindView(R.id.et_user_email)
    EditText etUserEmail;

    @BindView(R.id.et_user_birth)
    EditText etUserBirth;

    @BindView(R.id.et_user_address)
    EditText etUserAddress;

    private  MenuItem itemEdit;
    private  MenuItem itemSave;
    private User user;
    private ClientProfile profile;
    public static ClientProfileFragment newInstance() {
        return new ClientProfileFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(UserComponent.class).inject(this);
        presenter.setView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_profile,container,false);
        ButterKnife.bind(this, view);
        presenter.prepareUser(accountPreferences.getUserId());
        getBaseActivity().setSupportActionBar(toolbar);
        getActivity(NavigationMenuActivity.class).setupToolbarDrawer(toolbar);
        setHasOptionsMenu(true);
        boxLoaderView.setLoaderColor(this.getResources().getColor(R.color.colorPrimary));
        boxLoaderView.setStrokeColor(this.getResources().getColor(R.color.colorPrimaryDark));
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile,menu);
        itemEdit = menu.findItem(R.id.action_edit);
        itemSave = menu.findItem(R.id.action_save);
        itemSave.setVisible(false);
        itemEdit.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                hideAllTextView();
                showAllEditText();
                itemEdit.setVisible(false);
                itemSave.setVisible(true);
                return true;
            case R.id.action_save:
                user.setFirstName(etFirstName.getText().toString());
                user.setLastName(etLastName.getText().toString());
                presenter.updateUser(user);
                itemSave.setVisible(false);
                itemEdit.setVisible(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void hideAllTextView(){
        tvFirstName.setVisibility(View.INVISIBLE);
        tvLastName.setVisibility(View.INVISIBLE);
        tvUserEmail.setVisibility(View.INVISIBLE);
        tvUserAddress.setVisibility(View.INVISIBLE);
        tvUserBirth.setVisibility(View.INVISIBLE);
    }

    public void hideAllEditText(){
        etFirstName.setVisibility(View.INVISIBLE);
        etLastName.setVisibility(View.INVISIBLE);
        etUserEmail.setVisibility(View.INVISIBLE);
        etUserAddress.setVisibility(View.INVISIBLE);
        etUserBirth.setVisibility(View.INVISIBLE);
    }

    public void showAllTextView(){
        tvFirstName.setVisibility(View.VISIBLE);
        tvLastName.setVisibility(View.VISIBLE);
        tvUserEmail.setVisibility(View.VISIBLE);
        tvUserAddress.setVisibility(View.VISIBLE);
        tvUserBirth.setVisibility(View.VISIBLE);
    }

    public void showAllEditText(){
        etFirstName.setVisibility(View.VISIBLE);
        etLastName.setVisibility(View.VISIBLE);
        etUserEmail.setVisibility(View.VISIBLE);
        etUserAddress.setVisibility(View.VISIBLE);
        etUserBirth.setVisibility(View.VISIBLE);

        etFirstName.setText(user.getFirstName());
        etLastName.setText(user.getLastName());
        etUserEmail.setText(user.getUserName());
        etUserAddress.setText(profile.getAddress());
        etUserBirth.setText(profile.getDateOfBirth());

        etFirstName.setSelection(etFirstName.getText().length());
        etLastName.setSelection(etLastName.getText().length());
        etUserEmail.setSelection(etUserEmail.getText().length());
        etUserAddress.setSelection(etUserAddress.getText().length());
        etUserBirth.setSelection(etUserBirth.getText().length());
    }

    @Override
    public void showUser(User user) {
        if(user != null){
            this.user = user;
            tvFirstName.setText(user.getFirstName());
            tvLastName.setText(user.getLastName());
            tvUserEmail.setText(user.getUserName());
            presenter.getProfile();
        }
    }

    @Override
    public void showProfile(ClientProfile clientProfile) {
        if(clientProfile != null){
            itemEdit.setVisible(true);
            this.profile = clientProfile;
            tvUserGender.setText(clientProfile.getGenderString());
            tvUserAddress.setText(clientProfile.getAddress() == null ? clientProfile.getAddress() : "Unknown");
            tvUserBirth.setText(clientProfile.getDateOfBirth() == null ? clientProfile.getDateOfBirth() : "Unknown");
        }
    }

    @Override
    public void updateUser() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }

    @Override
    public void showProgressIndicator(boolean condition) {
        boxLoaderView.setVisibility(condition ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showInfo(String message) {
        Log.d("Tag",message);
    }

    @Override
    public void showError(String errorMessage) {
        Log.d("Tag","Error " + errorMessage);
    }
}
