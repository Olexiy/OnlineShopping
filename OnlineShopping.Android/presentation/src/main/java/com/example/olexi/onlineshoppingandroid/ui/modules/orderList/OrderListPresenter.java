package com.example.olexi.onlineshoppingandroid.ui.modules.orderList;

import android.util.Log;

import com.example.domain.interactor.CategoriesInteractor;
import com.example.domain.interactor.OrderInteractor;
import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;
import com.example.entity.state.OrderStatus;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/21/2018
 */

public class OrderListPresenter extends BasePresenter<OrderListView>{

    private OrderInteractor interactor;

    @Inject
    public OrderListPresenter(OrderInteractor interactor) {
        this.interactor = interactor;
    }

    public void getFilterOrders(){
        interactor.getOrders(new DisposableSingleObserver<List<Order>>() {
            @Override
            public void onSuccess(List<Order> orders) {
                List<Order> finishedOrders = new ArrayList<>();
                for(Order order : orders){
                    if(order.getStatus() == OrderStatus.Finished.ordinal() || order.getStatus() == OrderStatus.InProcess.ordinal() ){
                        finishedOrders.add(order);
                    }
                }
                view.showFinishedOrderList(finishedOrders);
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }
}
