package com.example.olexi.onlineshoppingandroid.ui.modules.profile;

import com.example.domain.interactor.ProfileInteractor;
import com.example.domain.interactor.UserInteractor;
import com.example.entity.models.ClientProfile;
import com.example.entity.models.User;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 12/21/2017
 */
@ActivityScope
public class ClientProfilePresenter extends BasePresenter<ClientProfileView>{

    private final UserInteractor userInteractor;
    private final ProfileInteractor profileInteractor;

    @Inject
    ClientProfilePresenter(UserInteractor userInteractor, ProfileInteractor profileInteractor) {
        this.userInteractor = userInteractor;
        this.profileInteractor = profileInteractor;
    }


    void prepareUser(int id){
        view.showProgressIndicator(true);
        userInteractor.getUser(id, new DisposableSingleObserver<User>() {
            @Override
            public void onSuccess(User value) {
                view.showProgressIndicator(false);
                view.showUser(value);
            }

            @Override
            public void onError(Throwable e) {
                view.showProgressIndicator(false);
                view.showError(e.getMessage());
            }
        });
    }

    void updateUser(User user){
        view.showProgressIndicator(true);
        userInteractor.updateUser(user, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {
                view.showProgressIndicator(false);
                view.updateUser();
            }

            @Override
            public void onError(Throwable e) {
                view.showProgressIndicator(false);
                view.showError(e.getMessage());
            }
        });
    }

    void getProfile(){
        view.showProgressIndicator(true);
        profileInteractor.getProfile(new DisposableSingleObserver<ClientProfile>() {
            @Override
            public void onSuccess(ClientProfile profile) {
                view.showProgressIndicator(false);
                view.showProfile(profile);
            }

            @Override
            public void onError(Throwable e) {
                view.showProgressIndicator(false);
                view.showError(e.getMessage());
            }
        });
    }

    void updateProfile(ClientProfile clientProfile){
        profileInteractor.updateProfile(clientProfile, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {

            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }
}
