package com.example.olexi.onlineshoppingandroid.ui.modules.product.productCharacteristic;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.entity.BundleParam;
import com.example.entity.models.ProductCharacteristics;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.ProductComponent;
import com.example.olexi.onlineshoppingandroid.ui.adapter.ProductCharacteristicRecyclerViewAdapter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/25/2018
 */

public class ProductCharacteristicFragment extends BaseFragment implements ProductCharacteristicView{

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private LinearLayoutManager layoutManager;
    private ProductCharacteristicRecyclerViewAdapter adapter;

    @Inject
    ProductCharacteristicPresenter presenter;

    public static ProductCharacteristicFragment newInstance(int productId) {
        Bundle args = new Bundle();
        args.putInt(BundleParam.PRODUCT_ID,productId);
        ProductCharacteristicFragment fragment = new ProductCharacteristicFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(ProductComponent.class).inject(this);
        presenter.setView(this);
        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new ProductCharacteristicRecyclerViewAdapter(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_characteristic,container,false);
        ButterKnife.bind(this, view);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        presenter.getProductsCharacteristics(getProductId());
        return view;
    }

    @Override
    public void showCharacteristics(List<ProductCharacteristics> characteristics) {
        adapter.loadData(characteristics);
    }

    private int getProductId() {
        return getArguments().getInt(BundleParam.PRODUCT_ID);
    }
}
