package com.example.olexi.onlineshoppingandroid.ui.modules.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerRegisterComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.RegisterComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.DefaultActivity;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.ui.modules.login.LoginActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.login.LoginFragment;

import java.util.zip.Inflater;

/**
 * Created by olexi on 2/9/2018
 */

public class RegisterActivity extends DefaultActivity implements ComponentContainer<RegisterComponent>{

    public Intent getCallingIntent(Context context){
        return new Intent(context,RegisterActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentFragment(RegisterFragment.newInstance());
    }

    @Override
    public RegisterComponent buildComponent() {
        return DaggerRegisterComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public RegisterComponent getComponent() {
        return getSavedComponent();
    }

    void navigateLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
