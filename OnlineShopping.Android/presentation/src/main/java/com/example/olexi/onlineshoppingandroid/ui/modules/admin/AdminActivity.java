package com.example.olexi.onlineshoppingandroid.ui.modules.admin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.AdminComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerAdminComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerAppComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.DefaultActivity;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

/**
 * Created by olexi on 2/12/2018
 */

public class AdminActivity extends DefaultActivity implements ComponentContainer<AdminComponent>,
        OnItemClickListener<Order> {

    public static Intent getCallingIntent(Context context){
        return new Intent(context,AdminActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentFragment(AdminFragment.getInstance());
    }

    @Override
    public AdminComponent buildComponent() {
        return DaggerAdminComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public AdminComponent getComponent() {
        return getSavedComponent();
    }

    public void logout() {
        getAppDependencyComponent()
                .preparationWorker()
                .logout(this);
    }

    @Override
    public void onItemClick(Order order) {
        navigator.navigateOrderAdmin(this,order.getId());
    }
}
