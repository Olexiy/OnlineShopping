package com.example.olexi.onlineshoppingandroid.ui.modules.product;

import android.util.Log;

import com.example.domain.interactor.OrderInteractor;
import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 12/21/2017
 */
@ActivityScope
public class ProductPresenter extends BasePresenter<ProductView>{

}
