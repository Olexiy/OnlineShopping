package com.example.olexi.onlineshoppingandroid.ui.modules.productsList;

import android.util.Log;

import com.example.domain.interactor.ProductInteractor;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/10/2018
 */
@ActivityScope
public class ProductsListPresenter extends BasePresenter<ProductsListView> {

    private ProductInteractor interactor;

    @Inject
    public ProductsListPresenter(ProductInteractor interactor) {
        this.interactor = interactor;
    }

    public void getProductsList(String categoryName){
        interactor.getProductsList(categoryName, new DisposableSingleObserver<List<Product>>() {
            @Override
            public void onSuccess(List<Product> productList) {
                view.showProductsList(productList);
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }

}
