package com.example.olexi.onlineshoppingandroid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.entity.models.ProductCharacteristics;
import com.example.olexi.onlineshoppingandroid.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/21/2018
 */

public class ProductCharacteristicRecyclerViewAdapter extends RecyclerView.Adapter<ProductCharacteristicRecyclerViewAdapter.ProductViewHolder>{

    private Context context;
    private ArrayList<ProductCharacteristics> characteristics;

    public ProductCharacteristicRecyclerViewAdapter(Context context) {
        this.context = context;
    }

    public void loadData(List<ProductCharacteristics> characteristics){
        if(this.characteristics == null){
            this.characteristics = new ArrayList<>();
        }
        this.characteristics.addAll(characteristics);
        notifyDataSetChanged();
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_characteristic,parent,false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        holder.initData(characteristics.get(position), (position%2)!=0);
    }

    @Override
    public int getItemCount() {
        return characteristics == null ? 0 : characteristics.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_char_name)
        TextView charName;

        @BindView(R.id.tv_char_description)
        TextView charDescription;

        @BindView(R.id.ll_char_container)
        LinearLayout container;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void initData(ProductCharacteristics characteristic, boolean background){
            charName.setText(characteristic.getName());
            charDescription.setText(characteristic.getDescription());

            if(background){
                container.setBackgroundColor(context.getResources().getColor(R.color.background_main));
            }
        }
    }
}
