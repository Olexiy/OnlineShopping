package com.example.olexi.onlineshoppingandroid.ui.modules.orderForm;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.example.entity.BundleParam;
import com.example.entity.models.Order;
import com.example.entity.models.User;
import com.example.entity.state.OrderStatus;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.OrderFormComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.utils.BoxLoaderView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by olexi on 2/6/2018
 */

public class OrderFormFragment extends BaseFragment implements OrderFormView {

    @BindView(R.id.toolbar_with_back)
    Toolbar toolbar;

    @BindView(R.id.box_loader)
    BoxLoaderView boxLoaderView;

    @BindView(R.id.user_name_wrapper)
    TextInputLayout userNameWrapper;

    @BindView(R.id.ship_email_wrapper)
    TextInputLayout shipEmailWrapper;

    @BindView(R.id.ship_address_wrapper)
    TextInputLayout shipAddressWrapper;

    @BindView(R.id.ship_phone_wrapper)
    TextInputLayout shipPhoneNumberWrapper;

    @Inject
    OrderFormPresenter presenter;

    private Order order;
    private static final String EMAIL_PATTERN =   "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_PATTERN = "^\\+(?:[0-9]){4,}[0-9]$";
    private Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
    private Pattern phonePattern = Pattern.compile(PHONE_PATTERN);
    private Matcher matcher;

    public static OrderFormFragment newInstance(int orderId) {
        Bundle args = new Bundle();
        args.putInt(BundleParam.ORDER_ID,orderId);
        OrderFormFragment fragment = new OrderFormFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(OrderFormComponent.class).inject(this);
        presenter.setView(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_form,container,false);
        ButterKnife.bind(this,view);
        toolbar.setNavigationOnClickListener(toolbarListener);
        boxLoaderView.setLoaderColor(this.getResources().getColor(R.color.colorPrimary));
        boxLoaderView.setStrokeColor(this.getResources().getColor(R.color.colorPrimaryDark));
        presenter.getOrder(getOrderId());
        presenter.loadUser();
        return view;
    }

    private int getOrderId() {
        return getArguments().getInt(BundleParam.ORDER_ID);
    }

    @Override
    public void showInfo(String message) {
        Log.d("Tag","info " + message);
    }

    @Override
    public void showError(String errorMessage) {
        Log.d("Tag","errorMessage  " + errorMessage);
    }

    @Override
    public void showProgressIndicator(boolean condition) {
        boxLoaderView.setVisibility(condition? View.VISIBLE : View.INVISIBLE);
    }

    View.OnClickListener toolbarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getActivity(OrderFormActivity.class).onBackPressed();
        }
    };

    @Override
    public void showOrderDetail(Order order) {
        this.order = order;
        shipAddressWrapper.getEditText().setText(order.getShipAddress());
        shipEmailWrapper.getEditText().setText(order.getShipEmail());
        shipPhoneNumberWrapper.getEditText().setText(order.getShipPhoneNumber());
    }

    @Override
    public void showUserDetail(User user) {
            userNameWrapper.getEditText().setText(user.toString());
    }

    @Override
    public void navigateOrderList() {
        getActivity(OrderFormActivity.class).navigator.navigateOrderList(getActivity());
    }

    public boolean validateEmail(String email) {
        matcher = emailPattern.matcher(email);
        return matcher.matches();
    }

    public boolean validatePhone(String phone) {
        matcher = phonePattern.matcher(phone);
        return matcher.matches();
    }

    @OnClick(R.id.btn_to_order)
    public void clickToOrder(){
        if(order == null) {
            order = new Order();
            order.setId(getOrderId());
        }

        String email = shipEmailWrapper.getEditText().getText().toString();
        String phone = shipPhoneNumberWrapper.getEditText().getText().toString();
        String address = shipAddressWrapper.getEditText().getText().toString();

        if(!validateEmail(email)){
            shipEmailWrapper.setError("Not a valid email address!");
        }else if (!validatePhone(phone)){
            shipPhoneNumberWrapper.setError("Not a valid phone number!");
            shipEmailWrapper.setErrorEnabled(false);
        }
        else {
            shipEmailWrapper.setErrorEnabled(false);
            shipPhoneNumberWrapper.setErrorEnabled(false);
            order.setShipEmail(email);
            order.setShipPhoneNumber(phone);
            order.setShipAddress(address);
            order.setStatus(OrderStatus.InProcess.ordinal());
            presenter.updateOrder(order);
        }
    }
}
