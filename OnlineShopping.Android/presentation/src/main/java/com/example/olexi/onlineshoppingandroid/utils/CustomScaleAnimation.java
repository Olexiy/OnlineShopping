package com.example.olexi.onlineshoppingandroid.utils;

import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import com.example.olexi.onlineshoppingandroid.R;

/**
 * Created by olexi on 1/29/2018
 */

public class CustomScaleAnimation {

    private ImageView flipImage;
    private ScaleAnimation shrink, grow;
    private boolean isHeadsShrink,isHeadsGrow;
    private int upPicture,downPicture;

    public CustomScaleAnimation(ImageView flipImage) {
        this.flipImage = flipImage;
    }

    public void startAnimation(int upPicture, int downPicture){
        this.upPicture = upPicture;
        this.downPicture = downPicture;

        isHeadsShrink = true;
        isHeadsGrow = false;

        flipImage.setImageResource(upPicture);
        shrink = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(500);
        shrink.setAnimationListener(shrinkListener);

        grow = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        grow.setDuration(500);
        grow.setAnimationListener(growListener);

        flipImage.startAnimation(shrink);
    }

    public void cancelAnimation(){
        flipImage.animate().cancel();
    }

    Animation.AnimationListener shrinkListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {}

        @Override
        public void onAnimationEnd(Animation animation) {
            if (isHeadsShrink) {
                isHeadsShrink = false;
                flipImage.setImageResource(downPicture);
            } else {
                isHeadsShrink = true;
                flipImage.setImageResource(upPicture);
            }
            flipImage.startAnimation(grow);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {}
    };

    Animation.AnimationListener growListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {}

        @Override
        public void onAnimationEnd(Animation animation) {
            if (isHeadsGrow) {
                isHeadsGrow = false;
                flipImage.setImageResource(upPicture);
            } else {
                isHeadsGrow = true;
                flipImage.setImageResource(downPicture);
            }
            flipImage.startAnimation(shrink);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {}
    };
}
