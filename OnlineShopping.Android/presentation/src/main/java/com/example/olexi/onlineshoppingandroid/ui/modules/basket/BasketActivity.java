package com.example.olexi.onlineshoppingandroid.ui.modules.basket;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.entity.IntentExtraParam;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.BasketComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerBasketComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerOrderComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.order.OrderActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.order.OrderFragment;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

/**
 * Created by olexi on 1/26/2018
 */

public class BasketActivity extends NavigationMenuActivity implements ComponentContainer<BasketComponent>,
        OnItemClickListener<Product>{

    public static Intent getCallingIntent(Context context){
        return new Intent(context,BasketActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentFragment(BasketFragment.newInstance());
    }

    @Override
    public BasketComponent buildComponent() {
        return DaggerBasketComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public BasketComponent getComponent() {
        return getSavedComponent();
    }

    @Override
    public void onItemClick(Product product) {
        navigator.navigateProduct(this, product.getId());
    }

    public void navigateToOrderForm(int orderId){
        navigator.navigateOrderForm(this,orderId);
    }
}
