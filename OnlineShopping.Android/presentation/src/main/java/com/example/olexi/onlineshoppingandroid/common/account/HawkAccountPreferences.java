package com.example.olexi.onlineshoppingandroid.common.account;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.example.entity.Constant;
import com.orhanobut.hawk.Hawk;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by olexi on 12/19/2017
 */

@Singleton
public class HawkAccountPreferences implements AccountPreferences {

    @Inject
    public HawkAccountPreferences(){}

    @Override
    public void setUserId(int Id) {
        Hawk.put(Constant.USERID, Id);
    }

    @Override
    public int getUserId() {
        return Hawk.get(Constant.USERID);
    }

    @Override
    public void setLogin(String login) {
        Hawk.put(Constant.LOGIN, login);
    }

    @Override
    public String getLogin() {
        return Hawk.get(Constant.LOGIN);
    }

    @Override
    public void setPassword(String password) {
        Hawk.put(Constant.PASSWORD, password);
    }

    @Override
    public String getPassword() {
        return Hawk.get(Constant.PASSWORD);
    }

    @Override
    public boolean isSigned() {
        return !TextUtils.isEmpty(getLogin()) && !TextUtils.isEmpty(getPassword());
    }

    @Override
    public void clearData() {
        Hawk.delete(Constant.LOGIN);
        Hawk.delete(Constant.PASSWORD);
    }
}
