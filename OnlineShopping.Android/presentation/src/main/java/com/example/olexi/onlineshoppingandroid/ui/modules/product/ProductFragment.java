package com.example.olexi.onlineshoppingandroid.ui.modules.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.entity.BundleParam;
import com.example.entity.Constant;
import com.example.entity.models.Order;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.ProductComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.ProductsListComponent;
import com.example.olexi.onlineshoppingandroid.ui.adapter.ViewPagerAdapter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.product.productCharacteristic.ProductCharacteristicFragment;
import com.example.olexi.onlineshoppingandroid.ui.modules.product.productGeneral.ProductGeneralFragment;
import com.example.olexi.onlineshoppingandroid.ui.modules.product.productReview.ProductReviewFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by olexi on 12/21/2017
 */

public class ProductFragment  extends BaseFragment implements ProductView{
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private ViewPagerAdapter pagerAdapter;

    public static ProductFragment newInstance(int productId) {
        Bundle args = new Bundle();
        args.putInt(BundleParam.PRODUCT_ID,productId);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ProductFragment(){
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        pagerAdapter.addFragment(ProductGeneralFragment.newInstance(getProductId()), Constant.PRODUCT_GENERAL);
        pagerAdapter.addFragment(ProductCharacteristicFragment.newInstance(getProductId()), Constant.PRODUCT_CHARACTERISTIC);
        pagerAdapter.addFragment(new ProductReviewFragment(),Constant.PRODUCT_REVIEW);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product,container,false);
        ButterKnife.bind(this, view);

        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(pagerAdapter.getCount());
        tabLayout.setupWithViewPager(viewPager);

        getBaseActivity().setSupportActionBar(toolbar);
        getBaseActivity().getSupportActionBar().setTitle("Product");
        getActivity(NavigationMenuActivity.class).setupToolbarDrawer(toolbar);

        return view;
    }

    private int getProductId() {
        return getArguments().getInt(BundleParam.PRODUCT_ID);
    }
}
