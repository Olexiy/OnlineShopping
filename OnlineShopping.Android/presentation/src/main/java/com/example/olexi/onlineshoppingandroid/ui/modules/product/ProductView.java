package com.example.olexi.onlineshoppingandroid.ui.modules.product;

import com.example.entity.models.Order;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

import java.util.List;

/**
 * Created by olexi on 12/21/2017
 */

public interface ProductView extends BaseView{
}
