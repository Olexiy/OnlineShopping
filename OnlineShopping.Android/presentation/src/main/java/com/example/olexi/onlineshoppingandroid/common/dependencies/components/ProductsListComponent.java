package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.productsList.ProductsListFragment;

import dagger.Component;

/**
 * Created by olexi on 1/10/2018
 */
@ActivityScope
@Component(dependencies = AppComponent.class)
public interface ProductsListComponent {
    void inject(ProductsListFragment productsListFragment);
}
