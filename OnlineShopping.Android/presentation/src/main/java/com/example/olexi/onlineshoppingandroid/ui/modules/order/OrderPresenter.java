package com.example.olexi.onlineshoppingandroid.ui.modules.order;

import android.util.Log;

import com.example.domain.interactor.OrderInteractor;
import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;
import com.example.olexi.onlineshoppingandroid.ui.modules.orderList.OrderListView;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/23/2018
 */

public class OrderPresenter  extends BasePresenter<OrderView> {

    private OrderInteractor interactor;

    @Inject
    public OrderPresenter(OrderInteractor interactor) {
        this.interactor = interactor;
    }

    public void getOrder(int orderId){
        interactor.getOrder(orderId,new DisposableSingleObserver<Order>() {
            @Override
            public void onSuccess(Order order) {
                Log.d("Tag","Order Presenter succeed order id " + order.getId() );
                view.showOrder(order);
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Tag","Order Presenter error" + e.getMessage());
                view.showError(e.getMessage());
            }
        });
    }

    public void updateOrder(Order order) {
        view.showProgressIndicator(true);
        interactor.updateOrder(order, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {
                view.showProgressIndicator(false);
                view.navigateOrderAdmin();
                view.showInfo("Complete");
            }

            @Override
            public void onError(Throwable e) {
                view.showProgressIndicator(false);
                view.showError("Error");
            }
        });
    }
}
