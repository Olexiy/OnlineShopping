package com.example.olexi.onlineshoppingandroid.utils;

/**
 * Created by olexi on 1/5/2018
 */

public interface OnItemClickListener<T> {
    void onItemClick(T item);
}
