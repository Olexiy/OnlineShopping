package com.example.olexi.onlineshoppingandroid.ui.modules.admin;

import com.example.domain.interactor.AdminInteractor;
import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 2/12/2018
 */
@ActivityScope
public class AdminPresenter extends BasePresenter<AdminView> {
    private AdminInteractor interactor;

    @Inject
    public AdminPresenter(AdminInteractor interactor) {
        this.interactor = interactor;
    }

    public void getAdminOrders(){
        interactor.getAdminOrders(new DisposableSingleObserver<List<Order>>() {
            @Override
            public void onSuccess(List<Order> orders) {
                view.showActiveOrders(orders);
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }
}
