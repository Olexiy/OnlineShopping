package com.example.olexi.onlineshoppingandroid.ui.infrastructure;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.example.olexi.onlineshoppingandroid.R;

/**
 * Created by olexi on 12/19/2017
 */

public class DefaultActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);
    }

    protected void setContentFragment(Fragment fragment){
        super.setFragment(fragment,R.id.content_place_default);
    }
}
