package com.example.olexi.onlineshoppingandroid.ui.modules.product.productCharacteristic;

import com.example.domain.interactor.ProductInteractor;
import com.example.entity.models.ProductCharacteristics;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/25/2018
 */

public class ProductCharacteristicPresenter extends BasePresenter<ProductCharacteristicView>{

    private ProductInteractor interactor;

    @Inject
    public ProductCharacteristicPresenter(ProductInteractor interactor) {
        this.interactor = interactor;
    }

    public void getProductsCharacteristics(int productId){
        interactor.getProductCharacteristics(productId, new DisposableSingleObserver<List<ProductCharacteristics>>() {
            @Override
            public void onSuccess(List<ProductCharacteristics> characteristics) {
                view.showCharacteristics(characteristics);
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
}
