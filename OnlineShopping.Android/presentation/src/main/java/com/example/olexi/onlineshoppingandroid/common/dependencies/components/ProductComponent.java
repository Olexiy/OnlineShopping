package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.product.ProductFragment;
import com.example.olexi.onlineshoppingandroid.ui.modules.product.productCharacteristic.ProductCharacteristicFragment;
import com.example.olexi.onlineshoppingandroid.ui.modules.product.productGeneral.ProductGeneralFragment;

import dagger.Component;

/**
 * Created by olexi on 12/21/2017
 */
@ActivityScope
@Component(dependencies = AppComponent.class)
public interface ProductComponent {
    void inject(ProductGeneralFragment fragment);
    void inject(ProductCharacteristicFragment fragment);
}
