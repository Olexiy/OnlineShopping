package com.example.olexi.onlineshoppingandroid.ui.modules.basket;

import com.example.entity.models.OrderItem;
import com.example.entity.models.Product;

/**
 * Created by olexi on 1/26/2018
 */

public interface OnBasketClickListener {
    void changeQuantityOfOrderItem(OrderItem orderItem);
    void removeOrderItemFromOrder(OrderItem orderItem);
}
