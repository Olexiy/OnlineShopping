package com.example.olexi.onlineshoppingandroid.ui.modules.orderForm;

import com.example.domain.interactor.OrderInteractor;
import com.example.domain.interactor.UserInteractor;
import com.example.entity.models.Order;
import com.example.entity.models.User;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 2/6/2018
 */

public class OrderFormPresenter extends BasePresenter<OrderFormView>{

    private OrderInteractor interactor;
    private AccountPreferences accountPreferences;
    private UserInteractor userInteractor;

    @Inject
    public OrderFormPresenter(OrderInteractor interactor, AccountPreferences accountPreferences, UserInteractor userInteractor) {
        this.interactor = interactor;
        this.accountPreferences = accountPreferences;
        this.userInteractor = userInteractor;
    }

    public void getOrder(int orderId){
        interactor.getOrder(orderId, new DisposableSingleObserver<Order>() {
            @Override
            public void onSuccess(Order order) {
                view.showOrderDetail(order);
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }

    public void updateOrder(Order order){
        view.showProgressIndicator(true);
        interactor.updateOrder(order, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {
                view.showProgressIndicator(false);
                view.showInfo("Order in processing...");
                view.navigateOrderList();
            }

            @Override
            public void onError(Throwable e) {
                view.showProgressIndicator(false);
                view.showError(e.getMessage());
            }
        });
    }

    public void loadUser(){
        userInteractor.getUser(accountPreferences.getUserId(), new DisposableSingleObserver<User>() {
            @Override
            public void onSuccess(User user) {
                view.showUserDetail(user);
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }
}
