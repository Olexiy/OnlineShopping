package com.example.olexi.onlineshoppingandroid.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.example.entity.Constant;

/**
 * Created by olexi on 2/1/2018
 */

public class CustomExceptionHandler implements Thread.UncaughtExceptionHandler{

    Thread.UncaughtExceptionHandler handler;

    public CustomExceptionHandler(){
        handler = Thread.getDefaultUncaughtExceptionHandler();
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Log.d("Tag", "Something wrong happened! error = " + e.getMessage());
        if(handler != null){
            handler.uncaughtException(t,e);
        }
        System.exit(0);
    }
}
