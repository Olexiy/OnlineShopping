package com.example.olexi.onlineshoppingandroid.ui.modules.basket;

import android.util.Log;

import com.example.domain.interactor.OrderInteractor;
import com.example.domain.interactor.OrderItemInteractor;
import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;
import com.example.entity.state.OrderStatus;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/26/2018
 */

public class BasketPresenter extends BasePresenter<BasketView>{

    private OrderInteractor orderInteractor;
    private OrderItemInteractor orderItemInteractor;


    @Inject
    public BasketPresenter(OrderInteractor orderInteractor,OrderItemInteractor orderItemInteractor) {
        this.orderInteractor = orderInteractor;
        this.orderItemInteractor = orderItemInteractor;
    }

    public void getActiveOrder(){
        orderInteractor.getOrders(new DisposableSingleObserver<List<Order>>() {
            @Override
            public void onSuccess(List<Order> orders) {
                Order activeOrder = null;
                for(Order order : orders){
                    if(order.getStatus() == OrderStatus.Active.ordinal()){
                        Log.d("Tag","BasketPresenter active order ");
                        activeOrder = order;
                    }
                }
                view.showActiveOrder(activeOrder);
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Tag","BasketPresenter getOrders onError");
            }
        });
    }

    public void updateOrder(Order order) {
        orderInteractor.updateOrder(order, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {
                Log.d("Tag","updateOrder onSuccess");
                view.updateOrder();
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Tag","updateOrder onSuccess");
                view.showError(e.getMessage());
            }
        });
    }

    public void updateOrderItem(OrderItem orderItem) {
        orderItemInteractor.updateOrderItem(orderItem, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {
                view.updateOrder();
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }

    public void deleteOrderItem(int orderItemId) {
        orderItemInteractor.deleteOrderItem(orderItemId, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {
                view.updateOrder();
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
}
