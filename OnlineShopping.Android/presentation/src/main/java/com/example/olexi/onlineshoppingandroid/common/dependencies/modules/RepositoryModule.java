package com.example.olexi.onlineshoppingandroid.common.dependencies.modules;

import com.example.data.repository.MemoryAdminRepository;
import com.example.data.repository.MemoryCategoriesRepository;
import com.example.data.repository.MemoryOrderItemRepository;
import com.example.data.repository.MemoryOrderRepository;
import com.example.data.repository.MemoryProductRepository;
import com.example.data.repository.MemoryProfileRepository;
import com.example.data.repository.MemoryUserRepository;
import com.example.data.repository.SingleAccountRepository;
import com.example.domain.repository.AccountRepository;
import com.example.domain.repository.AdminRepository;
import com.example.domain.repository.CategoriesRepository;
import com.example.domain.repository.OrderItemRepository;
import com.example.domain.repository.OrderRepository;
import com.example.domain.repository.ProductRepository;
import com.example.domain.repository.ProfileRepository;
import com.example.domain.repository.UserRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by olexi on 12/19/2017
 */
@Module
public class RepositoryModule {

    @Provides
    @Singleton
    AccountRepository provideAccountRepository(SingleAccountRepository repository){
        return repository;
    }

    @Provides
    @Singleton
    UserRepository provideUserRepository(MemoryUserRepository repository) {
        return repository;
    }


    @Provides
    @Singleton
    ProfileRepository provideProfileRepository(MemoryProfileRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    CategoriesRepository provideCategoriesRepository(MemoryCategoriesRepository repository){return repository;}

    @Provides
    @Singleton
    ProductRepository provideProductRepository(MemoryProductRepository repository){return repository;}

    @Provides
    @Singleton
    OrderRepository provideOrderRepository(MemoryOrderRepository repository){return repository;}

    @Provides
    @Singleton
    OrderItemRepository provideOrderItemRepository(MemoryOrderItemRepository repository){return repository;}

    @Provides
    @Singleton
    AdminRepository provideAdminRepository(MemoryAdminRepository repository){return repository;}
}
