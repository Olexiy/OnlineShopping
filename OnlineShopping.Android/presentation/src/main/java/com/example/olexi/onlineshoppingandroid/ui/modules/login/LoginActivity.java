package com.example.olexi.onlineshoppingandroid.ui.modules.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.entity.models.Authorization;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerLoginComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.LoginComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseActivity;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.DefaultActivity;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;
import com.example.olexi.onlineshoppingandroid.ui.modules.register.RegisterActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.start.StartActivity;

/**
 * Created by olexi on 12/18/2017
 */

public class LoginActivity extends DefaultActivity implements ComponentContainer<LoginComponent>{

    public static Intent getCallingIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentFragment(LoginFragment.newInstance());
        setFragment(LoginFragment.newInstance(),R.id.content_place_default);
    }

    void navigateHome(IndicativeView view,String token) {
        getAppDependencyComponent()
                .preparationWorker()
                .loginPreparation(this, view,token);
    }

    @Override
    public LoginComponent buildComponent() {
        return DaggerLoginComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public LoginComponent getComponent() {
        return getSavedComponent();
    }

    public void navigateRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
