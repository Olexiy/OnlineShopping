package com.example.olexi.onlineshoppingandroid.ui.modules.productsList;

import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

import java.util.List;

/**
 * Created by olexi on 1/10/2018
 */

public interface ProductsListView extends BaseView,InformationView {
    void showProductsList(List<Product> productList);
}
