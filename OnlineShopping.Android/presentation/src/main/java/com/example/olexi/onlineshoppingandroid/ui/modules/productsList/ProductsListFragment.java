package com.example.olexi.onlineshoppingandroid.ui.modules.productsList;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.entity.BundleParam;
import com.example.entity.models.Category;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.CategoriesComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.ProductsListComponent;
import com.example.olexi.onlineshoppingandroid.ui.adapter.CategoriesRecyclerViewAdapter;
import com.example.olexi.onlineshoppingandroid.ui.adapter.ProductsListRecyclerViewAdapter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.categories.CategoriesActivity;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/10/2018
 */

public class ProductsListFragment extends BaseFragment implements ProductsListView{

    public static ProductsListFragment newInstance(String categoryName) {
        Bundle args = new Bundle();
        args.putString(BundleParam.CATEGORY_NAME,categoryName);
        ProductsListFragment fragment = new ProductsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar_not_scroll)
    Toolbar toolbar;

    @Inject
    ProductsListPresenter presenter;

    private ProductsListRecyclerViewAdapter adapter;
    private LinearLayoutManager layoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getDependencyComponent(ProductsListComponent.class).inject(this);
        presenter.setView(this);

        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        OnItemClickListener<Product> listener = getActivity(ProductsListActivity.class);
        adapter = new ProductsListRecyclerViewAdapter(getContext(),listener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products_list,container,false);
        ButterKnife.bind(this,view);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        getBaseActivity().setSupportActionBar(toolbar);
        getActivity(NavigationMenuActivity.class).setupToolbarDrawer(toolbar);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter.cleanData();
        presenter.getProductsList(getCategoryName());
    }

    @Override
    public void showInfo(String message) {
        Log.d("Tag","Info ProductsListFragment");
    }

    @Override
    public void showError(String errorMessage) {
        Log.d("Tag","showError ProductsListFragment");
    }

    @Override
    public void showProductsList(List<Product> productList) {
        adapter.loadData(productList);
    }

    private String getCategoryName() {
        return getArguments().getString(BundleParam.CATEGORY_NAME);
    }
}
