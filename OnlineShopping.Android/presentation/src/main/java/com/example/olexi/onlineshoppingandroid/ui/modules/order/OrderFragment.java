package com.example.olexi.onlineshoppingandroid.ui.modules.order;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.entity.BundleParam;
import com.example.entity.models.Order;
import com.example.entity.models.Product;
import com.example.entity.state.OrderStatus;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.OrderComponent;
import com.example.olexi.onlineshoppingandroid.ui.adapter.OrderItemRecycleViewAdapter;
import com.example.olexi.onlineshoppingandroid.ui.adapter.ProductsListRecyclerViewAdapter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.utils.BoxLoaderView;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/23/2018
 */

public class OrderFragment extends BaseFragment implements OrderView{

    @BindView(R.id.toolbar_not_scroll)
    Toolbar toolbar;

    @BindView(R.id.tv_order_sum)
    TextView orderSum;

    @BindView(R.id.tv_order_status)
    TextView orderStatus;

    @BindView(R.id.tv_order_date)
    TextView orderDate;

    @BindView(R.id.tv_order_address)
    TextView orderAddress;

    @BindView(R.id.tv_order_email)
    TextView orderEmail;

    @BindView(R.id.tv_order_phone)
    TextView orderPhone;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.box_loader)
    BoxLoaderView boxLoaderView;

    @Inject
    OrderPresenter presenter;
    private OrderItemRecycleViewAdapter adapter;
    private LinearLayoutManager layoutManager;

    public static OrderFragment newInstance(int orderId, boolean isAdmin) {
        Bundle args = new Bundle();
        args.putInt(BundleParam.ORDER_ID,orderId);
        args.putBoolean(BundleParam.IS_ADMIN,isAdmin);
        OrderFragment fragment = new OrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(OrderComponent.class).inject(this);
        presenter.setView(this);

        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        OnItemClickListener<Product> listener = getActivity(OrderActivity.class);
        adapter = new OrderItemRecycleViewAdapter(getContext(),listener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order,container,false);
        ButterKnife.bind(this,view);
        getBaseActivity().setSupportActionBar(toolbar);

        if(isAdmin()){
            getActivity(NavigationMenuActivity.class).lockToolbarDrawer();
            setHasOptionsMenu(true);
        }else {
            getActivity(NavigationMenuActivity.class).setupToolbarDrawer(toolbar);
        }
        boxLoaderView.setLoaderColor(this.getResources().getColor(R.color.colorPrimary));
        boxLoaderView.setStrokeColor(this.getResources().getColor(R.color.colorPrimaryDark));
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter.cleanData();
        presenter.getOrder(getOrderId());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_order,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_complete:
                updateOrderAdmin();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateOrderAdmin(){
        Order order = new Order();
        order.setId(getOrderId());
        order.setStatus(OrderStatus.Finished.ordinal());
        presenter.updateOrder(order);
    }

    private int getOrderId() {
        return getArguments().getInt(BundleParam.ORDER_ID);
    }

    private boolean isAdmin() {
        return getArguments().getBoolean(BundleParam.IS_ADMIN);
    }

    @Override
    public void showInfo(String message) {
        Log.d("Tag",message);
    }

    @Override
    public void showError(String errorMessage) {
        Log.d("Tag",errorMessage);
    }

    @Override
    public void showOrder(Order order) {
        adapter.loadData(order.getOrderItems());
        getBaseActivity().getSupportActionBar().setTitle("Order № " + order.getId());
        orderDate.setText(order.getCreatedDate() == null ? "" : order.getCreatedDate());
        orderStatus.setText(order.getStatusString(order.getStatus()));
        orderSum.setText(order.getSum()+"грн");
        orderAddress.setText(order.getShipAddress());
        orderEmail.setText(order.getShipEmail() == null ? "" : order.getShipEmail());
        orderPhone.setText(order.getShipPhoneNumber() == null ? "" : order.getShipPhoneNumber());
    }

    @Override
    public void navigateOrderAdmin() {
        getActivity(OrderActivity.class).navigateOrderAdmin();
    }

    @Override
    public void showProgressIndicator(boolean condition) {
        boxLoaderView.setVisibility(condition ? View.VISIBLE : View.INVISIBLE);
    }
}
