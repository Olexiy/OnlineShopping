package com.example.olexi.onlineshoppingandroid.ui.modules.orderList;

import com.example.entity.models.Category;
import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

import java.util.List;

/**
 * Created by olexi on 1/21/2018
 */

public interface OrderListView extends BaseView,InformationView {
    void showFinishedOrderList(List<Order> orderList);
}
