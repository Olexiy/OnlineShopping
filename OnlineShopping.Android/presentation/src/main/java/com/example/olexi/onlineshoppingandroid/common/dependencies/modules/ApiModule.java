package com.example.olexi.onlineshoppingandroid.common.dependencies.modules;

import com.example.data.api.RestApiService;
import com.example.data.tools.LoggingInterceptor;
import com.example.olexi.onlineshoppingandroid.common.account.AccountInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by olexi on 12/19/2017
 */
@Module
public class ApiModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(AccountInterceptor accountInterceptor, LoggingInterceptor loggingInterceptor) {
        return new OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.SECONDS)
                .addInterceptor(accountInterceptor)
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(RestApiService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //convert http response to java object
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    RestApiService provideRestApiService(Retrofit retrofit){
        return retrofit.create(RestApiService.class);
    }
}
