package com.example.olexi.onlineshoppingandroid.ui.modules.categories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.entity.models.Category;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.CategoriesComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerCategoriesComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.utils.CustomExceptionHandler;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

/**
 * Created by olexi on 1/5/2018
 */

public class CategoriesActivity extends NavigationMenuActivity implements ComponentContainer<CategoriesComponent>,
        OnItemClickListener<Category> {


    public static Intent getCallingIntent(Context context){
        return new Intent(context,CategoriesActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentFragment(CategoriesFragment.newInstance());
    }

    @Override
    public CategoriesComponent buildComponent() {
        return DaggerCategoriesComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public CategoriesComponent getComponent() {
        return getSavedComponent();
    }

    @Override
    public void onItemClick(Category item) {
        navigator.navigateProductsList(this,item.getName());
    }
}
