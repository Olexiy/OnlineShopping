package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.admin.AdminFragment;

import dagger.Component;

/**
 * Created by olexi on 2/12/2018
 */
@ActivityScope
@Component(dependencies = AppComponent.class)
public interface AdminComponent {
    void inject(AdminFragment adminFragment);
}
