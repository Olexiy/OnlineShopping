package com.example.olexi.onlineshoppingandroid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.entity.models.OrderItem;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.utils.AsyncTaskLoadImage;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/28/2018
 */

public class OrderItemRecycleViewAdapter extends RecyclerView.Adapter<OrderItemRecycleViewAdapter.OrderItemViewHolder>{

    private Context context;
    private ArrayList<OrderItem> orderItems;
    private OnItemClickListener<Product> listener;

    public OrderItemRecycleViewAdapter(Context context, OnItemClickListener<Product> listener) {
        this.context = context;
        this.listener = listener;
    }

    public void loadData(List<OrderItem> orderItems){
        if(this.orderItems == null){
            this.orderItems = new ArrayList<>();
        }
        this.orderItems.addAll(orderItems);
        notifyDataSetChanged();
    }

    public void cleanData() {
        orderItems = new ArrayList<>();
    }

    @Override
    public OrderItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order,parent,false);
        return new OrderItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderItemViewHolder holder, int position) {
        holder.initData(context,orderItems.get(position));
    }

    @Override
    public int getItemCount() {
        return orderItems == null ? 0 : orderItems.size();
    }

    public class OrderItemViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ll_item_container)
        LinearLayout itemContainer;

        @BindView(R.id.iv_product_title)
        ImageView productImageTitle;

        @BindView(R.id.tv_product_name)
        TextView productName;

        @BindView(R.id.tv_item_price)
        TextView itemPrice;

        @BindView(R.id.tv_quantity_of_product)
        TextView quantityProduct;

        public OrderItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void initData(Context context, final OrderItem orderItem){

            productName.setText(orderItem.getProduct().toString());
            itemPrice.setText(orderItem.getPrice()+ " " + context.getString(R.string.uah_symbol));
            quantityProduct.setText(orderItem.getQuantity() + " " +context.getString(R.string.pieces_shor_form));

            if(orderItem.getProduct().getImageTitle() != null){
                showProductImageTitle(orderItem.getProduct().getImageTitle());
            }else {
                productImageTitle.setImageResource(R.drawable.star);
            }

            itemContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(orderItem.getProduct());
                }
            });
        }

        void showProductImageTitle(String imageName) {
            String url = " http://192.168.0.104:8080/api/ProductImage?imageName=" + imageName;
            new AsyncTaskLoadImage(productImageTitle).execute(url);
        }

    }

}
