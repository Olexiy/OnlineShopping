package com.example.olexi.onlineshoppingandroid.ui.modules.product.productGeneral;

import com.example.entity.models.Order;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

import java.util.List;

/**
 * Created by olexi on 1/25/2018
 */

public interface ProductGeneralView extends BaseView,InformationView {
    void showProduct(Product product);
    void showProductImages(List<String> productImages);
    void initActiveOrder(List<Order> orderList);
    void initOrder();
}
