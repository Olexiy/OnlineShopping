package com.example.olexi.onlineshoppingandroid.ui.modules.login;

import android.text.TextUtils;
import android.util.Log;

import com.example.domain.interactor.AccountInteractor;
import com.example.entity.models.Authorization;
import com.example.olexi.onlineshoppingandroid.common.account.AccountInterceptor;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 12/28/2017
 */

public class LoginPresenter extends BasePresenter<LoginView>{
    private AccountInteractor accountInteractor;
    private AccountPreferences accountPreferences;

    @Inject
    public LoginPresenter(AccountInteractor accountInteractor, AccountPreferences accountPreferences){
        this.accountInteractor = accountInteractor;
        this.accountPreferences = accountPreferences;
    }

    void signIn() {
        checkView();
        view.showProgressIndicator(true);

        final String login = view.getInputLogin();
        final String password = view.getInputPassword();

        if(!isValidInput(login, password)){
            view.showError("email or password is empty");
        }

        accountInteractor.signIn(login, password, new DisposableSingleObserver<Authorization>() {
            @Override
            public void onSuccess(Authorization value) {
                Log.d("Tag","Login Presenter succeed");
                accountPreferences.setLogin(login);
                accountPreferences.setPassword(password);
                view.loginSucceed(value.getAccessToken());
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Tag","Login Presenter error");
                view.showProgressIndicator(false);
                view.showError("error in server");
            }
        });
        
    }

    private boolean isValidInput(String email, String password){
        return !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password);
    }

}
