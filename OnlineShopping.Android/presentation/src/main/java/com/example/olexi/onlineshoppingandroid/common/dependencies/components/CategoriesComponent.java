package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.categories.CategoriesFragment;

import dagger.Component;

/**
 * Created by olexi on 1/5/2018
 */
@ActivityScope
@Component(dependencies = AppComponent.class)
public interface CategoriesComponent {
    void inject(CategoriesFragment categoriesFragment);
}
