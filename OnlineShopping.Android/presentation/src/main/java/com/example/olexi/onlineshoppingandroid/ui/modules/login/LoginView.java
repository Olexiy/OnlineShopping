package com.example.olexi.onlineshoppingandroid.ui.modules.login;

import com.example.entity.models.Authorization;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

/**
 * Created by olexi on 12/20/2017
 */

public interface LoginView extends InformationView, BaseView,IndicativeView {
    void loginSucceed(String token);
    String getInputLogin();
    String getInputPassword();
}
