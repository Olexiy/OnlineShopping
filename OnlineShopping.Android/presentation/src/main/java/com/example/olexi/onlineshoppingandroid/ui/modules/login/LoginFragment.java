package com.example.olexi.onlineshoppingandroid.ui.modules.login;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.entity.models.Authorization;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.LoginComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.modules.register.RegisterActivity;
import com.example.olexi.onlineshoppingandroid.utils.BoxLoaderView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by olexi on 12/19/2017
 */

public class LoginFragment extends BaseFragment implements LoginView {

    @BindView(R.id.email_wrapper)
    TextInputLayout loginWrapper;

    @BindView(R.id.password_wrapper)
    TextInputLayout passwordWrapper;

    @BindView(R.id.btn_sign_in)
    Button btnSignIn;

    @BindView(R.id.box_loader)
    BoxLoaderView boxLoaderView;

    @BindView(R.id.tv_sign_up)
    TextView tvSignUp;

    @NonNull
    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Inject
    LoginPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(LoginComponent.class).inject(this);
        presenter.setView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login,container,false);
        ButterKnife.bind(this,root);
        boxLoaderView.setLoaderColor(this.getResources().getColor(R.color.colorPrimary));
        boxLoaderView.setStrokeColor(this.getResources().getColor(R.color.colorPrimaryDark));
        tvSignUp.setPaintFlags(tvSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }


    @OnClick(R.id.btn_sign_in)
    protected void btnSignIn(){
        presenter.signIn();
    }

    @OnClick(R.id.tv_sign_up)
    protected void tvSignUn(){
        getActivity(LoginActivity.class).navigateRegister();
    }

    @Override
    public void loginSucceed(String token) {
        getActivity(LoginActivity.class).navigateHome(this,token);
    }

    @Override
    public String getInputLogin() {
        return loginWrapper.getEditText().getText().toString();
    }

    @Override
    public String getInputPassword() {
        return passwordWrapper.getEditText().getText().toString();
    }

    @Override
    public void showInfo(String message) {
    }

    @Override
    public void showError(String errorMessage) {
        Log.d("Tag","Error in login " + errorMessage);
    }

    @Override
    public void showProgressIndicator(boolean condition) {
        boxLoaderView.setVisibility(condition ? View.VISIBLE : View.INVISIBLE);
    }
}
