package com.example.olexi.onlineshoppingandroid.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/22/2018
 */

public class OrderListRecyclerViewAdapter extends RecyclerView.Adapter<OrderListRecyclerViewAdapter.OrderListViewHolder>{

    private ArrayList<Order> orders;
    private OnItemClickListener<Order> listener;

    public OrderListRecyclerViewAdapter(OnItemClickListener<Order> listener) {
        this.listener = listener;
    }

    public void loadData(List<Order>orderList){
        if(orders == null){
            orders = new ArrayList<>();
        }
        orders.addAll(orderList);
        notifyDataSetChanged();
    }

    public void cleanData(){
        orders = new ArrayList<>();
    }

    @Override
    public OrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_list, parent, false);
        return new OrderListRecyclerViewAdapter.OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderListViewHolder holder, int position) {
        holder.initData(orders.get(position));
    }

    @Override
    public int getItemCount() {
        return orders == null ? 0 : orders.size();
    }

    public class OrderListViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_order_id)
        TextView orderId;

        @BindView(R.id.tv_order_date)
        TextView orderDate;

        @BindView(R.id.tv_order_status)
        TextView orderStatus;

        @BindView(R.id.tv_order_sum)
        TextView orderSum;

        @BindView(R.id.ll_order_container)
        LinearLayout container;

        public OrderListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void initData(final Order order){
            orderId.setText("№"+order.getId());
            orderDate.setText(order.getCreatedDate());
            orderStatus.setText(order.getStatusString(order.getStatus()));
            orderSum.setText(order.getSum()+"грн");
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(order);
                }
            });
        }
    }
}
