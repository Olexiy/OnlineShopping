package com.example.olexi.onlineshoppingandroid.common.dependencies;

/**
 * Created by olexi on 12/19/2017
 */

public interface ComponentContainer <C> {
    C buildComponent();
    C getComponent();
}
