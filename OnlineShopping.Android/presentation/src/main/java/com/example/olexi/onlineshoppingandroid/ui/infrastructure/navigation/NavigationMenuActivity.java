package com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation;


import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.entity.models.User;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/3/2018
 */

public class NavigationMenuActivity extends BaseActivity implements NavigationMenuView, NavigationView.OnNavigationItemSelectedListener{
    
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.navigation_view)
    NavigationView navigationView;

    @Inject
    NavigationMenuPresenter presenter;
    private boolean showDrawerToggle;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);
        getAppDependencyComponent().inject(this);
        navigationView.setNavigationItemSelectedListener(this);
        presenter.setView(this);
        presenter.loadUser();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if(showDrawerToggle)
            drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showInfo(String message) {
        //Toast
    }

    @Override
    public void showError(String errorMessage) {
        //Toast
    }

    public void setupDrawerToggle(){
        drawerToggle.syncState();
    }

    public void setupToolbarDrawer(Toolbar toolbar){
        if (drawerToggle != null) {
            drawerLayout.removeDrawerListener(drawerToggle);
        }
        ActionBarDrawerToggle toggle = setupDrawerToggle(toolbar);
        drawerLayout.addDrawerListener(toggle);
        drawerToggle = toggle;
        showDrawerToggle = true;
    }

    public void lockToolbarDrawer() {
        showDrawerToggle = false;
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    private ActionBarDrawerToggle setupDrawerToggle(Toolbar toolbar) {
        return new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_item_home_profile:
                navigator.navigateUserProfile(this);
                break;
            case R.id.nav_item_home_categories:
                navigator.navigateCategories(this);
                break;
            case R.id.nav_item_basket:
                navigator.navigateBasket(this);
                break;
            case R.id.nav_item_home_order_list:
                navigator.navigateOrderList(this);
                break;
            case R.id.nav_item_home_setting:
                navigator.navigateSetting(this);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }


    @Override
    public void placeUser(User user) {
        View headerLayout = navigationView.getHeaderView(0);
        TextView nameView = (TextView) headerLayout.findViewById(R.id.tv_nav_header_username);
        nameView.setText(user.toString());
    }

    protected void setContentFragment(Fragment fragment){
        setFragment(fragment,R.id.content_place_navigation);
    }
}
