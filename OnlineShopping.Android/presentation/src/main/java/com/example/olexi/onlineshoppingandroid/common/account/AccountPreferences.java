package com.example.olexi.onlineshoppingandroid.common.account;

import android.support.annotation.NonNull;

/**
 * Created by olexi on 12/19/2017
 */

public interface AccountPreferences {
    void setUserId(int Id);
    int getUserId();

    void setLogin(String login);
    String getLogin();

    void setPassword(String password);
    String getPassword();

    boolean isSigned();
    void clearData();
}
