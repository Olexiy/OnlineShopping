package com.example.olexi.onlineshoppingandroid.ui.modules.basket;

import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

/**
 * Created by olexi on 1/26/2018
 */

public interface BasketView extends BaseView,InformationView{
    void showActiveOrder(Order order);
    void updateOrder();
}
