package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.profile.ClientProfileActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.profile.ClientProfileFragment;

import dagger.Component;

/**
 * Created by olexi on 12/21/2017
 */

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface UserComponent {
   // void inject(ClientProfileActivity profileActivity);
    void inject(ClientProfileFragment profileFragment);
}
