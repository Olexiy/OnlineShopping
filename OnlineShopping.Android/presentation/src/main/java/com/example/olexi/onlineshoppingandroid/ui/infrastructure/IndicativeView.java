package com.example.olexi.onlineshoppingandroid.ui.infrastructure;

/**
 * Created by olexi on 1/29/2018
 */

public interface IndicativeView {

    void showProgressIndicator(boolean condition);
}
