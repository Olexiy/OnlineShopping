package com.example.olexi.onlineshoppingandroid.ui.modules.categories;

import com.example.entity.models.Category;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

import java.util.List;

/**
 * Created by olexi on 1/5/2018
 */

public interface CategoriesView extends BaseView, InformationView{
    void showCategories(List<Category> categories);
}
