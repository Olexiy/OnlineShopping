package com.example.olexi.onlineshoppingandroid.ui.modules.product.productCharacteristic;

import com.example.entity.models.ProductCharacteristics;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;

import java.util.List;

/**
 * Created by olexi on 1/25/2018
 */

public interface ProductCharacteristicView extends BaseView{
    void showCharacteristics(List<ProductCharacteristics> characteristics);
}
