package com.example.olexi.onlineshoppingandroid.ui.modules.basket;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.BasketComponent;
import com.example.olexi.onlineshoppingandroid.ui.adapter.BasketProductsRecycleViewAdapter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by olexi on 1/26/2018
 */

public class BasketFragment extends BaseFragment implements BasketView, OnBasketClickListener{

    @BindView(R.id.toolbar_not_scroll)
    Toolbar toolbar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.btn_to_order)
    Button btnToOrder;

    @BindView(R.id.tv_basket_empty)
    TextView tvBasketEmpty;

    @Inject
    BasketPresenter presenter;

    private BasketProductsRecycleViewAdapter adapter;
    private LinearLayoutManager layoutManager;
    private Order order;

    public static BasketFragment newInstance() {
        return new BasketFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(BasketComponent.class).inject(this);
        presenter.setView(this);

        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        OnItemClickListener<Product> listener = getActivity(BasketActivity.class);
        adapter = new BasketProductsRecycleViewAdapter(getContext(),listener, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basket,container,false);
        ButterKnife.bind(this,view);

        getBaseActivity().setSupportActionBar(toolbar);
        getBaseActivity().getSupportActionBar().setTitle(this.getResources().getString(R.string.basket));
        getActivity(NavigationMenuActivity.class).setupToolbarDrawer(toolbar);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        presenter.getActiveOrder();

        return view;
    }

    @Override
    public void showInfo(String message) {

    }

    @Override
    public void showError(String errorMessage) {

    }

    @Override
    public void showActiveOrder(Order order) {
        if(order != null){
            this.order = order;
            adapter.loadData(order.getOrderItems());
        }else {
            recyclerView.setVisibility(View.INVISIBLE);
            btnToOrder.setVisibility(View.INVISIBLE);
            tvBasketEmpty.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.btn_to_order)
    public void clickToOrder(View view){
        getActivity(BasketActivity.class).navigateToOrderForm(order.getId());
    }

    @Override
    public void changeQuantityOfOrderItem(OrderItem orderItem) {
        presenter.updateOrderItem(orderItem);

    }

    @Override
    public void removeOrderItemFromOrder(OrderItem orderItem) {
        presenter.deleteOrderItem(orderItem.getId());
    }

    public void updateOrder(){
        adapter.cleanData();
        presenter.getActiveOrder();
    }
}
