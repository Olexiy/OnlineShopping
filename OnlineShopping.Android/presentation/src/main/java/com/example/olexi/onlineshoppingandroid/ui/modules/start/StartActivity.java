package com.example.olexi.onlineshoppingandroid.ui.modules.start;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.App;
import com.example.olexi.onlineshoppingandroid.common.PreparationWorker;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.utils.CustomScaleAnimation;
import com.example.olexi.onlineshoppingandroid.utils.PostDelayedTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 12/18/2017
 */

public class StartActivity extends AppCompatActivity implements IndicativeView{

    private static final int SPLASH_TIME_MS = 3000;

    @BindView(R.id.iv_flip)
    ImageView flipImage;

    private CustomScaleAnimation scaleAnimation;

    @Inject
    PreparationWorker preparationWorker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App)getApplicationContext()).getDependencyComponent().inject(this);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this, this);

        PostDelayedTask.create(SPLASH_TIME_MS, true, new PostDelayedTask.Callback() {
            @Override
            public void onSucceed() {
                preparationWorker.startPreparation(StartActivity.this,StartActivity.this);
            }
        });

        scaleAnimation = new CustomScaleAnimation(flipImage);
        scaleAnimation.startAnimation(R.drawable.coin_dolar,R.drawable.coin_euro);
    }

    @Override
    public void showProgressIndicator(boolean condition) {
       scaleAnimation.cancelAnimation();
    }

}
