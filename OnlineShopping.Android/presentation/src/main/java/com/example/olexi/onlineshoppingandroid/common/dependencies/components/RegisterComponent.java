package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.register.RegisterFragment;

import dagger.Component;

/**
 * Created by olexi on 2/9/2018
 */

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface RegisterComponent {
    void inject(RegisterFragment registerFragment);
}
