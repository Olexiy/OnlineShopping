package com.example.olexi.onlineshoppingandroid.ui.modules.register;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.entity.models.ClientProfile;
import com.example.entity.models.Credentials;
import com.example.entity.models.Registration;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.RegisterComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.modules.login.LoginFragment;
import com.example.olexi.onlineshoppingandroid.utils.BoxLoaderView;

import java.util.regex.Pattern;
import java.util.zip.Inflater;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by olexi on 2/9/2018
 */

public class RegisterFragment extends BaseFragment implements RegisterView{

    @BindView(R.id.first_name_wrapper)
    TextInputLayout firstNameWrapper;

    @BindView(R.id.last_name_wrapper)
    TextInputLayout lastNameWrapper;

    @BindView(R.id.email_wrapper)
    TextInputLayout emailWrapper;

    @BindView(R.id.password_wrapper)
    TextInputLayout passwordWrapper;

    @BindView(R.id.btn_sign_up)
    Button btnSignUp;

    @BindView(R.id.box_loader)
    BoxLoaderView boxLoaderView;

    @Inject
    RegisterPresenter presenter;

    private String firstName, lastName, email, password;
    private static final String EMAIL_PATTERN =   "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(RegisterComponent.class).inject(this);
        presenter.setView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register,container,false);
        ButterKnife.bind(this,view);
        boxLoaderView.setLoaderColor(this.getResources().getColor(R.color.colorPrimary));
        boxLoaderView.setStrokeColor(this.getResources().getColor(R.color.colorPrimaryDark));
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @OnClick(R.id.btn_sign_up)
    protected void btnSignUp(){
        firstName = getFirstName();
        lastName = getLastName();
        email = getEmail();
        password = getPassword();
        if(checkCredential()){
            Credentials credentials = new Credentials(firstName, lastName, email,password);
            ClientProfile profile = new ClientProfile();
            Registration registration = new Registration(credentials,profile);
            presenter.signUp(registration);
        }
    }

    @Override
    public void showProgressIndicator(boolean condition) {
        btnSignUp.setVisibility(!condition ? View.VISIBLE : View.INVISIBLE);
        boxLoaderView.setVisibility(condition ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showInfo(String message) {

    }

    @Override
    public void showError(String errorMessage) {
        Log.d("Tag","Error " + errorMessage);
    }

    @Override
    public void succeedRegister() {
        getActivity(RegisterActivity.class).navigateLogin();
    }

    public String getFirstName(){
       return firstNameWrapper.getEditText().getText().toString();
    }

    public String getLastName(){
        return lastNameWrapper.getEditText().getText().toString();
    }

    public String getEmail(){
        return emailWrapper.getEditText().getText().toString();
    }

    public String getPassword(){
        return passwordWrapper.getEditText().getText().toString();
    }

    public boolean checkCredential(){
        if(TextUtils.isEmpty(firstName)){
            firstNameWrapper.setErrorEnabled(true);
            firstNameWrapper.setError("Error");
            return false;
        }
        if(TextUtils.isEmpty(lastName)){
            firstNameWrapper.setErrorEnabled(false);
            lastNameWrapper.setErrorEnabled(true);
            lastNameWrapper.setError("Error");
            return false;
        }
        if(TextUtils.isEmpty(email)|| ! emailPattern.matcher(email).matches()){
            firstNameWrapper.setErrorEnabled(false);
            lastNameWrapper.setErrorEnabled(false);
            emailWrapper.setErrorEnabled(true);
            emailWrapper.setError("Error");
            return false;
        }
        if(TextUtils.isEmpty(password)|| password.length() < 6){
            firstNameWrapper.setErrorEnabled(false);
            lastNameWrapper.setErrorEnabled(false);
            emailWrapper.setErrorEnabled(false);
            passwordWrapper.setErrorEnabled(true);
            passwordWrapper.setError("Error, min size 6");
            return false;
        }
        return true;
    }

}
