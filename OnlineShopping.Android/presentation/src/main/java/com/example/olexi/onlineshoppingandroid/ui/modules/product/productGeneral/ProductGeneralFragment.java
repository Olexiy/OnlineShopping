package com.example.olexi.onlineshoppingandroid.ui.modules.product.productGeneral;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.entity.BundleParam;
import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;
import com.example.entity.models.Product;
import com.example.entity.state.OrderStatus;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.ProductComponent;
import com.example.olexi.onlineshoppingandroid.ui.adapter.ProductImageAdapter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by olexi on 1/25/2018
 */

public class ProductGeneralFragment extends BaseFragment implements ProductGeneralView{

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.ll_images_dots)
    LinearLayout imagesDots;

    @BindView(R.id.tv_product_name)
    TextView productName;

    @BindView(R.id.tv_product_category)
    TextView categoryName;

    @BindView(R.id.tv_product_price)
    TextView productPrice;

    @BindView(R.id.btn_product_order)
    Button btnProductOrder;

    @Inject
    ProductGeneralPresenter presenter;

    private int dotsCount;
    private ImageView[] dots;
    private ProductImageAdapter adapter;
    private Product product;
    private Order order;

    public static ProductGeneralFragment newInstance(int productId) {
        Bundle args = new Bundle();
        args.putInt(BundleParam.PRODUCT_ID,productId);
        ProductGeneralFragment fragment = new ProductGeneralFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(ProductComponent.class).inject(this);
        presenter.setView(this);
        adapter = new ProductImageAdapter(getContext(), getProductId());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_general,container,false);
        ButterKnife.bind(this, view);

        viewPager.getLayoutParams().height = (int) (getScreenHeight() * 0.3);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(listener);
        presenter.getProduct(getProductId());
        presenter.getProductsImages(getProductId());
        presenter.getOrders();
        return view;
    }

    public int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    ViewPager.OnPageChangeListener listener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
        @Override
        public void onPageSelected(int position) {
            for (int i = 0; i< dotsCount; i++){
                dots[i].setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.nonactive_dot));
            }
            dots[position].setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.active_dot));
        }

        @Override
        public void onPageScrollStateChanged(int state) {}
    };

    private int getProductId() {
        return getArguments().getInt(BundleParam.PRODUCT_ID);
    }

    @Override
    public void showProduct(Product product) {
        this.product = product;
        productName.setText(product.toString());
        categoryName.setText(product.getCompany());
        productPrice.setText(product.getPrice()+" грн");
    }

    @Override
    public void showProductImages(List<String> productImages) {
        adapter.loadData(productImages);
        dotsCount = adapter.getCount();
        dots = new ImageView[dotsCount];
        for (int i = 0; i< dotsCount; i++){
            dots[i] = new ImageView(getContext());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.nonactive_dot));
            LinearLayout.LayoutParams params =
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            imagesDots.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.active_dot));
    }

    @Override
    public void initActiveOrder(List<Order> orderList) {
        for (Order order : orderList) {
            if(order.getStatus() == OrderStatus.Active.ordinal()){
                this.order = order;
                showBtnAddToOrder();
            }
        }

        if(order == null){
            order = new Order();
            order.setStatus(OrderStatus.Active.ordinal());
            presenter.createOrder(order);
        }
    }

    @Override
    public void initOrder() {
        presenter.getOrders();
    }


    public void showBtnAddToOrder() {
        if(checkProductInOrder(order)){
            initButtonProductOrder(true,R.string.add_to_order);
            btnProductOrder.setOnClickListener(btnListener);
        }else {
            initButtonProductOrder(false,R.string.product_already_in_basket);
        }
    }

    public void initButtonProductOrder(boolean status, int msg){
        btnProductOrder.setClickable(status);
        btnProductOrder.setText(msg);
    }


    private boolean checkProductInOrder(Order order){
        for (OrderItem orderItem : order.getOrderItems()) {
            if(this.product != null && this.product.getId() == orderItem.getProductId()){
                return false;
            }
        }
        return true;
    }

    View.OnClickListener btnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            presenter.createOrderItem(new OrderItem(order.getId(), product.getId()));
            btnProductOrder.setClickable(false);
            btnProductOrder.setText(R.string.product_already_in_basket);
        }
    };

    @Override
    public void showInfo(String message) {

    }

    @Override
    public void showError(String errorMessage) {

    }
}
