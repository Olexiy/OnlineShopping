package com.example.olexi.onlineshoppingandroid.ui.modules.admin;

import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

import java.util.List;

/**
 * Created by olexi on 2/12/2018
 */

public interface AdminView extends BaseView, InformationView, IndicativeView {
    void showActiveOrders(List<Order> orders);
}
