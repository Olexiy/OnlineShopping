package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.categories.CategoriesFragment;
import com.example.olexi.onlineshoppingandroid.ui.modules.orderList.OrderListFragment;

import dagger.Component;

/**
 * Created by olexi on 1/21/2018
 */
@ActivityScope
@Component(dependencies = AppComponent.class)
public interface OrderListComponent {
    void inject(OrderListFragment orderListFragment);
}
