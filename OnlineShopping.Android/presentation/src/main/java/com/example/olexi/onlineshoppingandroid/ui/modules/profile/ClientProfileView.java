package com.example.olexi.onlineshoppingandroid.ui.modules.profile;

import com.example.entity.models.ClientProfile;
import com.example.entity.models.User;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

/**
 * Created by olexi on 12/21/2017
 */

public interface ClientProfileView extends BaseView, InformationView, IndicativeView{
    void showUser(User user);
    void showProfile(ClientProfile clientProfile);
    void updateUser();
}
