package com.example.olexi.onlineshoppingandroid.ui.modules.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerUserComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.UserComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseActivity;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;

/**
 * Created by olexi on 12/21/2017
 */

public class ClientProfileActivity extends NavigationMenuActivity implements ComponentContainer<UserComponent>{

    public static Intent getCallingIntent(Context context) {
        Intent intent = new Intent(context, ClientProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ClientProfileFragment.newInstance(),R.id.content_place_navigation);
    }

    @Override
    public UserComponent buildComponent() {
        return DaggerUserComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public UserComponent getComponent() {
        return getSavedComponent();
    }
}
