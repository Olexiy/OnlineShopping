package com.example.olexi.onlineshoppingandroid.ui.modules.product.productReview;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;

import javax.inject.Inject;

/**
 * Created by olexi on 1/25/2018
 */

public class ProductReviewFragment extends BaseFragment {
    @Inject
    ProductReviewPresenter presenter;

    public static ProductReviewFragment newInstance() {
        return new ProductReviewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_review, container, false);
        return view;
    }
}
