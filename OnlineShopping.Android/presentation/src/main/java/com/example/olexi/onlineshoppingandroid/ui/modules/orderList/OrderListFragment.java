package com.example.olexi.onlineshoppingandroid.ui.modules.orderList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.OrderListComponent;
import com.example.olexi.onlineshoppingandroid.ui.adapter.OrderListRecyclerViewAdapter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/21/2018
 */

public class OrderListFragment  extends BaseFragment implements OrderListView {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar_not_scroll)
    Toolbar toolbar;

    @Inject
    OrderListPresenter presenter;

    private OrderListRecyclerViewAdapter adapter;
    private LinearLayoutManager layoutManager;

    public static OrderListFragment newInstance() {
        return new OrderListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(OrderListComponent.class).inject(this);
        presenter.setView(this);

        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        OnItemClickListener<Order> listener = getActivity(OrderListActivity.class);
        adapter = new OrderListRecyclerViewAdapter(listener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_list,container,false);
        ButterKnife.bind(this,view);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        getBaseActivity().setSupportActionBar(toolbar);
        getActivity(NavigationMenuActivity.class).setupToolbarDrawer(toolbar);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter.cleanData();
        presenter.getFilterOrders();
    }

    @Override
    public void showInfo(String message) {

    }

    @Override
    public void showError(String errorMessage) {

    }

    @Override
    public void showFinishedOrderList(List<Order> orderList) {
        adapter.loadData(orderList);
    }
}
