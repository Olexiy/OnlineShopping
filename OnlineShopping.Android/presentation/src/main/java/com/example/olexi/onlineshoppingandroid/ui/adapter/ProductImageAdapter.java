package com.example.olexi.onlineshoppingandroid.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.utils.AsyncTaskLoadImage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by olexi on 1/15/2018
 */

public class ProductImageAdapter extends PagerAdapter{

    private Context context;
    private List<String> productImages;
    private ImageView imageView;
    private int productId;

    public ProductImageAdapter(Context context, int productId) {
        this.context = context;
        this.productId = productId;
    }

    public void loadData(List<String> images){
        if(productImages == null){
            productImages = new ArrayList<>();
        }
        productImages.addAll(images);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return productImages == null ? 0 : productImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        imageView = new ImageView(context);
        int padding = context.getResources().getDimensionPixelSize(R.dimen.padding_small);
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        //imageView.setImageResource(productImages());

        showProductImage(imageView,productImages.get(position));
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }

//    public List<String> listProductImage(int productId) {
//        String url = " http://192.168.0.104:8080/api/ProductImage?productId=" + productId;
//        new AsyncTaskLoadImage(imageView).execute(url);
//    }

    public void showProductImage(ImageView imageView,String imageName) {
        String url = " http://192.168.0.104:8080/api/ProductImage?imageName=" + imageName;
        new AsyncTaskLoadImage(imageView).execute(url);
    }

}
