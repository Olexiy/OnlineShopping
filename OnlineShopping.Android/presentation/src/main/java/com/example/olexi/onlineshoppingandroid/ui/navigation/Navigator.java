package com.example.olexi.onlineshoppingandroid.ui.navigation;

import android.app.Activity;
import android.content.Context;

import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.ui.modules.admin.AdminActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.basket.BasketActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.categories.CategoriesActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.login.LoginActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.order.OrderActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.orderForm.OrderFormActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.orderList.OrderListActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.product.ProductActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.productsList.ProductsListActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.profile.ClientProfileActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.setting.SettingActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.start.StartActivity;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by olexi on 12/19/2017
 */
@Singleton
public class Navigator {

    private AccountPreferences accountPreferences;

    @Inject
    public Navigator(AccountPreferences accountPreferences){
        this.accountPreferences = accountPreferences;
    }

    public void navigateUserProfile(Activity context){
        if(!(context instanceof ClientProfileActivity)) {
            context.startActivity(ClientProfileActivity.getCallingIntent(context));
            if (context instanceof StartActivity || context instanceof LoginActivity) {
                context.finish();
            }
        }
    }

    public void navigateAdmin(Activity context){
        if(!(context instanceof AdminActivity)) {
            context.startActivity(AdminActivity.getCallingIntent(context));
            if (context instanceof StartActivity || context instanceof LoginActivity) {
                context.finish();
            }
        }
    }

    public void navigateUserLogin(Activity context){
        context.startActivity(LoginActivity.getCallingIntent(context));
        context.finish();
    }

    public void navigateCategories(Activity context){
        if(!(context instanceof CategoriesActivity))
            context.startActivity(CategoriesActivity.getCallingIntent(context));
    }

    public void navigateProductsList(Activity context, String categoryName){
        if(!(context instanceof ProductsListActivity))
            context.startActivity(ProductsListActivity.getCallingIntent(context,categoryName));
    }

    public void navigateProduct(Activity context,int productId){
        if(!(context instanceof ProductActivity))
            context.startActivity(ProductActivity.getCallingIntent(context, productId));
    }

    public void navigateOrderList(Activity context){
        if(!(context instanceof OrderListActivity))
            context.startActivity(OrderListActivity.getCallingIntent(context));
    }

    public void navigateOrder(Activity context, int orderId){
        if(!(context instanceof OrderActivity))
            context.startActivity(OrderActivity.getCallingIntent(context,orderId, false));
    }

    public void navigateOrderAdmin(Activity context, int orderId){
        if(!(context instanceof OrderActivity))
            context.startActivity(OrderActivity.getCallingIntent(context,orderId,true));
    }

    public void navigateBasket(Activity context){
        if(!(context instanceof BasketActivity))
            context.startActivity(BasketActivity.getCallingIntent(context));
    }

    public void navigateOrderForm(Activity context, int orderId){
        if(!(context instanceof OrderFormActivity))
            context.startActivity(OrderFormActivity.getCallingIntent(context, orderId));
    }

    public void navigateSetting(Activity context){
        if(!(context instanceof SettingActivity))
            context.startActivity(SettingActivity.getCallingIntent(context));
    }

}
