package com.example.olexi.onlineshoppingandroid.ui.infrastructure;

/**
 * Created by olexi on 12/20/2017
 */

public interface InformationView{
    void showInfo(String message);
    void showError(String errorMessage);
}
