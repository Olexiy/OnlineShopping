package com.example.olexi.onlineshoppingandroid.ui.modules.categories;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.entity.models.Category;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.CategoriesComponent;
import com.example.olexi.onlineshoppingandroid.ui.adapter.CategoriesRecyclerViewAdapter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.utils.BoxLoaderView;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/5/2018
 */

public class CategoriesFragment extends BaseFragment implements CategoriesView{

    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar_not_scroll)
    Toolbar toolbar;

    @Inject
    CategoriesPresenter presenter;

    private CategoriesRecyclerViewAdapter adapter;
    private LinearLayoutManager layoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(CategoriesComponent.class).inject(this);
        presenter.setView(this);

        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        OnItemClickListener<Category> listener = getActivity(CategoriesActivity.class);
        adapter = new CategoriesRecyclerViewAdapter(getContext(),listener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_categories,container,false);
        ButterKnife.bind(this,view);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        getBaseActivity().setSupportActionBar(toolbar);
        getActivity(NavigationMenuActivity.class).setupToolbarDrawer(toolbar);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter.cleanData();
        presenter.getCategories();
    }

    @Override
    public void showInfo(String message) {
        Log.d("Tag","Info categories");
    }

    @Override
    public void showError(String errorMessage) {
        Log.d("Tag","Error categories");
    }

    @Override
    public void showCategories(List<Category> categories) {
        adapter.loadData(categories);
    }
}
