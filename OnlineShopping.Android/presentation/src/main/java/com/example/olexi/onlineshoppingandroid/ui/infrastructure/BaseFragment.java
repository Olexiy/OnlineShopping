package com.example.olexi.onlineshoppingandroid.ui.infrastructure;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.UserComponent;
import com.example.olexi.onlineshoppingandroid.ui.modules.profile.ClientProfileActivity;

/*
 * Created by olexi on 12/20/2017.
 */

public class BaseFragment extends Fragment {
    protected BaseActivity getBaseActivity(){
        return (BaseActivity)getActivity();
    }

    protected <A extends BaseActivity>A getActivity(Class<A> aClass){
        return  aClass.cast(getActivity());
    }

    protected <C> C getDependencyComponent(Class<C> componentClass){
        if (! (getActivity() instanceof ComponentContainer)) {
            throw new RuntimeException("Activity not implemented ComponentContainer");
        }

        ComponentContainer container = (ComponentContainer) getActivity();

        C component = componentClass.cast(container.getComponent());
        if (component == null) {
            throw new RuntimeException("Activity not implemented ComponentContainer for: " + componentClass);
        }
        return component;
    }
}
