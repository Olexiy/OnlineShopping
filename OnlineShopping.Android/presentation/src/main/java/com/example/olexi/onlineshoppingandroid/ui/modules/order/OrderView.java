package com.example.olexi.onlineshoppingandroid.ui.modules.order;

import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

/**
 * Created by olexi on 1/23/2018
 */

public interface OrderView extends BaseView, InformationView, IndicativeView{
    void showOrder(Order order);
    void navigateOrderAdmin();
}
