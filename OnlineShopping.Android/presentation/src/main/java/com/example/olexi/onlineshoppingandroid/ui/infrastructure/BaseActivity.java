package com.example.olexi.onlineshoppingandroid.ui.infrastructure;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.App;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.AppComponent;
import com.example.olexi.onlineshoppingandroid.ui.modules.basket.BasketActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.categories.CategoriesActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.orderList.OrderListActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.product.ProductActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.profile.ClientProfileActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.setting.SettingActivity;
import com.example.olexi.onlineshoppingandroid.ui.navigation.Navigator;

import javax.inject.Inject;

/**
 * Created by olexi on 12/19/2017
 */

public class BaseActivity extends AppCompatActivity {
    @Inject
    public Navigator navigator;

    private Object savedComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App)getApplicationContext()).getDependencyComponent().inject(this);
        if (this instanceof ComponentContainer) {
            savedComponent = ((ComponentContainer) this).buildComponent();
        }
    }

    protected void setFragment(Fragment fragment, int contentLayoutId) {
        getSupportFragmentManager().beginTransaction()
                .replace(contentLayoutId, fragment)
                .commit();
    }

    @NonNull
    public AppComponent getAppDependencyComponent() {
        return ((App) getApplicationContext()).getDependencyComponent();
    }

    @Nullable
    public <C> C getSavedComponent() {
        return (C) savedComponent;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(this instanceof CategoriesActivity ||
                this instanceof BasketActivity ||
                this instanceof OrderListActivity){
            Intent intent = new Intent(this,ClientProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else if(this instanceof ClientProfileActivity){
            finish();
//            Intent a = new Intent(Intent.ACTION_MAIN);
//            a.addCategory(Intent.CATEGORY_HOME);
//            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(a);
        }else {
            super.onBackPressed();
        }

    }
}
