package com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation;

import com.example.entity.models.User;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

/**
 * Created by olexi on 1/3/2018
 */

public interface NavigationMenuView extends BaseView,InformationView {
    void placeUser(User user);
}
