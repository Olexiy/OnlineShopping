package com.example.olexi.onlineshoppingandroid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.ui.modules.basket.OnBasketClickListener;
import com.example.olexi.onlineshoppingandroid.utils.AsyncTaskLoadImage;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 1/26/2018
 */

public class BasketProductsRecycleViewAdapter extends RecyclerView.Adapter<BasketProductsRecycleViewAdapter.ProductViewHolder>{

    private Context context;
    private ArrayList<OrderItem> orderItems;
    private OnItemClickListener<Product> listener;
    private OnBasketClickListener basketClickListener;

    public BasketProductsRecycleViewAdapter(Context context, OnItemClickListener<Product> listener, OnBasketClickListener basketClickListener) {
        this.context = context;
        this.listener = listener;
        this.basketClickListener = basketClickListener;
    }

    public void loadData(List<OrderItem> orderItems){
        if(this.orderItems == null){
            this.orderItems = new ArrayList<>();
        }
        this.orderItems.addAll(orderItems);
        notifyDataSetChanged();
    }

    public void cleanData() {
        orderItems = new ArrayList<>();
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_basket_products_list,parent,false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        holder.initData(context,orderItems.get(position));
    }

    @Override
    public int getItemCount() {
        return orderItems == null ? 0 : orderItems.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ll_products_container)
        LinearLayout productsContainer;

        @BindView(R.id.tv_product_name)
        TextView productName;

        @BindView(R.id.tv_product_price)
        TextView productPrice;

        @BindView(R.id.tv_quantity_of_product)
        TextView quantityProduct;

        @BindView(R.id.iv_product_title)
        ImageView productImageTitle;

        @BindView(R.id.iv_quantity_minus)
        ImageView productQuantityMinus;

        @BindView(R.id.iv_quantity_plus)
        ImageView productQuantityPlus;

        @BindView(R.id.iv_product_remove)
        ImageView productRemove;

        private OrderItem orderItem;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void initData(final Context context, final OrderItem orderItem){
            this.orderItem = orderItem;
            productName.setText(orderItem.getProduct().toString());
            productPrice.setText(orderItem.getPrice()+ " " + context.getString(R.string.uah_symbol));
            quantityProduct.setText(orderItem.getQuantity() + " " +context.getString(R.string.pieces_shor_form));

            if(orderItem.getProduct().getImageTitle() != null){
                showProductImageTitle(orderItem.getProduct().getImageTitle());
            }else {
                productImageTitle.setImageResource(R.drawable.star);
            }

            productImageTitle.setOnClickListener(imageListener);

            productQuantityPlus.setOnClickListener(plusListener);

            productQuantityMinus.setOnClickListener(minusListener);

            if(checkQuantityScope()){
                productQuantityMinus.setVisibility(View.VISIBLE);
            }else {
                productQuantityMinus.setVisibility(View.INVISIBLE);
            }

            productRemove.setOnClickListener(removeListener);
        }

        void showProductImageTitle(String imageName) {
            String url = " http://192.168.0.104:8080/api/ProductImage?imageName=" + imageName;
            new AsyncTaskLoadImage(productImageTitle).execute(url);
        }

        void changeQuantity(int quantity, int price){
            quantityProduct.setText(quantity + " " + context.getString(R.string.pieces_shor_form));
            productPrice.setText(price + " " + context.getString(R.string.uah_symbol));
        }

        boolean checkQuantityScope(){
            return orderItem.getQuantity() > 1;
        }

        View.OnClickListener imageListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(orderItem.getProduct());
            }
        };

        View.OnClickListener minusListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkQuantityScope()){
                    orderItem.setQuantity(orderItem.getQuantity() - 1);
                    basketClickListener.changeQuantityOfOrderItem(orderItem);
                }else {
                    productQuantityMinus.setVisibility(View.INVISIBLE);
                }
            }
        };

        View.OnClickListener plusListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderItem.setQuantity(orderItem.getQuantity() + 1);
                basketClickListener.changeQuantityOfOrderItem(orderItem);
            }
        };

        View.OnClickListener removeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                basketClickListener.removeOrderItemFromOrder(orderItem);
            }
        };


    }
}
