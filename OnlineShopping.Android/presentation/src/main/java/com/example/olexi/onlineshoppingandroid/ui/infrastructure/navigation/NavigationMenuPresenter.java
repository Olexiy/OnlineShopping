package com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation;

import com.example.domain.interactor.UserInteractor;
import com.example.entity.models.User;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/3/2018
 */

public class NavigationMenuPresenter extends BasePresenter<NavigationMenuView>{

    private final AccountPreferences accountPreferences;
    private final UserInteractor userInteractor;

    @Inject
    public NavigationMenuPresenter(AccountPreferences accountPreferences, UserInteractor userInteractor) {
        this.accountPreferences = accountPreferences;
        this.userInteractor = userInteractor;
    }

    public void loadUser(){
        checkView();
        userInteractor.getUser(accountPreferences.getUserId(), new DisposableSingleObserver<User>() {
            @Override
            public void onSuccess(User user) {
                view.placeUser(user);
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }
}
