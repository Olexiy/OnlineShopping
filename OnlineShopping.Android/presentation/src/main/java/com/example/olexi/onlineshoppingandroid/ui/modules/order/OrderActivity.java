package com.example.olexi.onlineshoppingandroid.ui.modules.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.entity.IntentExtraParam;
import com.example.entity.models.Order;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerOrderComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerOrderListComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.OrderComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.ProductComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.admin.AdminActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.orderList.OrderListActivity;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

/**
 * Created by olexi on 1/23/2018
 */

public class OrderActivity extends NavigationMenuActivity implements ComponentContainer<OrderComponent>,
        OnItemClickListener<Product>{

    public static Intent getCallingIntent(Context context, int orderId, boolean isAdmin){
        Intent intent = new Intent(context,OrderActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IntentExtraParam.ORDER_ID, orderId);
        intent.putExtra(IntentExtraParam.IS_ADMIN, isAdmin);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentFragment(OrderFragment.newInstance(getOrderId(), isAdmin()));
    }

    @Override
    public OrderComponent buildComponent() {
        return DaggerOrderComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public OrderComponent getComponent() {
        return getSavedComponent();
    }

    private int getOrderId() {
        return getIntent().getIntExtra(IntentExtraParam.ORDER_ID,0);
    }

    private boolean isAdmin() {
        return getIntent().getBooleanExtra(IntentExtraParam.IS_ADMIN,false);
    }

    @Override
    public void onItemClick(Product product) {
        navigator.navigateProduct(this, product.getId());
    }

    public void navigateOrderAdmin() {
        Intent intent = new Intent(this, AdminActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
