package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.login.LoginActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.login.LoginFragment;

import dagger.Component;

/**
 * Created by olexi on 12/19/2017
 */
@ActivityScope
@Component(dependencies = AppComponent.class)
public interface LoginComponent {
    //void inject(LoginActivity loginActivity);
    void inject(LoginFragment loginFragment);
}
