package com.example.olexi.onlineshoppingandroid.ui.infrastructure;

import android.support.annotation.CallSuper;

/**
 * Created by olexi on 12/21/2017
 */

public abstract class BasePresenter<V extends BaseView> {
    protected V view;

    final public void setView(V view) {
        this.view = view;
    }

    @CallSuper
    public void detachView() {
        this.view = null;
    }

    final protected void checkView() {
        if (view == null) {
            throw new RuntimeException();
        }
    }
}
