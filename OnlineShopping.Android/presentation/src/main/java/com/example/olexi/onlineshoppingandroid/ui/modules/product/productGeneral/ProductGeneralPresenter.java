package com.example.olexi.onlineshoppingandroid.ui.modules.product.productGeneral;

import android.util.Log;

import com.example.domain.interactor.OrderInteractor;
import com.example.domain.interactor.OrderItemInteractor;
import com.example.domain.interactor.ProductInteractor;
import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/25/2018
 */
@ActivityScope
public class ProductGeneralPresenter extends BasePresenter<ProductGeneralView> {

    private ProductInteractor productInteractor;
    private OrderInteractor orderInteractor;
    private OrderItemInteractor orderItemInteractor;

    @Inject
    public ProductGeneralPresenter(ProductInteractor productInteractor, OrderInteractor orderInteractor, OrderItemInteractor orderItemInteractor) {
        this.productInteractor = productInteractor;
        this.orderInteractor = orderInteractor;
        this.orderItemInteractor = orderItemInteractor;
    }

    public void getProductsImages(int productId){
        productInteractor.getProductImages(productId, new DisposableSingleObserver<List<String>>() {
            @Override
            public void onSuccess(List<String> images) {
                view.showProductImages(images);
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Tag","error getProductsImages");
            }
        });
    }

    public void getProduct(int productId){
        productInteractor.getProduct(productId, new DisposableSingleObserver<Product>() {
            @Override
            public void onSuccess(Product product) {
                view.showProduct(product);
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Tag","error getProduct");
            }
        });
    }

    public void createOrder(final Order order){
        orderInteractor.createOrder(order, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {
                view.initOrder();
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Tag","createOrder onError e = " + e);
                view.showInfo(e.getMessage());
            }
        });
    }

    public void createOrderItem(OrderItem orderItem){
        orderItemInteractor.createOrderItem(orderItem, new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean value) {
                Log.d("Tag","createOrderItem OrderItem successful add ");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Tag","createOrderItem onError e = " + e);
                view.showInfo(e.getMessage());
            }
        });
    }

    public void getOrders(){
        orderInteractor.getOrders(new DisposableSingleObserver<List<Order>>() {
            @Override
            public void onSuccess(List<Order> orders) {
                view.initActiveOrder(orders);
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }

}
