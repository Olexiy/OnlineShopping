package com.example.olexi.onlineshoppingandroid.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.olexi.onlineshoppingandroid.R;

/**
 * Created by olexi on 2/1/2018
 */

public class BoxLoaderView extends View {

    private static final int FRAME_RATE = 2;
    private static final int DEFAULT_SPEED = 10;
    private static final int DEFAULT_STROKE_WIDTH = 20;
    private static final int DEFAULT_STROKE_COLOR = Color.GRAY;
    private static final int DEFAULT_LOADER_COLOR = Color.BLUE;

    private int speed;
    private int strokeWidth;
    private int strokeColor, loaderColor;
    private boolean dirChange = false;
    private Box box, outBox;
    private Handler handler;

    public BoxLoaderView(Context context) {
        super(context);
        init(context, null);
    }

    public BoxLoaderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public void setSpeed(int speed){
        this.speed = speed;
    }

    public void setStrokeWidth(int strokeWidth){
        this.strokeWidth = strokeWidth;
    }

    public void setStrokeColor(int color){
        strokeColor = color;
    }

    public void setLoaderColor(int color){
        loaderColor = color;
    }

    public void init(Context context,@Nullable AttributeSet attrs){
        handler = new Handler();
        if(attrs != null){
            TypedArray typedArray = context.obtainStyledAttributes(R.styleable.BoxLoaderView);
            strokeColor = typedArray.getColor(R.styleable.BoxLoaderView_strokeColor, DEFAULT_STROKE_COLOR);
            loaderColor = typedArray.getColor(R.styleable.BoxLoaderView_loaderColor, DEFAULT_LOADER_COLOR);
            strokeWidth = typedArray.getInt(R.styleable.BoxLoaderView_strokeWidth, DEFAULT_STROKE_WIDTH);
            speed = typedArray.getInt(R.styleable.BoxLoaderView_speed, DEFAULT_SPEED);
            typedArray.recycle();
        }else {
            strokeColor = DEFAULT_STROKE_COLOR;
            loaderColor = DEFAULT_LOADER_COLOR;
            strokeWidth = DEFAULT_STROKE_WIDTH;
            speed = DEFAULT_SPEED;
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (outBox == null){
            outBox = new Box(left, top, right, bottom, strokeColor, 10);
            outBox.getPaint().setStrokeWidth(strokeWidth);
        }
        if(box == null){
            box = new Box(left, top,
                    right, bottom,
                    loaderColor, 10);
            box.setDx(speed);
            box.setDy(speed);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRect(outBox.getRectangle(), outBox.getPaint());
        dirChange = box.bounce(canvas, strokeWidth);
        rectifyBoundaries(canvas, box);
        canvas.drawRect(box.getRectangle(), box.getPaint());
        handler.postDelayed(runnable, dirChange ? FRAME_RATE *20 : FRAME_RATE);
    }

    private void rectifyBoundaries(Canvas canvas, Box box){
        if(box.getLeft() < strokeWidth){
            box.getRectangle().left = strokeWidth;
        }
        if(box.getTop() < strokeWidth){
            box.getRectangle().top = strokeWidth;
        }
        if(box.getRight() > canvas.getWidth() -  strokeWidth){
            box.getRectangle().right = canvas.getWidth() - strokeWidth;
        }
        if(box.getBottom() > canvas.getHeight() -  strokeWidth){
            box.getRectangle().bottom = canvas.getHeight() - strokeWidth;
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            invalidate();
        }
    };

    private static class Box{
        private int color, radius,dx,dy,direction;
        private Rect rectangle;
        private Paint paint;

        private final static int DIRECTION_RIGHT = 0;
        private final static int DIRECTION_BOTTOM = 1;
        private final static int DIRECTION_LEFT = 2;
        private final static int DIRECTION_TOP = 3;

        public Box(int left, int top, int right, int bottom, int color, int radius){
            rectangle = new Rect(left,top,right,bottom);
            this.paint = new Paint();
            this.color = color;
            this.radius = radius;
            this.paint = new Paint();
            paint.setColor(color);
            this.dx = 0;
            this.dy = 0;
            this.direction = 0;
        }

        public Rect getRectangle(){
            return rectangle;
        }

        public int getLeft(){
            return rectangle.left;
        }

        public int getTop(){
            return rectangle.top;
        }

        public int getRight(){
            return rectangle.right;
        }

        public int getBottom(){
            return rectangle.bottom;
        }

        public int getRadius(){
            return radius;
        }

        public Paint getPaint(){
            return paint;
        }

        public void setColor(int color) {
            this.color = color;
        }

        public void setDx(int dx) {
            this.dx = dx;
        }

        public void setDy(int dy) {
            this.dy = dy;
        }

        public boolean bounce(Canvas canvas, int strokeWidth){
            switch (direction){
                case DIRECTION_RIGHT:{
                    if(rectangle.right < canvas.getWidth() - strokeWidth){
                        increaseRight();
                    }else {
                        increaseLeft();
                        if(rectangle.left > canvas.getWidth()/2){
                            direction++;
                            return true;
                        }
                    }
                    break;
                }
                case DIRECTION_BOTTOM:{
                    if(rectangle.bottom < canvas.getHeight() - strokeWidth){
                        increaseBottom();
                    }else {
                        increaseTop();
                        if(rectangle.top > canvas.getHeight()/2){
                            direction++;
                            return true;
                        }
                    }
                    break;
                }
                case DIRECTION_LEFT:{
                    if(rectangle.left > strokeWidth){
                        decreaseLeft();
                    }else {
                        decreaseRight();
                        if(rectangle.right < canvas.getWidth()/2){
                            direction++;
                            return true;
                        }
                    }
                    break;
                }
                case DIRECTION_TOP:{
                    if(rectangle.top > strokeWidth){
                        decreaseTop();
                    }else {
                        decreaseBottom();
                        if(rectangle.bottom < canvas.getHeight()/2){
                            direction = 0;
                            return true;
                        }
                    }
                    break;
                }
            }
            return false;
        }

        private void increaseRight() {
            rectangle.right += dx;
        }

        private void increaseBottom() {
            rectangle.bottom += dy;
        }

        private void increaseLeft() {
            rectangle.left += dx;
        }

        private void increaseTop() {
            rectangle.top += dy;
        }

        private void decreaseRight() {
            rectangle.right -= dx;
        }

        private void decreaseBottom() {
            rectangle.bottom -= dy;
        }

        private void decreaseLeft() {
            rectangle.left -= dx;
        }

        private void decreaseTop() {
            rectangle.top -= dy;
        }
    }
}
