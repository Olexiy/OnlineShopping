package com.example.olexi.onlineshoppingandroid.common.account;

import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by olexi on 12/19/2017
 */
@Singleton
public class AccountInterceptor implements Interceptor {

    private String token;

    @Inject
    public AccountInterceptor() {
    }

    public void setToken(String token){
        this.token = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!TextUtils.isEmpty(token)) {
            return chain.proceed(chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer " + token)
                    .build());
        }

        return chain.proceed(chain.request());
    }

}
