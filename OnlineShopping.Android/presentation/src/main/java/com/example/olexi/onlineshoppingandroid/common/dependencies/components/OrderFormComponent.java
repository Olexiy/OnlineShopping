package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.modules.order.OrderFragment;
import com.example.olexi.onlineshoppingandroid.ui.modules.orderForm.OrderFormFragment;

import dagger.Component;

/**
 * Created by olexi on 2/6/2018
 */
@ActivityScope
@Component(dependencies = AppComponent.class)
public interface OrderFormComponent {
    void inject(OrderFormFragment orderFormFragment);
}
