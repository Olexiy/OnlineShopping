package com.example.olexi.onlineshoppingandroid.ui.modules.product;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.entity.IntentExtraParam;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerProductComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.ProductComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseActivity;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.productsList.ProductsListActivity;

/**
 * Created by olexi on 12/21/2017
 */

public class ProductActivity extends NavigationMenuActivity implements ComponentContainer<ProductComponent>{

    public static Intent getCallingIntent(Context context, int productId) {
        Intent intent = new Intent(context, ProductActivity.class);
        intent.putExtra(IntentExtraParam.PRODUCT_ID, productId);
        return intent;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentFragment(ProductFragment.newInstance(getProductId()));
    }

    @Override
    public ProductComponent buildComponent() {
        return DaggerProductComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public ProductComponent getComponent() {
        return getSavedComponent();
    }

    private int getProductId() {
        return getIntent().getIntExtra(IntentExtraParam.PRODUCT_ID,0);
    }
}
