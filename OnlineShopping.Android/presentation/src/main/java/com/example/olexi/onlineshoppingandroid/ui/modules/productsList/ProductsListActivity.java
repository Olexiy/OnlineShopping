package com.example.olexi.onlineshoppingandroid.ui.modules.productsList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.entity.IntentExtraParam;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerProductsListComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.ProductsListComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.categories.CategoriesActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.product.ProductActivity;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by olexi on 1/10/2018
 */

public class ProductsListActivity extends NavigationMenuActivity implements ComponentContainer<ProductsListComponent>,
        OnItemClickListener<Product> {

    public static Intent getCallingIntent(Context context, String categoryName){
        Intent intent = new Intent(context,ProductsListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IntentExtraParam.CATEGORY_NAME, categoryName);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentFragment(ProductsListFragment.newInstance(getCategoryName()));
    }

    @Override
    public void onItemClick(Product item) {
        navigator.navigateProduct(this, item.getId());
    }

    @Override
    public ProductsListComponent buildComponent() {
        return DaggerProductsListComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public ProductsListComponent getComponent() {
        return getSavedComponent();
    }

    private String getCategoryName() {
        return getIntent().getStringExtra(IntentExtraParam.CATEGORY_NAME);
    }
}
