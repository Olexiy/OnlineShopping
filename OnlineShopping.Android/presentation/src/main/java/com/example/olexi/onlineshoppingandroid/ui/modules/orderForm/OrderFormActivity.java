package com.example.olexi.onlineshoppingandroid.ui.modules.orderForm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.entity.IntentExtraParam;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerOrderComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerOrderFormComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.OrderComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.OrderFormComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.DefaultActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.order.OrderActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.order.OrderFragment;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

/**
 * Created by olexi on 2/6/2018
 */

public class OrderFormActivity extends DefaultActivity implements ComponentContainer<OrderFormComponent>{

    public static Intent getCallingIntent(Context context, int orderId){
        Log.d("Tag","orderId " + orderId);
        Intent intent = new Intent(context,OrderFormActivity.class);
        intent.putExtra(IntentExtraParam.ORDER_ID, orderId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentFragment(OrderFormFragment.newInstance(getOrderId()));
    }

    @Override
    public OrderFormComponent buildComponent() {
        return DaggerOrderFormComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public OrderFormComponent getComponent() {
        return getSavedComponent();
    }

    private int getOrderId() {
        return getIntent().getIntExtra(IntentExtraParam.ORDER_ID,0);
    }
}
