package com.example.olexi.onlineshoppingandroid.common.dependencies;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by olexi on 12/19/2017
 */

@Scope
@Retention(RUNTIME)
public @interface ActivityScope {}
