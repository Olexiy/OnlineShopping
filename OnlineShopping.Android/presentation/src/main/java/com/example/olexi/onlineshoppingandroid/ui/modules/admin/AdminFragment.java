package com.example.olexi.onlineshoppingandroid.ui.modules.admin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.AdminComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.OrderComponent;
import com.example.olexi.onlineshoppingandroid.ui.adapter.OrderListRecyclerViewAdapter;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseFragment;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.orderList.OrderListActivity;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by olexi on 2/12/2018
 */

public class AdminFragment extends BaseFragment implements AdminView {

    @BindView(R.id.toolbar_not_scroll)
    Toolbar toolbar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Inject
    AdminPresenter presenter;

    private OrderListRecyclerViewAdapter adapter;
    private LinearLayoutManager layoutManager;

    public static AdminFragment getInstance() {
        return new AdminFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDependencyComponent(AdminComponent.class).inject(this);
        presenter.setView(this);

        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        OnItemClickListener<Order> listener = getActivity(AdminActivity.class);
        adapter = new OrderListRecyclerViewAdapter(listener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin, container, false);
        ButterKnife.bind(this, view);
        getBaseActivity().setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        presenter.getAdminOrders();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_admin, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgressIndicator(boolean condition) {

    }

    @Override
    public void showInfo(String message) {

    }

    @Override
    public void showError(String errorMessage) {
        Log.d("Tag", "Error  " + errorMessage);
    }

    @Override
    public void showActiveOrders(List<Order> orders) {
        adapter.cleanData();
        adapter.loadData(orders);
    }

    public void logout() {
        getActivity(AdminActivity.class).logout();
    }
}
