package com.example.olexi.onlineshoppingandroid.ui.modules.categories;

import com.example.domain.interactor.CategoriesInteractor;
import com.example.entity.models.Category;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ActivityScope;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by olexi on 1/5/2018
 */
@ActivityScope
public class CategoriesPresenter extends BasePresenter<CategoriesView> {

    private CategoriesInteractor interactor;

    @Inject
    public CategoriesPresenter(CategoriesInteractor interactor) {
        this.interactor = interactor;
    }

    void getCategories(){
        interactor.getCategories(new DisposableSingleObserver<List<Category>>() {
            @Override
            public void onSuccess(List<Category> categories) {
                view.showCategories(categories);
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }
        });
    }
}
