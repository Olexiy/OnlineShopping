package com.example.olexi.onlineshoppingandroid.common.dependencies.components;

import android.content.Context;

import com.example.domain.interactor.AccountInteractor;
import com.example.domain.interactor.AdminInteractor;
import com.example.domain.interactor.CategoriesInteractor;
import com.example.domain.interactor.OrderInteractor;
import com.example.domain.interactor.OrderItemInteractor;
import com.example.domain.interactor.ProductInteractor;
import com.example.domain.interactor.ProfileInteractor;
import com.example.domain.interactor.UserInteractor;
import com.example.domain.repository.ProfileRepository;
import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;
import com.example.olexi.onlineshoppingandroid.common.PreparationWorker;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.common.dependencies.modules.ApiModule;
import com.example.olexi.onlineshoppingandroid.common.dependencies.modules.AppModule;
import com.example.olexi.onlineshoppingandroid.common.dependencies.modules.RepositoryModule;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseActivity;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.setting.SettingActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.start.StartActivity;
import com.example.olexi.onlineshoppingandroid.ui.navigation.Navigator;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by olexi on 12/19/2017
 */
@Singleton
@Component(modules = {AppModule.class,  ApiModule.class , RepositoryModule.class})
public interface AppComponent {
    void inject(StartActivity startActivity);
    void inject(BaseActivity baseActivity);
    void inject(NavigationMenuActivity navigationMenuActivity);
    void inject(SettingActivity settingActivity);

    PreparationWorker preparationWorker();
    Context context();
    Navigator navigator();

    AccountPreferences accountPreferences();
    AccountInteractor accountInteractor();
    AdminInteractor adminInteractor();
    UserInteractor userInteractor();
    ProfileInteractor profileInteractor();
    CategoriesInteractor categoryInteractor();
    ProductInteractor productInteractor();
    OrderInteractor orderInteractor();
    OrderItemInteractor orderItemInteractor();
}
