package com.example.olexi.onlineshoppingandroid.common.dependencies.modules;

import android.content.Context;
import android.util.Log;

import com.example.data.executor.JobPoolExecutor;
import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.olexi.onlineshoppingandroid.common.App;
import com.example.olexi.onlineshoppingandroid.common.UIThread;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.common.account.HawkAccountPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by olexi on 12/19/2017
 */
@Module
public class AppModule {
    private App app;

    public AppModule(App app){
        this.app = app;
    }

    @Provides
    @Singleton
    AccountPreferences provideAccountPreferences(HawkAccountPreferences hawkAccountPreferences){
        return hawkAccountPreferences;
    }

    @Provides
    @Singleton
    Context provideApplicationContext(){
        return app;
    }

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor() {
        return new JobPoolExecutor();
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread() {
        return new UIThread();
    }
}
