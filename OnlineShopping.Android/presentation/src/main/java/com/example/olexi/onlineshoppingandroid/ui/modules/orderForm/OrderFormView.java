package com.example.olexi.onlineshoppingandroid.ui.modules.orderForm;

import com.example.entity.models.Order;
import com.example.entity.models.User;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

/**
 * Created by olexi on 2/6/2018
 */

public interface OrderFormView extends BaseView, InformationView, IndicativeView{
    void showOrderDetail(Order order);
    void showUserDetail(User user);
    void navigateOrderList();
}
