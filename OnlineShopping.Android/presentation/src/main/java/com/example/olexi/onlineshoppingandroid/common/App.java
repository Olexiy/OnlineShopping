package com.example.olexi.onlineshoppingandroid.common;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.example.olexi.onlineshoppingandroid.common.dependencies.components.AppComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerAppComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.modules.AppModule;
import com.example.olexi.onlineshoppingandroid.utils.CustomExceptionHandler;
import com.orhanobut.hawk.Hawk;

/**
 * Created by olexi on 12/19/2017
 */

public class App extends Application{

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler());
        super.onCreate();
        Hawk.init(this).build();
        buildDependencyComponent();
    }

    private void buildDependencyComponent(){
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getDependencyComponent(){
        return appComponent;
    }

}
