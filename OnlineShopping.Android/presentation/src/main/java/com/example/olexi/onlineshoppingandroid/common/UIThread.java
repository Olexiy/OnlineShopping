package com.example.olexi.onlineshoppingandroid.common;

import com.example.domain.executor.PostExecutionThread;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by olexi on 12/21/2017
 */

public class UIThread implements PostExecutionThread {
    @Override
    public Scheduler getObserveScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
