package com.example.olexi.onlineshoppingandroid.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.entity.models.Category;
import com.example.entity.models.Product;
import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.utils.AsyncTaskLoadImage;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.http.Url;

/**
 * Created by olexi on 1/10/2018
 */

public class ProductsListRecyclerViewAdapter extends RecyclerView.Adapter<ProductsListRecyclerViewAdapter.ProductsListViewHolder>{

    private Context context;
    private ArrayList<Product> products;
    private OnItemClickListener<Product> listener;

    public ProductsListRecyclerViewAdapter(Context context, OnItemClickListener<Product> listener) {
        this.context = context;
        this.listener = listener;
    }

    public void loadData(List<Product> productList){
        if(products == null){
            products = new ArrayList<>();
        }
        products.addAll(productList);
        notifyDataSetChanged();
    }

    public void cleanData() {
        products = new ArrayList<>();
    }

    @Override
    public ProductsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products_list, parent, false);
        return new ProductsListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductsListViewHolder holder, int position) {
        holder.initData(context,products.get(position));
    }

    @Override
    public int getItemCount() {
        return products == null ? 0 : products.size();
    }

    public class ProductsListViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ll_products_container)
        LinearLayout productsContainer;

        @BindView(R.id.tv_product_name)
        TextView productName;

        @BindView(R.id.tv_product_price)
        TextView productPrice;

        @BindView(R.id.iv_product_title)
        ImageView productImageTitle;

        public ProductsListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void initData(Context context, final Product product){
            productName.setText(product.toString());
            productPrice.setText(product.getPrice()+ " " + context.getString(R.string.uah_symbol));
            if(product.getImageTitle() != null){
                Log.d("Tag",product.getImageTitle());
                showProductImageTitle(product.getImageTitle());
            }else {
                productImageTitle.setImageResource(R.drawable.star);
            }

            productsContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(product);
                }
            });
        }

        public void showProductImageTitle(String imageName) {
            String url = " http://192.168.0.104:8080/api/ProductImage?imageName=" + imageName;
            new AsyncTaskLoadImage(productImageTitle).execute(url);
        }
    }

}
