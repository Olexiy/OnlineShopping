package com.example.olexi.onlineshoppingandroid.ui.modules.setting;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.example.olexi.onlineshoppingandroid.R;
import com.example.olexi.onlineshoppingandroid.common.App;
import com.example.olexi.onlineshoppingandroid.common.account.AccountPreferences;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerOrderComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.OrderComponent;
import com.example.olexi.onlineshoppingandroid.ui.navigation.Navigator;

import javax.inject.Inject;

/**
 * Created by olexi on 2/7/2018
 */

public class SettingActivity extends PreferenceActivity{

    @Inject
    Navigator navigator;

    @Inject
    AccountPreferences accountPreferences;

    private Preference prefLogout;
    private static final String PREF_LOGOUT = "pref_logout";

    public static Intent getCallingIntent(Context context){
        return new Intent(context, SettingActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App)getApplicationContext()).getDependencyComponent().inject(this);
        addToolbar();
        buildPreferences();
        prefLogout.setOnPreferenceClickListener(listenerLogout);
    }

    public void buildPreferences(){
        PreferenceScreen rootScreen = getPreferenceManager().createPreferenceScreen(this);
        setPreferenceScreen(rootScreen);
        prefLogout = new Preference(this);
        prefLogout.setKey(PREF_LOGOUT);
        prefLogout.setTitle("Logout");
        rootScreen.addPreference(prefLogout);
    }

    public void logout(){
        accountPreferences.clearData();
        navigator.navigateUserLogin(this);
    }

    public void addToolbar(){
        LinearLayout root = (LinearLayout)findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar toolbar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.toolbar_setting,root,false);
        toolbar.setOnClickListener(listenerToolbar);
        root.addView(toolbar,0);
    }

    Preference.OnPreferenceClickListener listenerLogout = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            logout();
            return false;
        }
    };

    View.OnClickListener listenerToolbar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };

}
