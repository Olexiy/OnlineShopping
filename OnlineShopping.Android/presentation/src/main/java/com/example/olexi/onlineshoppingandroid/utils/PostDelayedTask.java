package com.example.olexi.onlineshoppingandroid.utils;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by olexi on 1/29/2018
 */

public class PostDelayedTask extends AsyncTask<Long,Integer,Void> {

    private Callback mCallback;
    private long mTime;
    private boolean mExecuted;

    public static PostDelayedTask create(long time, boolean autoExecute, Callback callback) {
        return new PostDelayedTask(time, autoExecute, callback);
    }

    public PostDelayedTask(long time, boolean autoExecute, Callback callback) {
        mCallback = callback;
        mTime = time;
        if (autoExecute) {
            start();
        }
    }

    public void start() {
        if (mExecuted) {
            throw new RuntimeException("is already executed");
        }
        mExecuted = true;
        execute(mTime);
    }

    @Override
    protected Void doInBackground(Long... longs) {
        try {
            Thread.sleep(longs[0]);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        mCallback.onSucceed();
    }

    public interface Callback{
        void onSucceed();
    }
}
