package com.example.olexi.onlineshoppingandroid.ui.modules.register;

import com.example.olexi.onlineshoppingandroid.ui.infrastructure.BaseView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.IndicativeView;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.InformationView;

/**
 * Created by olexi on 2/9/2018
 */

public interface RegisterView extends BaseView,InformationView,IndicativeView{
    void succeedRegister();
}
