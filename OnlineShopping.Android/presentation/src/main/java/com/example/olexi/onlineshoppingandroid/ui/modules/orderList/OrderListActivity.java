package com.example.olexi.onlineshoppingandroid.ui.modules.orderList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.entity.models.Category;
import com.example.entity.models.Order;
import com.example.olexi.onlineshoppingandroid.common.dependencies.ComponentContainer;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.CategoriesComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerCategoriesComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.DaggerOrderListComponent;
import com.example.olexi.onlineshoppingandroid.common.dependencies.components.OrderListComponent;
import com.example.olexi.onlineshoppingandroid.ui.infrastructure.navigation.NavigationMenuActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.categories.CategoriesActivity;
import com.example.olexi.onlineshoppingandroid.ui.modules.categories.CategoriesFragment;
import com.example.olexi.onlineshoppingandroid.utils.OnItemClickListener;

/**
 * Created by olexi on 1/21/2018
 */

public class OrderListActivity extends NavigationMenuActivity implements ComponentContainer<OrderListComponent>,
        OnItemClickListener<Order> {

    public static Intent getCallingIntent(Context context){
        return new Intent(context,OrderListActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentFragment(OrderListFragment.newInstance());
    }

    @Override
    public void onItemClick(Order order) {
        navigator.navigateOrder(this,order.getId());
    }

    @Override
    public OrderListComponent buildComponent() {
        return DaggerOrderListComponent.builder()
                .appComponent(getAppDependencyComponent())
                .build();
    }

    @Override
    public OrderListComponent getComponent() {
        return getSavedComponent();
    }
}
