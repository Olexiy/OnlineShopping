package com.example.data.exception;

/**
 * Created by olexi on 12/21/2017.
 */

public class BadResponseException extends RuntimeException{
    private String errorMessage;
    private String description;

    public BadResponseException(String description, int statusCode) {
        errorMessage = "Status code: " + statusCode;
        this.description = "Error: " + description;
    }

    public String getDescription() {
        return description;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
