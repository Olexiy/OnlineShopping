package com.example.data.tools;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by olexi on 1/22/2018
 */
@Singleton
public class LoggingInterceptor implements Interceptor{

    private final Interceptor loggingInterceptor;

    @Inject
    public LoggingInterceptor() {
        loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        //.setLevel(BuildConfig.DEBUG ? Level.BODY : Level.NONE);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        return loggingInterceptor.intercept(chain);
    }
}
