package com.example.data.provider;

import android.content.Context;

import com.example.data.api.RestApiService;
import com.example.entity.models.ClientProfile;
import com.example.entity.models.Product;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by olexi on 2/13/2018
 */
public class ProfileNetProvider extends BaseNetProvider{

    @Inject
    public ProfileNetProvider(Context context, Retrofit retrofit, RestApiService restApiService) {
        super(context, retrofit, restApiService);
    }

    public Single<ClientProfile> getProfile(){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<ClientProfile> response = restApiService.getProfile().execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProduct()");
            }
        });
    }

    public Single<Boolean> updateProfile(ClientProfile clientProfile){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.updateProfile(clientProfile).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProduct()");
            }
        });
    }

    public Single<Boolean> deleteProfile(int id){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.deleteOrderItem(id).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProduct()");
            }
        });
    }
}
