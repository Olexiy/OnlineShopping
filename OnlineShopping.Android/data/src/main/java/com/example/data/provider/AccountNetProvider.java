package com.example.data.provider;

import android.content.Context;
import android.telecom.Call;
import android.util.Log;

import com.example.data.api.RestApiService;
import com.example.data.exception.BadResponseException;
import com.example.entity.models.Authorization;
import com.example.entity.models.Registration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by olexi on 12/21/2017
 */

@Singleton
public class AccountNetProvider extends BaseNetProvider {

    @Inject
    public AccountNetProvider(Context context, Retrofit retrofit, RestApiService restApiService) {
        super(context, retrofit, restApiService);
    }

    public Single<Authorization> signIn(final String login, final String password) {
        return Single.create(emitter -> {
            checkInternetConnection();
            Response<Authorization> response = restApiService
                    .signIn(createToken(login,password))
                    .execute();
            if(response.isSuccessful()){
                emitter.onSuccess(response.body());
            }else {
                throw new BadResponseException(response.message(),response.code());
            }

        });
    }

    public  Single<Boolean> SignUp(Registration registration){
        return Single.create(emitter ->{
            checkInternetConnection();
            Response<Boolean>response = restApiService
                    .signUp(registration)
                    .execute();
            if(response.isSuccessful()){
                emitter.onSuccess(response.body());
            }else {
                throw new BadResponseException(response.message(),response.code());
            }
        });
    }

    private static Map<String,String > createToken(String login, String password){
        Map<String,String> fields = new HashMap<>();
        fields.put("grant_type", "password");
        fields.put("username", login);
        fields.put("password", password);
        return fields;
    }


}

