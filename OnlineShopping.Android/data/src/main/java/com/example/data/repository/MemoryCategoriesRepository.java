package com.example.data.repository;

import com.example.data.provider.CategoriesNetProvider;
import com.example.domain.repository.CategoriesRepository;
import com.example.entity.models.Category;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

/**
 * Created by olexi on 1/5/2018
 */
@Singleton
public class MemoryCategoriesRepository implements CategoriesRepository{

    private CategoriesNetProvider provider;

    @Inject
    public MemoryCategoriesRepository(CategoriesNetProvider provider) {
        this.provider = provider;
    }

    @Override
    public Single<Category> getCategory(int id) {
        return provider.getCategory(id);
    }

    @Override
    public Single<List<Category>> getCategories() {
        return provider.getCategories();
    }
}
