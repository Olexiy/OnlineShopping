package com.example.data.repository;

import com.example.data.provider.AdminNetProvider;
import com.example.data.provider.OrderNetProvider;
import com.example.domain.repository.AdminRepository;
import com.example.entity.models.Order;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

/**
 * Created by olexi on 2/12/2018
 */
@Singleton
public class MemoryAdminRepository implements AdminRepository{

    private AdminNetProvider provider;

    @Inject
    public MemoryAdminRepository(AdminNetProvider provider) {
        this.provider = provider;
    }

    @Override
    public Single<List<Order>> getAdminOrders() {
        return provider.getAdminOrders();
    }

    @Override
    public Single<Boolean> updateAdminOrder(Order order) {
        return provider.updateAdminOrder(order);
    }
}
