package com.example.data.provider;

import android.content.Context;

import com.example.data.api.RestApiService;
import com.example.entity.models.Category;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by olexi on 1/5/2018
 */

public class CategoriesNetProvider extends BaseNetProvider {

    @Inject
    public CategoriesNetProvider(Context context, Retrofit retrofit, RestApiService restApiService) {
        super(context, retrofit, restApiService);
    }

    public Single<Category> getCategory(int id){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Category> response = restApiService.getCategory(id).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in CategoriesNetProvider getCategory()");
            }
        });
    }

    public Single<List<Category>> getCategories(){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<List<Category>> response = restApiService.getCategories().execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in CategoriesNetProvider getCategories()");
            }
        });
    }
}
