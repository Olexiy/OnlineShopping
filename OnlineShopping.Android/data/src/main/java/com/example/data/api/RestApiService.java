package com.example.data.api;

import com.example.entity.models.Authorization;
import com.example.entity.models.Category;
import com.example.entity.models.ClientProfile;
import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;
import com.example.entity.models.Product;
import com.example.entity.models.ProductCharacteristics;
import com.example.entity.models.Registration;
import com.example.entity.models.User;

import java.util.List;
import java.util.Map;

import okhttp3.Response;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by olexi on 12/20/2017
 */

public interface RestApiService {
    String BASE_URL = "http://192.168.0.104:8080";
    int MAIN_USER_ID = -1;

    @FormUrlEncoded
    @POST("token")
    Call<Authorization> signIn(@FieldMap Map<String, String> auth);

    @POST("api/user")
    Call<Boolean> signUp(@Body Registration registration);

    @GET("api/user/{id}")
    Call<User> getUser(@Path("id") int id);

    @GET("api/user")
    Call<User> getUser();

    @PUT("api/user")
    Call<Boolean> updateUser(@Body User user);

    @GET("api/user")
    Call<List<User>> getUsers(@Query("nameQuery") String nameQuery);

    @GET("api/profile")
    Call<ClientProfile> getProfile();

    @PUT("api/profile")
    Call<Boolean> updateProfile(@Body ClientProfile clientProfile);

    @DELETE("api/profile")
    Call<Boolean> deleteProfile(@Path("id") int id);

    @GET("api/Category")
    Call<List<Category>> getCategories();

    @GET("api/Category/{id}")
    Call<Category> getCategory(@Path("id") int id);

    @POST("api/Category")
    Call<Boolean> createCategoty(@Body Category category);

    @PUT("api/Category")
    Call<Boolean> updateCategory(@Body Category category);

    @DELETE("api/Category/{id}")
    Call<Boolean> deleteCategory(@Path("id") int id);

    @GET("api/Product")
    Call<List<Product>> getProductsList(@Query("categoryName") String categoryName);

    @GET("api/Product/{id}")
    Call<Product> getProduct(@Path("id") int id);

    @POST("api/Product")
    Call<Boolean> createProduct(@Body Product product);

    @PUT("api/Product")
    Call<Boolean> updateProduct(@Body Product product);

    @DELETE("api/Product/{id}")
    Call<Boolean> deleteProduct(@Path("id") int id);

    @GET("api/ProductImage")
    Call<List<String>> getProductImages(@Query("productId") int productId);

    @GET("api/ProductCharacteristics")
    Call<List<ProductCharacteristics>> getProductCharacteristics(@Query("productId") int productId);

    @GET("api/Order")
    Call<List<Order>> getOrders();

    @GET("api/Order/{id}")
    Call<Order> getOrder(@Path("id") int id);

    @POST("api/Order")
    Call<Boolean> createOrder(@Body Order order);

    @PUT("api/Order")
    Call<Boolean> updateOrder(@Body Order order);

    @DELETE("api/Order/{id}")
    Call<Boolean> deleteOrder(@Path("id") int id);

    @GET("api/OrderItem")
    Call<OrderItem> getOrderItem(@Query("orderId") int orderId ,@Query("productId") int productId);

    @GET("api/OrderItem/")
    Call<List<OrderItem>> getOrderItems(@Query("orderId") int orderId);

    @POST("api/OrderItem")
    Call<Boolean> createOrderItem(@Body OrderItem orderItem);

    @PUT("api/OrderItem")
    Call<Boolean> updateOrderItem(@Body OrderItem orderItem);

    @DELETE("api/OrderItem/{id}")
    Call<Boolean> deleteOrderItem(@Path("id") int id);

    @GET("api/AdminOrder")
    Call<List<Order>> getAdminOrders();

    @PUT("api/AdminOrder")
    Call<Boolean> updateAdminOrder(@Body Order order);
}
