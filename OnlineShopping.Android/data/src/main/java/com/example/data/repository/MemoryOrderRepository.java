package com.example.data.repository;

import com.example.data.provider.OrderNetProvider;
import com.example.data.provider.ProductNetProvider;
import com.example.domain.repository.OrderRepository;
import com.example.entity.models.Order;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

/**
 * Created by olexi on 1/21/2018
 */
@Singleton
public class MemoryOrderRepository implements OrderRepository{

    private OrderNetProvider provider;

    @Inject
    public MemoryOrderRepository(OrderNetProvider provider) {
        this.provider = provider;
    }


    @Override
    public Single<Order> getOrder(int id) {
        return provider.getOrder(id);
    }

    @Override
    public Single<List<Order>> getOrders() {
        return provider.getOrders();
    }

    @Override
    public Single<Boolean> createOrder(Order order) {
        return provider.createOrder(order);
    }

    @Override
    public Single<Boolean> updateOrder(Order order) {
        return provider.updateOrder(order);
    }

    @Override
    public Single<Boolean> deleteOrder(int id) {
        return provider.deleteOrder(id);
    }
}
