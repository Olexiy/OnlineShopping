package com.example.data.provider;

import android.content.Context;

import com.example.data.api.RestApiService;
import com.example.entity.models.Order;
import com.example.entity.models.Product;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by olexi on 1/21/2018
 */

public class OrderNetProvider extends BaseNetProvider{

    @Inject
    public OrderNetProvider(Context context, Retrofit retrofit, RestApiService restApiService) {
        super(context, retrofit, restApiService);
    }

    public Single<Order> getOrder(int id){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Order> response = restApiService.getOrder(id).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderNetProvider getOrder()");
            }
        });
    }

    public Single<List<Order>> getOrders(){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<List<Order>> response = restApiService.getOrders().execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderNetProvider getProductsList()");
            }
        });
    }

    public Single<Boolean> createOrder(Order order){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.createOrder(order).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderNetProvider createProduct()");
            }
        });
    }

    public Single<Boolean> updateOrder(Order order){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.updateOrder(order).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderNetProvider updateProduct()");
            }
        });
    }

    public Single<Boolean> deleteOrder(int id){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.deleteOrder(id).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderNetProvider deleteProduct()");
            }
        });
    }
}
