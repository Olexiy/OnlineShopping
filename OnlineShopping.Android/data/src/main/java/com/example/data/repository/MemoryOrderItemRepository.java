package com.example.data.repository;

import com.example.data.provider.OrderItemNetProvider;
import com.example.data.provider.OrderNetProvider;
import com.example.domain.repository.OrderItemRepository;
import com.example.entity.models.OrderItem;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

/**
 * Created by olexi on 1/31/2018
 */
@Singleton
public class MemoryOrderItemRepository implements OrderItemRepository {

    private OrderItemNetProvider provider;

    @Inject
    public MemoryOrderItemRepository(OrderItemNetProvider provider) {
        this.provider = provider;
    }

    @Override
    public Single<OrderItem> getOrderItem(int orderId, int productId) {
        return provider.getOrderItem(orderId,productId);
    }

    @Override
    public Single<List<OrderItem>> getOrderItems(int orderId) {
        return provider.getOrderItems(orderId);
    }

    @Override
    public Single<Boolean> createOrderItem(OrderItem orderItem) {
        return provider.createOrder(orderItem);
    }

    @Override
    public Single<Boolean> updateOrderItem(OrderItem orderItem) {
        return provider.updateOrder(orderItem);
    }

    @Override
    public Single<Boolean> deleteOrderItem(int id) {
        return provider.deleteOrder(id);
    }
}
