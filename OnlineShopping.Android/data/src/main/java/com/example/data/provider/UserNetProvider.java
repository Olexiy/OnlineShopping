package com.example.data.provider;

import android.content.Context;
import android.util.Log;

import com.example.data.api.RestApiService;
import com.example.entity.models.User;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.Response;
/**
 * Created by olexi on 12/21/2017
 */
@Singleton
public class UserNetProvider extends BaseNetProvider{

    @Inject
    public UserNetProvider(Context context, Retrofit retrofit, RestApiService restApiService) {
        super(context, retrofit, restApiService);
    }

    public Single<User> getUser(int id) {
        return Single.create(emitter ->{
            checkInternetConnection();
            Call<User> userCall = id == RestApiService.MAIN_USER_ID
                    ? restApiService.getUser()
                    : restApiService.getUser(id);
            Response<User> response = userCall.execute();
            if(response.isSuccessful()){
                emitter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in UserNetProvider getUser() " + response.code());
            }
        });
    }

    public Single<List<User>> getUsers(String nameQuery) {
        return Single.create(emitter -> {
            checkInternetConnection();
            Response<List<User>> response = restApiService.getUsers(nameQuery).execute();
            if (response.isSuccessful()) {
                emitter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in UserNetProvider getUsers()");
            }
        });
    }

    public Single<Boolean> updateUser(User user) {
        return Single.create(emitter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.updateUser(user).execute();
            if (response.isSuccessful()) {
                emitter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in UserNetProvider update User()");
            }
        });
    }
}
