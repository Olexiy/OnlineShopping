package com.example.data.provider;

import android.content.Context;

import com.example.data.api.RestApiService;
import com.example.entity.models.Order;
import com.example.entity.models.OrderItem;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by olexi on 1/31/2018
 */

public class OrderItemNetProvider extends BaseNetProvider {

    @Inject
    public OrderItemNetProvider(Context context, Retrofit retrofit, RestApiService restApiService) {
        super(context, retrofit, restApiService);
    }

    public Single<OrderItem> getOrderItem(int orderId, int productId){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<OrderItem> response = restApiService.getOrderItem(orderId,productId).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderItemNetProvider getOrder()");
            }
        });
    }

    public Single<List<OrderItem>> getOrderItems(int orderId){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<List<OrderItem>> response = restApiService.getOrderItems(orderId).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderItemNetProvider getProductsList()");
            }
        });
    }

    public Single<Boolean> createOrder(OrderItem orderItem){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.createOrderItem(orderItem).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderItemNetProvider createProduct()");
            }
        });
    }

    public Single<Boolean> updateOrder(OrderItem orderItem){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.updateOrderItem(orderItem).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderItemNetProvider updateProduct()");
            }
        });
    }

    public Single<Boolean> deleteOrder(int id){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.deleteOrderItem(id).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in OrderItemNetProvider deleteProduct()");
            }
        });
    }
}
