package com.example.data.repository;

import com.example.data.provider.ProductNetProvider;
import com.example.domain.repository.ProductRepository;
import com.example.entity.models.Product;
import com.example.entity.models.ProductCharacteristics;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import retrofit2.Response;

/**
 * Created by olexi on 1/10/2018
 */
@Singleton
public class MemoryProductRepository implements ProductRepository{

    private ProductNetProvider provider;

    @Inject
    public MemoryProductRepository(ProductNetProvider provider) {
        this.provider = provider;
    }

    @Override
    public Single<Product> getProduct(int id) {
        return provider.getProduct(id);
    }

    @Override
    public Single<List<Product>> getProductsList(String categoryName) {
        return provider.getProductsList(categoryName);
    }

    @Override
    public Single<Boolean> createProduct(Product product) {
        return provider.createProduct(product);
    }

    @Override
    public Single<Boolean> updateProduct(Product product) {
        return provider.updateProduct(product);
    }

    @Override
    public Single<Boolean> deleteProduct(int id) {
        return provider.deleteProduct(id);
    }

    @Override
    public Single<List<String>> getProductImages(int productId) {
        return provider.getProductImages(productId);
    }

    @Override
    public Single<List<ProductCharacteristics>> getProductCharacteristics(int productId) {
        return provider.getProductCharacteristics(productId);
    }
}
