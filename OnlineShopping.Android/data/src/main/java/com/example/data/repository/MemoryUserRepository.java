package com.example.data.repository;

import com.example.data.provider.UserNetProvider;
import com.example.domain.repository.UserRepository;
import com.example.entity.models.User;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

/**
 * Created by olexi on 12/21/2017
 */
@Singleton
public class MemoryUserRepository implements UserRepository{

    private UserNetProvider provider;

    @Inject
    public MemoryUserRepository(UserNetProvider userNetProvider) {
        this.provider = userNetProvider;
    }

    @Override
    public Single<User> getUser(int id) {
        return provider.getUser(id);
    }

    @Override
    public Single<List<User>> getUsers(String nameQuery) {
        return provider.getUsers(nameQuery);
    }

    @Override
    public Single<Boolean> updateUser(User user) {
        return provider.updateUser(user);
    }
}
