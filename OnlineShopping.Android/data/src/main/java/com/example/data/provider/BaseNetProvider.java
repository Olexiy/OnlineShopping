package com.example.data.provider;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;

import com.example.data.api.RestApiService;

import retrofit2.Retrofit;

/**
 * Created by olexi on 12/21/2017
 */

public class BaseNetProvider {

    private final Context context;
    protected final Retrofit retrofit;
    protected final RestApiService restApiService;

    public BaseNetProvider(Context context, Retrofit retrofit, RestApiService restApiService) {
        this.context = context;
        this.retrofit = retrofit;
        this.restApiService = restApiService;
    }

    void checkInternetConnection() throws Exception {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if(activeNetwork == null || !activeNetwork.isConnectedOrConnecting()){
            throw new Exception();
        }
        //boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    }

}

