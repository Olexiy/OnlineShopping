package com.example.data.repository;

import com.example.data.provider.AccountNetProvider;
import com.example.domain.repository.AccountRepository;
import com.example.entity.models.Authorization;
import com.example.entity.models.Registration;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

/**
 * Created by olexi on 12/21/2017
 */

@Singleton
public class SingleAccountRepository implements AccountRepository {

    private AccountNetProvider accountNetProvider;

    @Inject
    public SingleAccountRepository(AccountNetProvider accountNetProvider){
        this.accountNetProvider = accountNetProvider;
    }

    @Override
    public Single<Boolean> signUp(Registration registration) {
        return accountNetProvider.SignUp(registration);
    }

    @Override
    public Single<Authorization> signIn(String login, String password) {
        return accountNetProvider.signIn(login,password);
    }
}
