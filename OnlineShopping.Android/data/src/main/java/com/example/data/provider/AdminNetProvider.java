package com.example.data.provider;

import android.content.Context;

import com.example.data.api.RestApiService;
import com.example.entity.models.Category;
import com.example.entity.models.Order;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by olexi on 2/12/2018
 */

public class AdminNetProvider extends BaseNetProvider{

    @Inject
    public AdminNetProvider(Context context, Retrofit retrofit, RestApiService restApiService) {
        super(context, retrofit, restApiService);
    }

    public Single<List<Order>> getAdminOrders(){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<List<Order>> response = restApiService.getAdminOrders().execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in AdminNetProvider getCategory()");
            }
        });
    }

    public Single<Boolean> updateAdminOrder(Order order){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.updateAdminOrder(order).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in AdminNetProvider getCategory()");
            }
        });
    }
}
