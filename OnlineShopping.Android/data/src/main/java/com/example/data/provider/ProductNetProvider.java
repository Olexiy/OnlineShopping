package com.example.data.provider;

import android.content.Context;
import android.util.Log;

import com.example.data.api.RestApiService;
import com.example.entity.models.Product;
import com.example.entity.models.ProductCharacteristics;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by olexi on 1/10/2018
 */

public class ProductNetProvider extends BaseNetProvider{

    @Inject
    public ProductNetProvider(Context context, Retrofit retrofit, RestApiService restApiService) {
        super(context, retrofit, restApiService);
    }

    public Single<Product> getProduct(int id){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Product> response = restApiService.getProduct(id).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProduct()");
            }
        });
    }

    public Single<List<Product>> getProductsList(String categoryName){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<List<Product>> response = restApiService.getProductsList(categoryName).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProductsList()");
            }
        });
    }

    public Single<Boolean> createProduct(Product product){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.createProduct(product).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProductsList()");
            }
        });
    }

    public Single<Boolean> updateProduct(Product product){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.updateProduct(product).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProductsList()");
            }
        });
    }

    public Single<Boolean> deleteProduct(int id){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<Boolean> response = restApiService.deleteProduct(id).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProductsList()");
            }
        });
    }

    public Single<List<String>> getProductImages(int productId){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<List<String>> response = restApiService.getProductImages(productId).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProductImages()");
            }
        });
    }

    public Single<List<ProductCharacteristics>> getProductCharacteristics(int productId){
        return Single.create(emiter -> {
            checkInternetConnection();
            Response<List<ProductCharacteristics>> response = restApiService.getProductCharacteristics(productId).execute();
            if(response.isSuccessful()){
                emiter.onSuccess(response.body());
            } else {
                throw new RuntimeException("Error in ProductsListNetProvider getProductCharacteristics()");
            }
        });
    }

}
