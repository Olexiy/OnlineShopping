package com.example.data.repository;

import com.example.data.provider.ProfileNetProvider;
import com.example.domain.repository.ProfileRepository;
import com.example.entity.models.ClientProfile;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

/**
 * Created by olexi on 2/13/2018
 */
@Singleton
public class MemoryProfileRepository implements ProfileRepository {

    private ProfileNetProvider provider;

    @Inject
    public MemoryProfileRepository(ProfileNetProvider provider) {
        this.provider = provider;
    }


    @Override
    public Single<ClientProfile> getProfile() {
        return provider.getProfile();
    }

    @Override
    public Single<Boolean> updateProfile(ClientProfile clientProfile) {
        return provider.updateProfile(clientProfile);
    }

    @Override
    public Single<Boolean> deleteProfile(int id) {
        return provider.deleteProfile(id);
    }
}
